package com.ronem.carwash.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ronem.carwash.R;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by ram on 9/28/17.
 */

public class CarTypeViewHolder extends RecyclerView.ViewHolder {
    @Bind(R.id.car_type_icon)
    public ImageView carTypeIcon;
    @Bind(R.id.car_type_title_view)
    public TextView carTypeTitle;
    @Bind(R.id.car_type_check_view)
    public RelativeLayout carTypeCheckView;

    public CarTypeViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
