package com.ronem.carwash.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.ronem.carwash.R;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by ram on 9/28/17.
 */

public class ColorPlateViewHolder extends RecyclerView.ViewHolder {
    @Bind(R.id.color_plate_view)
    public View colorPlateView;
    @Bind(R.id.color_plate_view_selector)
    public View colorPlateViewSelector;
    @Bind(R.id.color_plate_check_view)
    public ImageView colorPlateCheckView;

    public ColorPlateViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
