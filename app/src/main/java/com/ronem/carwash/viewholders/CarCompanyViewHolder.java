package com.ronem.carwash.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.ronem.carwash.R;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by ronem on 8/30/17.
 */

public class CarCompanyViewHolder extends RecyclerView.ViewHolder {
    @Bind(R.id.car_company_image)
    public ImageView carIcon;
    @Bind(R.id.company_check_view)
    public RelativeLayout carCheckView;

    public CarCompanyViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
