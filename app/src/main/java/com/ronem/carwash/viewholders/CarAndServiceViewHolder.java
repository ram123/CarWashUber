package com.ronem.carwash.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.ronem.carwash.R;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by ram on 11/17/17.
 */

public class CarAndServiceViewHolder extends RecyclerView.ViewHolder {
    @Bind(R.id.car_name)
    public TextView carNameView;
    @Bind(R.id.car_color_value)
    public TextView colorValueView;
    @Bind(R.id.car_plate_value)
    public TextView carPlateValueView;
    @Bind(R.id.car_service_value)
    public TextView carServiceValueView;

    public CarAndServiceViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
