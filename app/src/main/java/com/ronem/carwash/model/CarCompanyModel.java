package com.ronem.carwash.model;

/**
 * Created by ronem on 8/30/17.
 */

public class CarCompanyModel {
    private int companyId;
    private int icon;
    private String name;
    private boolean isSelected;

    public CarCompanyModel(int companyId, int icon, String name,boolean isSelected) {
        this.companyId = companyId;
        this.icon = icon;
        this.name = name;
        this.isSelected = isSelected;
    }

    public int getCompanyId() {
        return companyId;
    }

    public int getIcon() {
        return icon;
    }

    public String getName() {
        return name;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
