package com.ronem.carwash.model;

/**
 * Created by ram on 11/17/17.
 */

public class CarAndService {
    private String cName;
    private String cColor;
    private String cPlateNo;
    private String serviceName;

    public CarAndService(String cName, String cColor, String cPlateNo, String serviceName) {
        this.cName = cName;
        this.cColor = cColor;
        this.cPlateNo = cPlateNo;
        this.serviceName = serviceName;
    }

    public String getcName() {
        return cName;
    }

    public String getcColor() {
        return cColor;
    }

    public String getcPlateNo() {
        return cPlateNo;
    }

    public String getServiceName() {
        return serviceName;
    }
}
