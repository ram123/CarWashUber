package com.ronem.carwash.model.users;

import java.util.HashMap;

/**
 * Created by ram on 9/2/17.
 */

public class Customer {
    private String userName;
    private String customerName;
    private String customerAddress;
    private String password;
    private String customerEmail;
    private String customerMobileNumber;
    private double latitude;
    private double longitude;
    private HashMap<String, HashMap<String, String>> cars;

    public Customer() {
    }

    public Customer(String customerName, String customerAddress,String password,String userName, String customerEmail, String customerMobileNumber, double latitude, double longitude, HashMap<String, HashMap<String, String>> cars) {
        this.customerName = customerName;
        this.customerAddress = customerAddress;
        this.password = password;
        this.userName = userName;
        this.customerEmail = customerEmail;
        this.customerMobileNumber = customerMobileNumber;
        this.latitude = latitude;
        this.longitude = longitude;
        this.cars = cars;
    }

    public String getUserName() {
        return userName;
    }

    public String getCustomerName() {
        return customerName;
    }

    public String getPassword() {
        return password;
    }

    public String getCustomerEmail() {
        return customerEmail;
    }

    public String getCustomerMobileNumber() {
        return customerMobileNumber;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public HashMap<String, HashMap<String, String>> getCars() {
        return cars;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public void setCustomerEmail(String customerEmail) {
        this.customerEmail = customerEmail;
    }

    public void setCustomerMobileNumber(String customerMobileNumber) {
        this.customerMobileNumber = customerMobileNumber;
    }

    public String getCustomerAddress() {
        return customerAddress;
    }

    public void setCustomerAddress(String customerAddress) {
        this.customerAddress = customerAddress;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public void setCars(HashMap<String, HashMap<String, String>> cars) {
        this.cars = cars;
    }

    @Override
    public String toString() {
        return "Customer{" +
                "userName='" + userName + '\'' +
                ", customerName='" + customerName + '\'' +
                ", customerAddress='" + customerAddress + '\'' +
                ", password='" + password + '\'' +
                ", customerEmail='" + customerEmail + '\'' +
                ", customerMobileNumber='" + customerMobileNumber + '\'' +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                ", cars=" + cars +
                '}';
    }
}
