package com.ronem.carwash.model;

import java.util.HashMap;
import java.util.List;

/**
 * Created by ronem on 8/7/17.
 */

public class RouteDistanceDuration {
    private List<List<HashMap<String, String>>> routes;
    private String distance;
    private String duration;
    private int value;

    public RouteDistanceDuration(List<List<HashMap<String, String>>> routes, String distance, String duration,int value) {
        this.routes = routes;
        this.distance = distance;
        this.duration = duration;
        this.value = value;
    }

    public List<List<HashMap<String, String>>> getRoutes() {
        return routes;
    }

    public String getDistance() {
        return distance;
    }

    public String getDuration() {
        return duration;
    }

    public int getValue() {
        return value;
    }
}
