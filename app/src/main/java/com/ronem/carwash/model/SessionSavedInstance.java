package com.ronem.carwash.model;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by ram on 10/23/17.
 */

public class SessionSavedInstance {
    private int orderType;
    private LatLng destination;
    private LatLng contact;
    private boolean isAdding;
    private String childNode;

    public SessionSavedInstance(int orderType, LatLng destination, LatLng contact, boolean isAdding, String childNode) {
        this.orderType = orderType;
        this.destination = destination;
        this.contact = contact;
        this.isAdding = isAdding;
        this.childNode = childNode;
    }

    public int getOrderType() {
        return orderType;
    }

    public LatLng getDestination() {
        return destination;
    }

    public LatLng getContact() {
        return contact;
    }

    public boolean isAdding() {
        return isAdding;
    }

    public String getChildNode() {
        return childNode;
    }
}
