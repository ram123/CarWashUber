//package com.ronem.carwash.model.users;
//
///**
// * Created by ronem on 9/10/17.
// */
//
//public class CustomerStationRequestProcessing {
//    private String cUserName;
//    private String cName;
//    private String cAddress;
//    private double cLatitude;
//    private double cLongitude;
//    private String dateTime;
//    private String carType;
//    private String serviceType;
//
//    private String sUserName;
//    private String sName;
//    private String sAddress;
//    private double sLatitude;
//    private double sLongitude;
//    private String sContact;
//    private String sEmail;
//    private int accepted;
//
//    public CustomerStationRequestProcessing() {
//    }
//
//    public CustomerStationRequestProcessing(String cUserName, String cName, String cAddress,
//                                            double cLatitude, double cLongitude, String dateTime, String carType, String serviceType,
//                                            String sUserName, String sName, String sAddress, double sLatitude,
//                                            double sLongitude, String sContact, String sEmail, int accepted) {
//        this.cUserName = cUserName;
//        this.cName = cName;
//        this.cAddress = cAddress;
//        this.cLatitude = cLatitude;
//        this.cLongitude = cLongitude;
//        this.dateTime = dateTime;
//        this.carType = carType;
//        this.serviceType = serviceType;
//        this.sUserName = sUserName;
//        this.sName = sName;
//        this.sAddress = sAddress;
//        this.sLatitude = sLatitude;
//        this.sLongitude = sLongitude;
//        this.sContact = sContact;
//        this.sEmail = sEmail;
//        this.accepted = accepted;
//    }
//
//    public String getcUserName() {
//        return cUserName;
//    }
//
//    public String getcName() {
//        return cName;
//    }
//
//    public String getcAddress() {
//        return cAddress;
//    }
//
//    public double getcLatitude() {
//        return cLatitude;
//    }
//
//    public double getcLongitude() {
//        return cLongitude;
//    }
//
//    public String getDateTime() {
//        return dateTime;
//    }
//
//    public String getCarType() {
//        return carType;
//    }
//
//    public String getServiceType() {
//        return serviceType;
//    }
//
//    public String getsUserName() {
//        return sUserName;
//    }
//
//    public String getsName() {
//        return sName;
//    }
//
//    public String getsAddress() {
//        return sAddress;
//    }
//
//    public double getsLatitude() {
//        return sLatitude;
//    }
//
//    public double getsLongitude() {
//        return sLongitude;
//    }
//
//    public String getsContact() {
//        return sContact;
//    }
//
//    public String getsEmail() {
//        return sEmail;
//    }
//
//    public int getAccepted() {
//        return accepted;
//    }
//
//    public void setAccepted(int accepted) {
//        this.accepted = accepted;
//    }
//
//    @Override
//    public String toString() {
//        return "CustomerStationRequest{" +
//                "cUserName='" + cUserName + '\'' +
//                ", cName='" + cName + '\'' +
//                ", cAddress='" + cAddress + '\'' +
//                ", cLatitude=" + cLatitude +
//                ", cLongitude=" + cLongitude +
//                ", dateTime='" + dateTime + '\'' +
//                ", carType='" + carType + '\'' +
//                ", serviceType='" + serviceType + '\'' +
//                ", sUserName='" + sUserName + '\'' +
//                ", sName='" + sName + '\'' +
//                ", sAddress='" + sAddress + '\'' +
//                ", sLatitude=" + sLatitude +
//                ", sLongitude=" + sLongitude +
//                ", sContact='" + sContact + '\'' +
//                ", sEmail='" + sEmail + '\'' +
//                ", accepted=" + accepted +
//                '}';
//    }
//}
