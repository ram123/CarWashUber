package com.ronem.carwash.model.users;

import java.util.HashMap;

/**
 * Created by ronem on 9/6/17.
 */

public class CustomerStationRequest {
    private String cUserName;
    private String cName;
    private String cAddress;
    private String cContact;
    private double cLatitude;
    private double cLongitude;
    private String dateTime;

    private int numberOfCars;
    private HashMap<String, HashMap<String, String>> cars;

    private double contactLatitude;
    private double contactLongitude;
    private String sUserName;
    private String sName;
    private String sAddress;
    private double sLatitude;
    private double sLongitude;
    private String sContact;
    private String sEmail;
    private int accepted;

    public CustomerStationRequest() {
    }

    public CustomerStationRequest(String cUserName, String cName, String cAddress,String cContact,
                                  double cLatitude, double cLongitude, double contactLatitude, double contactLongitude, String dateTime, int numberOfCars, HashMap<String, HashMap<String, String>> cars,
                                  String sUserName, String sName, String sAddress, double sLatitude,
                                  double sLongitude, String sContact, String sEmail, int accepted) {
        this.cUserName = cUserName;
        this.cName = cName;
        this.cAddress = cAddress;
        this.cContact = cContact;
        this.cLatitude = cLatitude;
        this.cLongitude = cLongitude;
        this.contactLatitude = contactLatitude;
        this.contactLongitude = contactLongitude;
        this.dateTime = dateTime;
        this.numberOfCars = numberOfCars;
        this.cars = cars;
        this.sUserName = sUserName;
        this.sName = sName;
        this.sAddress = sAddress;
        this.sLatitude = sLatitude;
        this.sLongitude = sLongitude;
        this.sContact = sContact;
        this.sEmail = sEmail;
        this.accepted = accepted;
    }

    public String getcUserName() {
        return cUserName;
    }

    public String getcName() {
        return cName;
    }

    public String getcAddress() {
        return cAddress;
    }

    public double getcLatitude() {
        return cLatitude;
    }

    public double getcLongitude() {
        return cLongitude;
    }

    public double getContactLatitude() {
        return contactLatitude;
    }

    public void setContactLatitude(double contactLatitude) {
        this.contactLatitude = contactLatitude;
    }

    public double getContactLongitude() {
        return contactLongitude;
    }

    public void setContactLongitude(double contactLongitude) {
        this.contactLongitude = contactLongitude;
    }

    public String getDateTime() {
        return dateTime;
    }

    public HashMap<String, HashMap<String, String>> getCars() {
        return cars;
    }

    public void setCars(HashMap<String, HashMap<String, String>> cars) {
        this.cars = cars;
    }

    public String getsUserName() {
        return sUserName;
    }

    public String getsName() {
        return sName;
    }

    public String getsAddress() {
        return sAddress;
    }

    public double getsLatitude() {
        return sLatitude;
    }

    public String getcContact() {
        return cContact;
    }

    public void setcContact(String cContact) {
        this.cContact = cContact;
    }

    public double getsLongitude() {
        return sLongitude;
    }

    public String getsContact() {
        return sContact;
    }

    public String getsEmail() {
        return sEmail;
    }

    public int getAccepted() {
        return accepted;
    }

    public void setAccepted(int accepted) {
        this.accepted = accepted;
    }

    public void setcUserName(String cUserName) {
        this.cUserName = cUserName;
    }

    public void setcName(String cName) {
        this.cName = cName;
    }

    public void setcAddress(String cAddress) {
        this.cAddress = cAddress;
    }

    public void setcLatitude(double cLatitude) {
        this.cLatitude = cLatitude;
    }

    public void setcLongitude(double cLongitude) {
        this.cLongitude = cLongitude;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }


    public void setsUserName(String sUserName) {
        this.sUserName = sUserName;
    }

    public void setsName(String sName) {
        this.sName = sName;
    }

    public void setsAddress(String sAddress) {
        this.sAddress = sAddress;
    }

    public void setsLatitude(double sLatitude) {
        this.sLatitude = sLatitude;
    }

    public void setsLongitude(double sLongitude) {
        this.sLongitude = sLongitude;
    }

    public void setsContact(String sContact) {
        this.sContact = sContact;
    }

    public void setsEmail(String sEmail) {
        this.sEmail = sEmail;
    }

    public int getNumberOfCars() {
        return numberOfCars;
    }

    public void setNumberOfCars(int numberOfCars) {
        this.numberOfCars = numberOfCars;
    }

    @Override
    public String toString() {
        return "CustomerStationRequest{" +
                "cUserName='" + cUserName + '\'' +
                ", cName='" + cName + '\'' +
                ", cAddress='" + cAddress + '\'' +
                ", cLatitude=" + cLatitude +
                ", cLongitude=" + cLongitude +
                ", dateTime='" + dateTime + '\'' +
                ", numberOfCars=" + numberOfCars +
                ", cars=" + cars +
                ", contactLatitude=" + contactLatitude +
                ", contactLongitude=" + contactLongitude +
                ", sUserName='" + sUserName + '\'' +
                ", sName='" + sName + '\'' +
                ", sAddress='" + sAddress + '\'' +
                ", sLatitude=" + sLatitude +
                ", sLongitude=" + sLongitude +
                ", sContact='" + sContact + '\'' +
                ", sEmail='" + sEmail + '\'' +
                ", accepted=" + accepted +
                '}';

    }
}
