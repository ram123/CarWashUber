package com.ronem.carwash.model.users;

import java.util.HashMap;

/**
 * Created by ronem on 9/5/17.
 */

public class SalesManStation {
    private String username;
    private String name;
    private String email;
    private String password;
    private String mobileNumber;
    private double latitude;
    private double longitude;
    private HashMap<String,HashMap<String,String>> polygonPoints;
    private String address;

    public SalesManStation() {
        //required for firebase Relational mapping
    }

    public SalesManStation(String name, String userName,String email, String password,String mobileNumber, double latitude, double longitude,HashMap<String,HashMap<String,String>> polygonPoints, String address) {
        this.name = name;
        this.email = email;
        this.username = userName;
        this.password = password;
        this.mobileNumber = mobileNumber;
        this.latitude = latitude;
        this.longitude = longitude;
        this.polygonPoints = polygonPoints;
        this.address = address;
    }

    public String getPassword() {
        return password;
    }

    public String getName() {
        return name;
    }

    public String getUsername() {
        return username;
    }

    public String getEmail() {
        return email;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public String getAddress() {
        return address;
    }

    public HashMap<String, HashMap<String, String>> getPolygonPoints() {
        return polygonPoints;
    }

    @Override
    public String toString() {
        return "SalesManStation{" +
                "username='" + username + '\'' +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", mobileNumber='" + mobileNumber + '\'' +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                ", polygonPoints=" + polygonPoints +
                ", address='" + address + '\'' +
                '}';
    }
}
