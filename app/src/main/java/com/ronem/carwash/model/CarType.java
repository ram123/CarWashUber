package com.ronem.carwash.model;

/**
 * Created by ronem on 8/2/17.
 */

public class CarType {
    private String id;
    private String type;
    private int icon;
    private String price;
    private String color;
    private String plateNo;

    private boolean checked;

    public CarType(String id, String type, int icon, String price, boolean checked,String color,String plateNo) {
        this.id = id;
        this.type = type;
        this.icon = icon;
        this.price = price;
        this.checked = checked;
        this.color = color;
        this.plateNo = plateNo;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean c) {
        checked = c;
    }

    public int getIcon() {
        return icon;
    }

    public String getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    public String getPrice() {
        return price;
    }

    public String getColor() {
        return color;
    }

    public String getPlateNo() {
        return plateNo;
    }
}
