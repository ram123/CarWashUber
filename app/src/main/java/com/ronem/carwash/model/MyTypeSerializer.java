package com.ronem.carwash.model;

import com.activeandroid.serializer.TypeSerializer;

/**
 * Created by ronem on 9/4/17.
 */

public class MyTypeSerializer extends TypeSerializer {
    @Override
    public Class<?> getDeserializedType() {
        return null;
    }

    @Override
    public Class<?> getSerializedType() {
        return null;
    }

    @Override
    public Object serialize(Object data) {
        return null;
    }

    @Override
    public Object deserialize(Object data) {
        return null;
    }
}
