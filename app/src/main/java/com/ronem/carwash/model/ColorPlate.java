package com.ronem.carwash.model;

/**
 * Created by ram on 9/27/17.
 */

public class ColorPlate {
    private int color;
    private String colorName;
    private boolean checked;

    public ColorPlate(int color, String colorName, boolean checked) {
        this.color = color;
        this.colorName = colorName;
        this.checked = checked;
    }

    public int getColor() {
        return color;
    }

    public String getColorName() {
        return colorName;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    @Override
    public String toString() {
        return "ColorPlate{" +
                "color=" + color +
                ", colorName='" + colorName + '\'' +
                ", checked=" + checked +
                '}';
    }
}
