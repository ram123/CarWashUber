package com.ronem.carwash.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ronem.carwash.R;

import java.util.List;

/**
 * Created by ram on 9/16/17.
 */

public class NoOfCarsAdapter extends BaseAdapter {
    private List<Integer> noOfcars;
    private LayoutInflater inflater;

    public NoOfCarsAdapter(List<Integer> noOfcars, Context context) {
        this.noOfcars = noOfcars;
        inflater = LayoutInflater.from(context);

    }

    @Override
    public int getCount() {
        return noOfcars.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.single_row_car_type, parent, false);
            holder.noOfCarsV = (TextView) convertView.findViewById(R.id.car_type);
            holder.priceView = (TextView) convertView.findViewById(R.id.price);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.noOfCarsV.setText(String.valueOf(noOfcars.get(position)));
        holder.priceView.setVisibility(View.GONE);
        return convertView;
    }

    class ViewHolder {
        TextView noOfCarsV, priceView;
    }

}
