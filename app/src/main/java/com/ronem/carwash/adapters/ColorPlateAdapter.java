package com.ronem.carwash.adapters;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ronem.carwash.R;
import com.ronem.carwash.model.ColorPlate;
import com.ronem.carwash.viewholders.ColorPlateViewHolder;

import java.util.List;

/**
 * Created by ram on 9/28/17.
 */

public class ColorPlateAdapter extends RecyclerView.Adapter<ColorPlateViewHolder> {
    private List<ColorPlate> colorPlates;
    private Context context;
    private LayoutInflater layoutInflater;

    public ColorPlateAdapter(List<ColorPlate> colorPlates, Context context) {
        this.colorPlates = colorPlates;
        this.context = context;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public ColorPlateViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = layoutInflater.inflate(R.layout.single_row_color_plate, parent, false);
        return new ColorPlateViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ColorPlateViewHolder holder, int position) {
        ColorPlate cp = colorPlates.get(position);
        GradientDrawable gd = (GradientDrawable) holder.colorPlateView.getBackground();
        gd.setColor(cp.getColor());

        if (cp.isChecked()) {
            holder.colorPlateViewSelector.setVisibility(View.VISIBLE);
            holder.colorPlateCheckView.setVisibility(View.VISIBLE);
            if (cp.getColorName().equals("White")) {
                holder.colorPlateCheckView.setColorFilter(ContextCompat.getColor(context, R.color.color_green));
            } else {
                holder.colorPlateCheckView.setColorFilter(ContextCompat.getColor(context, R.color.color_white));
            }
        } else {
            holder.colorPlateViewSelector.setVisibility(View.GONE);
            holder.colorPlateCheckView.setVisibility(View.GONE);
        }

    }

    @Override
    public int getItemCount() {
        return colorPlates.size();
    }
}
