package com.ronem.carwash.adapters;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ronem.carwash.R;
import com.ronem.carwash.model.CarCompanyModel;
import com.ronem.carwash.viewholders.CarCompanyViewHolder;

import java.util.List;

/**
 * Created by ronem on 8/30/17.
 */

public class CarCompanyAdapter extends RecyclerView.Adapter<CarCompanyViewHolder> {
    private List<CarCompanyModel> carCompanies;
    private Context context;

    public CarCompanyAdapter(List<CarCompanyModel> carCompanies, Context context) {
        this.carCompanies = carCompanies;
        this.context = context;
    }

    @Override
    public CarCompanyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.single_row_car_company, parent, false);
        return new CarCompanyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CarCompanyViewHolder holder, int position) {
        CarCompanyModel cm = carCompanies.get(position);
        holder.carIcon.setImageResource(cm.getIcon());
        if (cm.isSelected()) {
            holder.carCheckView.setVisibility(View.VISIBLE);
        } else {
            holder.carCheckView.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return carCompanies.size();
    }
}
