package com.ronem.carwash.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ronem.carwash.R;
import com.ronem.carwash.interfaces.OrderChangeStatusListener;
import com.ronem.carwash.model.users.CustomerStationRequest;
import com.ronem.carwash.utils.MetaData;
import com.ronem.carwash.view.OrderMapActivity;
import com.ronem.carwash.view.dialogs.OrderRequestDialog;
import com.ronem.carwash.viewholders.CustomerStationRequestViewHOlder;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by ram on 8/12/17.
 */

public class CustomerStationRequestAdapter extends RecyclerView.Adapter<CustomerStationRequestViewHOlder> {
    private List<CustomerStationRequest> csrs;
    private String status;
    private Context context;
    private OrderChangeStatusListener listener;
    private boolean arabaic;
    private boolean shouldShowInfo;

    public void setOnOrderChangeStatusListener(OrderChangeStatusListener listener) {
        this.listener = listener;
    }

    public void shouldShowInfo() {
        shouldShowInfo = true;
    }

    public CustomerStationRequestAdapter(List<CustomerStationRequest> csrs, String status) {
        this.csrs = csrs;
        this.status = status;
    }

    public void setLang(boolean arabaic) {
        this.arabaic = arabaic;
    }

    @Override
    public CustomerStationRequestViewHOlder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        View itemView = LayoutInflater.from(context).inflate(R.layout.single_row_live_order_client, parent, false);
        return new CustomerStationRequestViewHOlder(itemView);
    }

    @Override
    public void onBindViewHolder(CustomerStationRequestViewHOlder holder, int position) {
        final CustomerStationRequest csr = csrs.get(position);

        String vd = arabaic ? context.getString(R.string.view_detail_arabic) : context.getString(R.string.view_detail);
        String ao = arabaic ? context.getString(R.string.accept_order_arabic) : context.getString(R.string.accept_order);
        String io = arabaic ? context.getString(R.string.ignore_order_arabic) : context.getString(R.string.ignore_order);
        String vr = arabaic ? MetaData.BTN_VIEW_ROUTE_ARABIC : MetaData.BTN_VIEW_ROUTE;
        String co = arabaic ? MetaData.BTN_COMPLETE_ORDER_ARABIC : MetaData.BTN_COMPLETE_ORDER;
        switch (status) {
            case MetaData.ORDER_STATUS_LIVE:
                holder.parentLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent i = new Intent(context, OrderMapActivity.class);
                        i.putExtra(MetaData.KEY_LATITUDE, csr.getcLatitude());
                        i.putExtra(MetaData.KEY_LONGITUDE, csr.getcLongitude());
                        i.putExtra(MetaData.KEY_CONTACT_LATITUDE, csr.getContactLatitude());
                        i.putExtra(MetaData.KEY_CONTACT_LONGITUDE, csr.getContactLongitude());
                        i.putExtra(MetaData.USER_TYPE_CUSTOMER, csr.getcName());
                        context.startActivity(i);
                    }
                });
                holder.btnAcceptOrder.setText(ao);
                holder.btnViewDetail.setText(vd);
                holder.btnIgnoreOrder.setText(io);

                break;
            case MetaData.ORDER_STATUS_PROCESSING:
                holder.btnAcceptOrder.setVisibility(View.GONE);
                holder.btnIgnoreOrder.setVisibility(View.GONE);
                holder.processView.setVisibility(View.VISIBLE);
                holder.btnCompleteOrder.setVisibility(View.VISIBLE);

                holder.btnCompleteOrder.setText(co);

                break;
            case MetaData.ORDER_STATUS_FINISHED:
                holder.btnAcceptOrder.setVisibility(View.GONE);
                holder.btnIgnoreOrder.setVisibility(View.GONE);
                holder.processView.setVisibility(View.VISIBLE);
                holder.checkImage.setVisibility(View.VISIBLE);
                break;
        }

        holder.customerNameTv.setText(csr.getcName());
        holder.customerServiceTv.setText("Contact : " + csr.getcContact());
        holder.processView.setText(vr);
        holder.btnAcceptOrder
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        listener.onOrderAccepted(csr);
                    }
                });

        holder.btnIgnoreOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onOrderRejected(csr);
            }
        });

        holder.btnCompleteOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onOrderComplete(csr);
            }
        });

        holder.btnViewDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                OrderRequestDialog dialog = new OrderRequestDialog(context, csr);
                dialog.show();
            }
        });

        holder.processView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(context, OrderMapActivity.class);
                i.putExtra(MetaData.KEY_LATITUDE, csr.getcLatitude());
                i.putExtra(MetaData.KEY_LONGITUDE, csr.getcLongitude());
                i.putExtra(MetaData.KEY_CONTACT_LATITUDE, csr.getContactLatitude());
                i.putExtra(MetaData.KEY_CONTACT_LONGITUDE, csr.getContactLongitude());
                i.putExtra(MetaData.USER_TYPE_CUSTOMER, csr.getcName());
                if (shouldShowInfo) {
                    i.putExtra(MetaData.KEY_SHOULD_SHOW_INFO, true);
                }
                context.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return csrs.size();
    }
}
