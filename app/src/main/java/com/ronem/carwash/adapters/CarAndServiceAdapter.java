package com.ronem.carwash.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ronem.carwash.R;
import com.ronem.carwash.model.CarAndService;
import com.ronem.carwash.viewholders.CarAndServiceViewHolder;

import java.util.List;

/**
 * Created by ram on 11/17/17.
 */

public class CarAndServiceAdapter extends RecyclerView.Adapter<CarAndServiceViewHolder> {

    private List<CarAndService> datas;
    private Context context;
    private LayoutInflater inflater;

    public CarAndServiceAdapter(Context context, List<CarAndService> datas) {
        this.context = context;
        this.datas = datas;

        inflater = LayoutInflater.from(context);
    }

    @Override
    public CarAndServiceViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = inflater.inflate(R.layout.car_info_layout, parent, false);
        return new CarAndServiceViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CarAndServiceViewHolder holder, int position) {
        CarAndService c = datas.get(position);
        holder.carNameView.setText(c.getcName());
        holder.carPlateValueView.setText(c.getcPlateNo());
        holder.carServiceValueView.setText(c.getServiceName());

        String color = c.getcColor();
        color = color.substring(0, color.lastIndexOf("#"));
        holder.colorValueView.setText(color);
    }

    @Override
    public int getItemCount() {
        return datas.size();
    }
}
