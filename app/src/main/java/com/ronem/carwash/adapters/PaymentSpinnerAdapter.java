package com.ronem.carwash.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.ronem.carwash.R;
import com.ronem.carwash.model.PaymentMethod;

import java.util.List;

/**
 * Created by ronem on 8/22/17.
 */

public class PaymentSpinnerAdapter extends BaseAdapter {
    private List<PaymentMethod> paymentMethods;
    private LayoutInflater inflater;

    public PaymentSpinnerAdapter(List<PaymentMethod> paymentMethods, Context context) {
        this.paymentMethods = paymentMethods;
        inflater = LayoutInflater.from(context);

    }

    @Override
    public int getCount() {
        return paymentMethods.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.payment_method, parent, false);
            holder.paymentmethodView = (TextView) convertView.findViewById(R.id.payment_text);
            holder.paymentIcon = (ImageView) convertView.findViewById(R.id.payment_method_icon);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.paymentmethodView.setText(paymentMethods.get(position).getPaymentMethod());
        holder.paymentIcon.setImageResource(paymentMethods.get(position).getIcon());
        return convertView;
    }

    class ViewHolder {
        TextView paymentmethodView;
        ImageView paymentIcon;
    }

}
