package com.ronem.carwash.adapters;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ronem.carwash.R;
import com.ronem.carwash.model.CarType;
import com.ronem.carwash.viewholders.CarTypeViewHolder;

import java.util.List;

/**
 * Created by ram on 9/28/17.
 */

public class CarTypeRecyclerAdapter extends RecyclerView.Adapter<CarTypeViewHolder> {
    private List<CarType> carTypes;
    private Context context;
    private LayoutInflater inflater;

    public CarTypeRecyclerAdapter(List<CarType> carTypes, Context context) {
        this.carTypes = carTypes;
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public CarTypeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = inflater.inflate(R.layout.single_row_car_type_recycler, parent, false);
        return new CarTypeViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CarTypeViewHolder holder, int position) {
        CarType c = carTypes.get(position);
        holder.carTypeIcon.setImageResource(c.getIcon());
        holder.carTypeTitle.setText(c.getType());
        if (c.isChecked()) {
            holder.carTypeCheckView.setVisibility(View.VISIBLE);
        } else {
            holder.carTypeCheckView.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return carTypes.size();
    }
}
