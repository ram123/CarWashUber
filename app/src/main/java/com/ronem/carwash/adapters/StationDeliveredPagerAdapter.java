package com.ronem.carwash.adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.ronem.carwash.R;

import java.util.ArrayList;

/**
 * Created by ronem on 8/20/17.
 */

public class StationDeliveredPagerAdapter extends FragmentPagerAdapter {

    private ArrayList<Fragment> fragments;
    private String[] titlesEng;
    private String[] titlesArabic;
    private int[] icons = new int[]{R.drawable.motorcycle, R.drawable.car};
    private Context context;
    private boolean lang;

    public StationDeliveredPagerAdapter(FragmentManager fm, Context context) {
        super(fm);
        this.context = context;
        fragments = new ArrayList<>();
        titlesEng = new String[]{context.getString(R.string.delivery), context.getString(R.string.branch)};
        titlesArabic = new String[]{context.getString(R.string.delivery_arabic), context.getString(R.string.branch_arabic)};
    }

    @Override
    public Fragment getItem(int position) {
        return fragments.get(position);
    }

    public void addFragments(Fragment fragment) {
        fragments.add(fragment);
    }

    public void setLanguage(boolean lang) {
        this.lang = lang;
    }

    public View getTabView(int position) {
        View tabView = LayoutInflater.from(context).inflate(R.layout.custom_tab, null);

        ImageView img = (ImageView) tabView.findViewById(R.id.tab_icon);
        img.setImageResource(icons[position]);

        getUpdateText(tabView, position);

        return tabView;
    }


    @Override
    public int getCount() {
        return fragments.size();
    }

    public void getUpdateText(View v, int i) {
        TextView tv = (TextView) v.findViewById(R.id.tab_text);
        if (lang) {
            tv.setText(titlesArabic[i]);
        } else {
            tv.setText(titlesEng[i]);
        }
    }
}
