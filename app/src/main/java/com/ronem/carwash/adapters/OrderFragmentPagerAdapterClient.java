//package com.ronem.carwash.adapters;
//
//import android.content.Context;
//import android.support.v4.app.Fragment;
//import android.support.v4.app.FragmentManager;
//import android.support.v4.app.FragmentStatePagerAdapter;
//
//import com.ronem.carwash.R;
//import com.ronem.carwash.view.live_processing_orders_station_salesman.FragmentLiveOrderClient;
//import com.ronem.carwash.view.live_processing_orders_station_salesman.FragmentProcessingOrderClient;
//
///**
// * Created by ram on 8/12/17.
// */
//
//public class OrderFragmentPagerAdapterClient extends FragmentStatePagerAdapter {
//    private String[] tabTitle;
//    private String[] tabTitleArabic;
//
//    private boolean arabic;
//
//    public OrderFragmentPagerAdapterClient(FragmentManager fm, Context context) {
//        super(fm);
//        tabTitle = context.getResources().getStringArray(R.array.s_eng_tab);
//        tabTitleArabic = context.getResources().getStringArray(R.array.s_arabic_tab);
//    }
//
//    @Override
//    public Fragment getItem(int position) {
//        switch (position) {
//            case 0:
//                return new FragmentLiveOrderClient();
//            case 1:
//                return new FragmentProcessingOrderClient();
//        }
//        return null;
//    }
//
//    public void setLanguage(boolean arabic) {
//        this.arabic = arabic;
//    }
//
//    public String getTabTitle(int position) {
//        if (arabic) {
//            return tabTitleArabic[position];
//        } else {
//            return tabTitle[position];
//        }
//    }
//
//    @Override
//    public int getCount() {
//        return 2;
//    }
//
//    @Override
//    public CharSequence getPageTitle(int position) {
//        return tabTitle[position];
//    }
//}
//
//
