package com.ronem.carwash.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.text.TextUtils;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.ronem.carwash.model.SessionSavedInstance;
import com.ronem.carwash.model.users.Customer;
import com.ronem.carwash.model.users.SalesManStation;

/**
 * Created by ram on 8/1/17.
 */

public class SessionManager {
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private final String IS_LOGIN = "islogin";
    private final String KEY_USER_TYPE = "user_type";
    private final String KEY_USER = "user";
    private final String KEY_FULL_NAME = "full_name";
    private final String KEY_EMAIL = "email";
    private final String KEY_PASSWORD = "password";
    private final String KEY_CONTACT = "contact";
    private final String KEY_CAR_TYPE = "car_type";
    private final String KEY_LATI = "latitude";
    private final String KEY_LONGI = "longi";

    private final String KEY_COUNTER = "counter";
    private final String KEY_USER_COUNTER = "user_counter";

    private final String KEY_PAYMENT_DONE = "payment_done";
    private final String KEY_FIRST_LAUNCH = "first_launch";
    private final String KEY_LANGUAGE_SWITCH = "language_switch";
    public final boolean LANG_ENGLISH = false;
    public final boolean LANG_ARABIC = true;

    private final String KEY_CONTACT_LATITUDE = "contact_latitude";
    private final String KEY_CONTACT_LONGITUDE = "contact_longitude";
    private final String KEY_DESTINATION_LATITUDE = "destination_latitude";
    private final String KEY_DESTINATION_LONGITUDE = "destination_longitude";

    //sessionSavedState
    private final String KEY_ORDER_TYPE_M = "order_type_motorcycle";
    private final String KEY_IS_ADDING_M = "is_adding_motorcycle";
    private final String KEY_CHILD_NODE_M = "child_node_motorcycle";
    private final String KEY_C_LATITUDE_M = "c_latitude_motorcycle";
    private final String KEY_C_LONGITUDE_M = "c_longitude_motorcycle";
    private final String KEY_D_LATITUDE_M = "d_latitude_motorcycle";
    private final String KEY_D_LONGITUDE_M = "d_longitude_motorcycle";

    private final String KEY_ORDER_TYPE_S = "order_type_station";
    private final String KEY_IS_ADDING_S = "is_adding_station";
    private final String KEY_CHILD_NODE_S = "child_node_station";
    private final String KEY_C_LATITUDE_S = "c_latitude_station";
    private final String KEY_C_LONGITUDE_S = "c_longitude_station";
    private final String KEY_D_LATITUDE_S = "d_latitude_station";
    private final String KEY_D_LONGITUDE_S = "d_longitude_station";


    public SessionManager(Context context) {
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public void setCustomerLogin(String userType, Customer user) {
        editor = sharedPreferences.edit();
        Gson g = new Gson();
        String strCus = g.toJson(user);
        editor.putString(KEY_USER_TYPE, userType);
        editor.putString(KEY_USER, strCus);
        editor.putBoolean(IS_LOGIN, true);
        editor.apply();
    }

    public void setClientLogin(String userType, SalesManStation client) {

        editor = sharedPreferences.edit();
        Gson g = new Gson();
        String struser = g.toJson(client);
        editor.putString(KEY_USER_TYPE, userType);
        editor.putString(KEY_USER, struser);
        editor.putBoolean(IS_LOGIN, true);
        editor.apply();
    }

    public Customer getCustomer() {
        String strCust = sharedPreferences.getString(KEY_USER, "");
        if (!TextUtils.isEmpty(strCust)) {
            Gson g = new Gson();
            return g.fromJson(strCust, Customer.class);
        } else {
            return null;
        }
    }

    public SalesManStation getClient() {
        String strCust = sharedPreferences.getString(KEY_USER, "");
        if (!TextUtils.isEmpty(strCust)) {
            Gson g = new Gson();
            return g.fromJson(strCust, SalesManStation.class);
        } else {
            return null;
        }
    }

    public String getFullName() {
        return sharedPreferences.getString(KEY_FULL_NAME, "");
    }

    public String getEmail() {
        return sharedPreferences.getString(KEY_EMAIL, "");
    }

    public String getPassword() {
        return sharedPreferences.getString(KEY_PASSWORD, "");
    }

    public String getContact() {
        return sharedPreferences.getString(KEY_CONTACT, "");
    }

    public int getCarType() {
        return sharedPreferences.getInt(KEY_CAR_TYPE, 0);
    }

    public String getUserType() {
        return sharedPreferences.getString(KEY_USER_TYPE, "");
    }

    public String getLatitude() {
        return sharedPreferences.getString(KEY_LATI, "0.0");

    }

    public String getLongitude() {
        return sharedPreferences.getString(KEY_LONGI, "0.0");
    }

    public boolean isLoggedIn() {
        return sharedPreferences.getBoolean(IS_LOGIN, false);
    }

    public void logOut() {
        editor = sharedPreferences.edit();
        editor.putString(KEY_FULL_NAME, "");
        editor.putString(KEY_EMAIL, "");
        editor.putString(KEY_PASSWORD, "");
        editor.putString(KEY_CONTACT, "");
        editor.putBoolean(IS_LOGIN, false);
        editor.apply();
    }

    public void setPaymentDone() {
        editor = sharedPreferences.edit();
        editor.putBoolean(KEY_PAYMENT_DONE, true).apply();
    }

    public boolean getPaymentDone() {
        return sharedPreferences.getBoolean(KEY_PAYMENT_DONE, false);
    }

    public void clearPaymentDone() {
        editor = sharedPreferences.edit();
        editor.putBoolean(KEY_PAYMENT_DONE, false).apply();
    }


    //update the counter of order
    public void setOrderCounter(int counter) {
        editor = sharedPreferences.edit();
        editor.putInt(KEY_COUNTER, counter).apply();
    }

    public int getLatestCounter() {
        return sharedPreferences.getInt(KEY_COUNTER, 0);
    }

    //update the counter of users
    public void setUserCounter(int counter) {
        editor = sharedPreferences.edit();
        editor.putInt(KEY_USER_COUNTER, counter).apply();
    }

    public int getLatestUserCounter() {
        return sharedPreferences.getInt(KEY_USER_COUNTER, 0);
    }

    public boolean isFirstLaunch() {
        return sharedPreferences.getBoolean(KEY_FIRST_LAUNCH, true);
    }

    public void setFirstLaunch() {
        editor = sharedPreferences.edit();
        editor.putBoolean(KEY_FIRST_LAUNCH, false).apply();
    }

    public void switchToEnglish() {
        editor = sharedPreferences.edit();
        editor.putBoolean(KEY_LANGUAGE_SWITCH, LANG_ENGLISH).apply();
    }

    public void switchToArabic() {
        editor = sharedPreferences.edit();
        editor.putBoolean(KEY_LANGUAGE_SWITCH, LANG_ARABIC).apply();
    }

    public boolean getCurrentLanguage() {
        return sharedPreferences.getBoolean(KEY_LANGUAGE_SWITCH, LANG_ENGLISH);
    }

    public void setMotorcycleSavedInstance(
            int orderType, LatLng destination, LatLng contact, boolean isAdding, String childNode
    ) {

        editor = sharedPreferences.edit();
        editor.putInt(KEY_ORDER_TYPE_M, orderType);
        editor.putBoolean(KEY_IS_ADDING_M, isAdding);
        editor.putString(KEY_CHILD_NODE_M, childNode);

        double dLati = destination.latitude;
        double dLongi = destination.longitude;
        double cLati = contact.latitude;
        double cLongi = contact.longitude;

        editor.putString(KEY_C_LATITUDE_M, String.valueOf(cLati));
        editor.putString(KEY_C_LONGITUDE_M, String.valueOf(cLongi));

        editor.putString(KEY_D_LATITUDE_M, String.valueOf(dLati));
        editor.putString(KEY_D_LONGITUDE_M, String.valueOf(dLongi));

        editor.apply();

    }

    public SessionSavedInstance getMotorycycleSavedInstance() {
        int orderType = sharedPreferences.getInt(KEY_ORDER_TYPE_M, 0);
        boolean isAdding = sharedPreferences.getBoolean(KEY_IS_ADDING_M, false);
        String childNode = sharedPreferences.getString(KEY_CHILD_NODE_M, "");

        String strDLati = sharedPreferences.getString(KEY_D_LATITUDE_M, "");
        String strDLongi = sharedPreferences.getString(KEY_D_LONGITUDE_M, "");
        String strCLati = sharedPreferences.getString(KEY_C_LATITUDE_M, "");
        String strCLongi = sharedPreferences.getString(KEY_C_LONGITUDE_M, "");

        double dLati, dLongi, cLati, cLongi;

        if (orderType == 0) {
            return null;
        } else {
            dLati = Double.parseDouble(strDLati);
            dLongi = Double.parseDouble(strDLongi);
            cLati = Double.parseDouble(strCLati);
            cLongi = Double.parseDouble(strCLongi);

            return new SessionSavedInstance(orderType, new LatLng(dLati, dLongi), new LatLng(cLati, cLongi), isAdding, childNode);
        }

    }

    public void setStationSavedInstance(
            int orderType, LatLng destination, LatLng contact, boolean isAdding, String childNode
    ) {

        editor = sharedPreferences.edit();
        editor.putInt(KEY_ORDER_TYPE_S, orderType);
        editor.putBoolean(KEY_IS_ADDING_S, isAdding);
        editor.putString(KEY_CHILD_NODE_S, childNode);

        double dLati = destination.latitude;
        double dLongi = destination.longitude;
        double cLati = contact.latitude;
        double cLongi = contact.longitude;

        editor.putString(KEY_C_LATITUDE_S, String.valueOf(cLati));
        editor.putString(KEY_C_LONGITUDE_S, String.valueOf(cLongi));

        editor.putString(KEY_D_LATITUDE_S, String.valueOf(dLati));
        editor.putString(KEY_D_LONGITUDE_S, String.valueOf(dLongi));

        editor.apply();

    }

    public SessionSavedInstance getStationSavedInstance() {
        int orderType = sharedPreferences.getInt(KEY_ORDER_TYPE_S, 0);
        boolean isAdding = sharedPreferences.getBoolean(KEY_IS_ADDING_S, false);
        String childNode = sharedPreferences.getString(KEY_CHILD_NODE_S, "");

        String strDLati = sharedPreferences.getString(KEY_D_LATITUDE_S, "");
        String strDLongi = sharedPreferences.getString(KEY_D_LONGITUDE_S, "");
        String strCLati = sharedPreferences.getString(KEY_C_LATITUDE_S, "");
        String strCLongi = sharedPreferences.getString(KEY_C_LONGITUDE_S, "");

        double dLati, dLongi, cLati, cLongi;

        if (orderType == 0) {
            return null;
        } else {
            dLati = Double.parseDouble(strDLati);
            dLongi = Double.parseDouble(strDLongi);
            cLati = Double.parseDouble(strCLati);
            cLongi = Double.parseDouble(strCLongi);

            return new SessionSavedInstance(orderType, new LatLng(dLati, dLongi), new LatLng(cLati, cLongi), isAdding, childNode);
        }

    }

}
