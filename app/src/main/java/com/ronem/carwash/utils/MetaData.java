package com.ronem.carwash.utils;

import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.ronem.carwash.R;
import com.ronem.carwash.model.Address;
import com.ronem.carwash.model.CarCompanyModel;
import com.ronem.carwash.model.CarType;
import com.ronem.carwash.model.ColorPlate;
import com.ronem.carwash.model.NavItem;
import com.ronem.carwash.model.PaymentMethod;
import com.ronem.carwash.model.ServiceType;
import com.ronem.carwash.model.users.SalesManStation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by ronem on 8/1/17.
 */

public class MetaData {

    public class KEY {
        public static final String EVENT_TYPE = "eventType";
        public static final String CAR_ID = "carId";
        public static final String CAR_TYPE = "carType";
        public static final String CAR_PRICE = "carPrice";
        public static final String CAR_COMPANY = "carCompany";
        public static final String CAR_PLATE_NO = "carPlateNo";
        public static final String CAR_COLOR = "carColor";

        public static final String SERVICE_ID = "serviceId";
        public static final String SERVIE_TYPE = "serviceType";
        public static final String SERVICE_CHARGE = "serviceCharge";


    }

    public class VALUE {
        public static final String EVENT_TYPE_LOGIN = "LOGIN";
        public static final String EVENT_TYPE_SIGNUP = "SIGNUP";
    }

    public static final float MAP_ZOOM = /*17*/14.0f;
    public static final double RADIUS_SALESMAN = 7000;//7 km
    public static final double RADIUS_5 = 5000;//5 km
    public static final int STROKE_COLOR = 0xff174517; //green outline
    public static final int SHADE_COLOR = 0x2000ff00; //20% transparent green fill

    public static final int STROKE_COLOR_IMG = 0xff00ff00; //100 %green outline
    public static final int SHADE_COLOR_IMG = 0x0000ff00; //10% transparent green fill

    public static final int STROKE_COLOR_TRANS = 0x0000ff00; //transparent border
    public static final int SHADE_COLOR_TRANS = 0x0000ff00; //transparent fill

    public static final String MSG_EMPTY_FIELD = "Field should not be empty";
    public static final String MSG_PASSWORD_NOT_MATCHED = "Sorry ! But password not matched";
    public static final String MSG_EMAIL_NOT_FOUND = "Sorry But This email not found in our database";
    public static final String MSG_SELECT_CAR_TYPE = "Please select what type of car you have";
    public static final String MSG_SELECT_CAR_TYPE2 = "Please select what type of 2nd car you have";
    public static final String MSG_SELECT_CAR_TYPE3 = "Please select what type of 3rd car you have";
    public static final String SELECT_CAR_TYPE = "Choose car type";
    public static final String KEY_LATITUDE = "latitude";
    public static final String KEY_LONGITUDE = "longitude";
    public static final String KEY_CONTACT_LATITUDE = "contact_latitude";
    public static final String KEY_CONTACT_LONGITUDE = "contact_longitude";
    public static final String KEY_ADDRESS = "address";
    public static final String KEY_DISTANCE = "distance";
    public static final String KEY_DURATION = "duration";
    public static final String KEY_SHOULD_SHOW_INFO = "should_show_info";
    public static final String KEY_ORDER_TYPE = "order_type";
    public static final String ORDER_TYPE_DELIVERD = "Delivered";
    public static final String ORDER_TYPE_STATION = "Station";
    public static final String KEY_USER_TYPE = "user_type";

//    public static final String USER_TYPE_CUSTOMER = "Customer";
//    public static final String USER_TYPE_CLIENT = "Client";
//    public static final String USER_TYPE_CLIENT_STATION = "Station";
//    public static final String USER_TYPE_CLIENT_SALESMAN = "Salesman";
//    public static final String AVAILABLE_SALESMANS = "AvailableSalesman";
//    public static final String AVAILABLE_STATIONS = "AvailableStations";
//    public static final String CUSTOMER_STATION_REQUEST = "CustomerStationRequest";
//    public static final String CUSTOMER_SALESMAN_REQUEST = "CustomerSalesManRequest";
//    public static final String CUSTOMER_SALESMAN_LIVE_ORDER_REQUEST = "WorkingSalesMan";
//    public static final String STATION_FINISHED_TASK = "CompletedTaskStation";
//    public static final String SALESMAN_FINISHED_TASK = "CompletedTaskSalesman";


    public static final String USER_TYPE_CUSTOMER = "Customer";
    public static final String USER_TYPE_CLIENT = "Client";
    public static final String USER_TYPE_CLIENT_STATION = "Branch";
    public static final String USER_TYPE_CLIENT_SALESMAN = "Motorcycle";
    public static final String AVAILABLE_SALESMANS = "AvailableMotorcycle";
    public static final String AVAILABLE_STATIONS = "AvailableBranch";
    public static final String CUSTOMER_STATION_REQUEST = "CustomerBranchRequest";
    public static final String CUSTOMER_SALESMAN_REQUEST = "CustomerMotorcycleRequest";
    public static final String CUSTOMER_SALESMAN_LIVE_ORDER_REQUEST = "WorkingMotorcycle";
    public static final String STATION_FINISHED_TASK = "CompletedTaskBranch";
    public static final String SALESMAN_FINISHED_TASK = "CompletedTaskMotorcycle";
    public static final String STATION_REJECTED_ORDER = "RejectedOrderBranch";
    public static final String SALESMAN_REJECTED_ORDER = "RejectedOrderMotorcycle";


    public static final int ORDER_IDLE = 0;
    public static final int ORDER_ACCEPTED = 1;
    public static final int ORDER_REJECTED = 2;
    public static final int ORDER_COMPLETED = 3;

    //English Translations-------------------------------------------------------------------
    public static final String ITEM_ORDERS = "Orders";
    public static final String ITEM_MY_APPOINTMENTS = "My Appointments";
    public static final String ITEM_FINISHED_ORDER = "Finished Orders";
    public static final String ITEM_REJECTED_ORDER = "Rejected Orders";
    public static final String ITEM_NOTIFICATIONS = "Notifications";
    public static final String ITEM_EDIT_PROFILE = "Profile";
    public static final String ITEM_CARS = "Cars";
    public static final String ITEM_SETTINGS = "Settings";
    public static final String ITEM_HISTORY = "History";
    public static final String ITEM_BILLS = "Bills";
    public static final String ITEM_CONTACT_US = "Contact Us";
    public static final String ITEM_ABOUT_US = "About us";
    public static final String ITEM_HELP = "Help";
    public static final String ITEM_LOG_OUT = "Logout";
    public static final String BTN_VIEW_DETAIL = "View Detail";
    public static final String BTN_ACCEPT_ORDER = "Accept Order";
    public static final String BTN_REJECT_ORDER = "Reject Order";
    public static final String BTN_COMPLETE_ORDER = "Complete Order";
    public static final String BTN_VIEW_ROUTE = "View Route";
    public static final String ORDER_STATUS_LIVE = "live_order";
    public static final String ORDER_STATUS_PROCESSING = "processing_order";
    public static final String ORDER_STATUS_FINISHED = "finished_order";

    //Arabic Translations-------------------------------------------------------------------
    public static final String ITEM_ORDERS_ARABIC = "طلبات";
    public static final String ITEM_MY_APPOINTMENTS_ARABIC = "الطلبات الحية";
    public static final String ITEM_FINISHED_ORDER_ARABIC = "الطلبات المنتهية";
    public static final String ITEM_REJECTED_ORDER_ARABIC = "Rejected Order";
    public static final String ITEM_NOTIFICATIONS_ARABIC = "الإشعارات";
    public static final String ITEM_EDIT_PROFILE_ARABIC = "الملف الشخصي";
    public static final String ITEM_CARS_ARABIC = "السيارات";
    public static final String ITEM_SETTINGS_ARABIC = "الإعدادات";
    public static final String ITEM_HISTORY_ARABIC = "التاريخ";
    public static final String ITEM_BILLS_ARABIC = "فواتير";
    public static final String ITEM_CONTACT_US_ARABIC = " تواصل معنا";
    public static final String ITEM_ABOUT_US_ARABIC = "من نحن";
    public static final String ITEM_HELP_ARABIC = "مساعدة";
    public static final String ITEM_LOG_OUT_ARABIC = "تسجيل خروج";
    public static final String BTN_VIEW_DETAIL_ARABIC = "عرض التفاصيل";
    public static final String BTN_ACCEPT_ORDER_ARABIC = "قبول الطلب";
    public static final String BTN_REJECT_ORDER_ARABIC = "رفض الطلب ";
    public static final String BTN_COMPLETE_ORDER_ARABIC = "اتمام الطلب";
    public static final String BTN_VIEW_ROUTE_ARABIC = "عرض الطريق";
    public static final String ORDER_STATUS_LIVE_ARABIC = "الطلب الحالي";
    public static final String ORDER_STATUS_PROCESSING_ARABIC = "طلب قيد المعالجة";
    public static final String ORDER_STATUS_FINISHED_ARABIC = "الطلبات المنتهية";


    public static List<NavItem> getnavItems(String userType, boolean arabic) {
        List<NavItem> items = new ArrayList<>();


        if (userType.equals(USER_TYPE_CUSTOMER)) {
            if (arabic) {
                items.add(new NavItem(R.mipmap.ic_edit_black_24dp, ITEM_EDIT_PROFILE_ARABIC));
//                items.add(new NavItem(R.mipmap.ic_add_shopping_cart_white_24dp, ITEM_ORDERS_ARABIC));
//                items.add(new NavItem(R.mipmap.ic_directions_car_black_24dp, ITEM_CARS_ARABIC));
//                items.add(new NavItem(R.mipmap.ic_add_shopping_cart_white_24dp, ITEM_BILLS_ARABIC));
//                items.add(new NavItem(R.mipmap.ic_description_black_24dp, ITEM_HISTORY_ARABIC));
//                items.add(new NavItem(R.mipmap.ic_settings_black_24dp, ITEM_SETTINGS_ARABIC));
                items.add(new NavItem(R.mipmap.ic_info_outline_black_24dp, ITEM_ABOUT_US_ARABIC));
                items.add(new NavItem(R.mipmap.ic_call_black_24dp, ITEM_CONTACT_US_ARABIC));
//                items.add(new NavItem(R.mipmap.ic_help_outline_black_24dp, ITEM_HELP_ARABIC));
                items.add(new NavItem(R.mipmap.ic_power_settings_new_white_24dp, ITEM_LOG_OUT_ARABIC));
            } else {
                items.add(new NavItem(R.mipmap.ic_edit_black_24dp, ITEM_EDIT_PROFILE));
//                items.add(new NavItem(R.mipmap.ic_add_shopping_cart_white_24dp, ITEM_ORDERS));
//                items.add(new NavItem(R.mipmap.ic_directions_car_black_24dp, ITEM_CARS));
//                items.add(new NavItem(R.mipmap.ic_add_shopping_cart_white_24dp, ITEM_BILLS));
//                items.add(new NavItem(R.mipmap.ic_description_black_24dp, ITEM_HISTORY));
//                items.add(new NavItem(R.mipmap.ic_settings_black_24dp, ITEM_SETTINGS));
                items.add(new NavItem(R.mipmap.ic_info_outline_black_24dp, ITEM_ABOUT_US));
                items.add(new NavItem(R.mipmap.ic_call_black_24dp, ITEM_CONTACT_US));
//                items.add(new NavItem(R.mipmap.ic_help_outline_black_24dp, ITEM_HELP));
                items.add(new NavItem(R.mipmap.ic_power_settings_new_white_24dp, ITEM_LOG_OUT));
            }
        } else {

            if (arabic) {
                items.add(new NavItem(R.mipmap.ic_add_shopping_cart_white_24dp, ITEM_MY_APPOINTMENTS_ARABIC));
                items.add(new NavItem(R.mipmap.ic_notifications_black_24dp, ITEM_FINISHED_ORDER_ARABIC));
                items.add(new NavItem(R.mipmap.ic_notifications_black_24dp, ITEM_REJECTED_ORDER_ARABIC));
                items.add(new NavItem(R.mipmap.ic_notifications_black_24dp, ITEM_NOTIFICATIONS_ARABIC));
                items.add(new NavItem(R.mipmap.ic_edit_black_24dp, ITEM_EDIT_PROFILE_ARABIC));
                items.add(new NavItem(R.mipmap.ic_info_outline_black_24dp, ITEM_ABOUT_US_ARABIC));
                items.add(new NavItem(R.mipmap.ic_power_settings_new_white_24dp, ITEM_LOG_OUT_ARABIC));
            } else {
                items.add(new NavItem(R.mipmap.ic_add_shopping_cart_white_24dp, ITEM_MY_APPOINTMENTS));
                items.add(new NavItem(R.mipmap.ic_notifications_black_24dp, ITEM_FINISHED_ORDER));
                items.add(new NavItem(R.mipmap.ic_notifications_black_24dp, ITEM_REJECTED_ORDER));
                items.add(new NavItem(R.mipmap.ic_notifications_black_24dp, ITEM_NOTIFICATIONS));
                items.add(new NavItem(R.mipmap.ic_edit_black_24dp, ITEM_EDIT_PROFILE));
                items.add(new NavItem(R.mipmap.ic_info_outline_black_24dp, ITEM_ABOUT_US));
                items.add(new NavItem(R.mipmap.ic_power_settings_new_white_24dp, ITEM_LOG_OUT));
            }
        }


        return items;
    }


    public static List<CarType> getCarType() {
        List<CarType> c = new ArrayList<>();

        c.add(new CarType("1", "Sedan", R.mipmap.sedan, "25", false, "", ""));
        c.add(new CarType("2", "4x4 Car", R.mipmap.forbyfor, "30", false, "", ""));

        return c;
    }

    public static List<ServiceType> getServiceType() {
        List<ServiceType> s = new ArrayList<>();
        s.add(new ServiceType("1", "General", "0"));
        s.add(new ServiceType("2", "External", "5"));
        s.add(new ServiceType("3", "Internal", "10"));
        return s;
    }


    public static List<CarCompanyModel> getCarCompany() {
        List<CarCompanyModel> cars = new ArrayList<>();
        cars.add(new CarCompanyModel(2, R.mipmap.cheverlet, "Chevrolet", false));
        cars.add(new CarCompanyModel(1, R.mipmap.bmw, "BMW", false));
        cars.add(new CarCompanyModel(3, R.mipmap.ford, "Ford", false));
        cars.add(new CarCompanyModel(5, R.mipmap.honda, "Honda", false));
        cars.add(new CarCompanyModel(6, R.mipmap.hyundai, "Hyundai", false));
        cars.add(new CarCompanyModel(7, R.mipmap.jeep, "Jeep", false));
        cars.add(new CarCompanyModel(8, R.mipmap.lexus, "Lexus", false));
        cars.add(new CarCompanyModel(9, R.mipmap.nissan, "Nissan", false));
        cars.add(new CarCompanyModel(10, R.mipmap.toyota, "Toyota", false));
        cars.add(new CarCompanyModel(11, R.mipmap.benz, "Benz", false));
        cars.add(new CarCompanyModel(4, R.drawable.others, "Others", false));

        return cars;
    }

    public static List<Address> getAddress() {
        List<Address> ad = new ArrayList<>();

        ad.add(new Address(1, "Al Mohammadiyah, Riyadh1"));
        ad.add(new Address(2, "Al Mohammadiyah, Riyadh2"));
        ad.add(new Address(3, "Al Mohammadiyah, Riyadh3"));
        ad.add(new Address(4, "Al Mohammadiyah, Riyadh4"));
        return ad;
    }

    public static List<PaymentMethod> getpaymentMEthods() {
        List<PaymentMethod> p = new ArrayList<>();
        p.add(new PaymentMethod(false, 1, "Credit Card", R.mipmap.ic_credit_card_black_24dp, R.mipmap.ic_check_box_outline_blank_black_24dp));
        p.add(new PaymentMethod(false, 2, "Cash Payment", R.mipmap.ic_description_black_24dp, R.mipmap.ic_check_box_outline_blank_black_24dp));
        p.add(new PaymentMethod(false, 3, "Coupon", R.mipmap.ic_card_giftcard_black_24dp, R.mipmap.ic_check_box_outline_blank_black_24dp));
        p.add(new PaymentMethod(false, 4, "Monthly Payment", R.mipmap.ic_perm_contact_calendar_black_24dp, R.mipmap.ic_check_box_outline_blank_black_24dp));
        return p;
    }


    public static void addingTempStationsAndSalesman() {

        addMotorCycle(MetaData.USER_TYPE_CLIENT_SALESMAN);
        addStation(MetaData.USER_TYPE_CLIENT_STATION);

    }


    private static void addMotorCycle(String childNode) {
        //sales man for saudi
        double[][] latlangSaudi = getLatLang();
        DatabaseReference salesManRef = FirebaseDatabase.getInstance().getReference().child(childNode);
        for (int i = 0; i < latlangSaudi.length; i++) {
            int id = i + 1;
            SalesManStation sms = new SalesManStation("MotorCycle " + id,
                    "motorcycle" + id,
                    "motorcycle" + id + "@gmail.com"
                    , "123",
                    "908098",
                    latlangSaudi[i][0], latlangSaudi[i][1],
                    null,
                    "MotorCycle Location " + id);

            salesManRef.child(sms.getUsername()).setValue(sms);
        }


        //sales man for nepal
        SalesManStation sms3 = new SalesManStation("Kunal sharma", "kunal123", "kunal@gmail.com", "123", "908098", 27.640898, 85.316126, null, "Sano thimi");
        SalesManStation sms4 = new SalesManStation("Sankar sha", "sankar123", "suresh@gmail.com", "123", "908098", 27.609359, 83.464806, null, "New Baneshwor");
        SalesManStation sms5 = new SalesManStation("Chandu don", "chandu123", "chandu@gmail.com", "123", "908098", 27.640898, 85.316126, null, "Purano Sinamangal");
        SalesManStation sms51 = new SalesManStation("Charu", "charu123", "charu@gmail.com", "123", "908098", 26.413367, 87.279348, null, "Purano Sinamangal");

        salesManRef.child(sms3.getUsername()).setValue(sms3);
        salesManRef.child(sms4.getUsername()).setValue(sms4);
        salesManRef.child(sms5.getUsername()).setValue(sms5);
        salesManRef.child(sms51.getUsername()).setValue(sms51);


    }

    private static void addStation(String childNode) {
        DatabaseReference salesManRef = FirebaseDatabase.getInstance().getReference().child(childNode);


        SalesManStation sstn1 = new SalesManStation("Hari Shrestha", "hari123", "hari@gmail.com", "123", "90809801", 27.640898, 85.316126, getPolygon1(), "New Baneshwor");
        SalesManStation sstn4 = new SalesManStation("Station 1", "station1", "station1@gmail.com", "123", "9080981", 24.682415, 46.690585, getPolygon2(), "Station 1 Address");


        salesManRef.child(sstn1.getUsername()).setValue(sstn1);
        salesManRef.child(sstn4.getUsername()).setValue(sstn4);

    }

    private static HashMap<String, HashMap<String, String>> getPolygon2() {
        List<LatLng> p1 = new ArrayList<>();
        p1.add(new LatLng(24.682353, 46.688228));
        p1.add(new LatLng(24.681374, 46.689875));
        p1.add(new LatLng(24.681383, 46.691490));
        p1.add(new LatLng(24.683796, 46.691479));
        p1.add(new LatLng(24.684128, 46.689355));

        HashMap<String, HashMap<String, String>> polygonMap = new HashMap<>();

        for (int i = 0; i < p1.size(); i++) {
            String latLangmap = "point" + i;
            HashMap<String, String> pm = new HashMap<>();
            pm.put(KEY_LATITUDE, String.valueOf(p1.get(i).latitude));
            pm.put(KEY_LONGITUDE, String.valueOf(p1.get(i).longitude));
            polygonMap.put(latLangmap, pm);
        }

        return polygonMap;
    }

    private static HashMap<String, HashMap<String, String>> getPolygon1() {
        List<LatLng> p1 = new ArrayList<>();
        p1.add(new LatLng(27.635184, 85.308591));
        p1.add(new LatLng(27.642837, 85.311400));
        p1.add(new LatLng(27.647397, 85.311818));
        p1.add(new LatLng(27.647923, 85.319127));
        p1.add(new LatLng(27.640026, 85.321472));

        HashMap<String, HashMap<String, String>> polygonMap = new HashMap<>();

        for (int i = 0; i < p1.size(); i++) {
            String latLangmap = "point" + i;
            HashMap<String, String> pm = new HashMap<>();
            pm.put(KEY_LATITUDE, String.valueOf(p1.get(i).latitude));
            pm.put(KEY_LONGITUDE, String.valueOf(p1.get(i).longitude));
            polygonMap.put(latLangmap, pm);
        }

        return polygonMap;
    }


    private static double[][] getLatLang() {
        double latlang[][] = new double[][]{
                {24.682415, 46.690585},
                {24.682474, 46.689630},
                {24.682376, 46.691636},
                {24.681937, 46.692334},
                {24.682678, 46.689008},
                {24.682093, 46.690950},
                {24.682971, 46.692752},
                {24.683985, 46.689147},
                {24.684053, 46.689330},
                {24.683565, 46.691465},
                {24.683010, 46.691765},
                {24.683965, 46.690070},
                {24.683000, 46.692580},
        };

        return latlang;
    }

    public static List<ColorPlate> getColorPlates() {
        List<ColorPlate> colorPlates = new ArrayList<>();
        colorPlates.add(new ColorPlate(0xffd20202, "Red", false));
        colorPlates.add(new ColorPlate(0xff0202d2, "Blue", false));
        colorPlates.add(new ColorPlate(0xff086b01, "Green", false));
        colorPlates.add(new ColorPlate(0xff380b6b, "Purple", false));
        colorPlates.add(new ColorPlate(0xffffffff, "White", false));
        colorPlates.add(new ColorPlate(0xff000000, "Black", false));
        colorPlates.add(new ColorPlate(0xfffff200, "Yellow", false));
        colorPlates.add(new ColorPlate(0xff6e6e6e, "Grey", false));
        return colorPlates;
    }
}
