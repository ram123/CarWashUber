package com.ronem.carwash.utils;

/**
 * Created by ram on 7/10/17.
 */

public class Events {

    public static class ReloadEvent {
        private boolean reload;

        public ReloadEvent(boolean reload) {
            this.reload = reload;
        }

        public boolean isReload() {
            return reload;
        }
    }

    public static class CaptainInfoEvent {
        private String type;
        private String distance;
        private String time;
        private String carwasherName;
        private String carwasherAddress;
        private String carType;

        public CaptainInfoEvent(String type, String distance, String time, String carwasherName, String carwasherAddress, String carType) {
            this.type = type;
            this.distance = distance;
            this.time = time;
            this.carwasherName = carwasherName;
            this.carwasherAddress = carwasherAddress;
            this.carType = carType;
        }

        public String getType() {
            return type;
        }

        public String getDistance() {
            return distance;
        }

        public String getTime() {
            return time;
        }

        public String getCarwasherName() {
            return carwasherName;
        }

        public String getCarwasherAddress() {
            return carwasherAddress;
        }

        public String getCarType() {
            return carType;
        }
    }

    public static class LanguageChangeEvent {
        private boolean lang;

        public LanguageChangeEvent(boolean lang) {
            this.lang = lang;
        }

        public boolean isLang() {
            return lang;
        }
    }
}
