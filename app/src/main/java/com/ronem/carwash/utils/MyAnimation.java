package com.ronem.carwash.utils;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by ronem on 3/22/17.
 */

public class MyAnimation {

    private ValueAnimator slideAnimator(int start, int end, final View view) {

        //what = 1 ; vertical;
        //what = 2 ; horizontal;
        ValueAnimator animator = ValueAnimator.ofInt(start, end);

        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {

                int value = (Integer) valueAnimator.getAnimatedValue();

                ViewGroup.LayoutParams layoutParams = view
                        .getLayoutParams();

                    layoutParams.height = value;


                view.setLayoutParams(layoutParams);
            }
        });
        return animator;
    }

    public void expandView(View v) {
        v.setVisibility(View.VISIBLE);
        ValueAnimator mAnimator;

        final int widthSpec = View.MeasureSpec.makeMeasureSpec(0,
                View.MeasureSpec.UNSPECIFIED);
        final int heightSpec = View.MeasureSpec.makeMeasureSpec(0,
                View.MeasureSpec.UNSPECIFIED);

        v.measure(widthSpec, heightSpec);

            mAnimator = slideAnimator(0, v.getMeasuredHeight(), v);

        mAnimator.start();
    }


    public void collapseView(final View v) {
        int dimen;
        ValueAnimator mAnimator;


            dimen = v.getHeight();
            mAnimator = slideAnimator(dimen, 0, v);


        mAnimator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationEnd(Animator animator) {
                // Height=0, but it set visibility to GONE
                v.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationStart(Animator animation) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onAnimationCancel(Animator animation) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onAnimationRepeat(Animator animation) {
                // TODO Auto-generated method stub

            }

        });
        mAnimator.start();
    }
}
