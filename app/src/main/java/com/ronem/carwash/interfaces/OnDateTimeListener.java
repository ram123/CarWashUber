package com.ronem.carwash.interfaces;

/**
 * Created by ronem on 9/10/17.
 */

public interface OnDateTimeListener {
    void onDateTimeReceived(String date, String time);


}
