package com.ronem.carwash.interfaces;

/**
 * Created by ronem on 9/6/17.
 */

public interface ShowDialogEventListener {
    void onRequestClicked();

    void onCancelClicked();
}
