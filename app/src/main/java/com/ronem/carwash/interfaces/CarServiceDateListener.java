package com.ronem.carwash.interfaces;

import java.util.HashMap;

/**
 * Created by ronem on 9/10/17.
 */

public interface CarServiceDateListener {
    void onCarTypeServiceDateReceived(int noOfCar, HashMap<String, HashMap<String, String>> cars, String dateTime);
}
