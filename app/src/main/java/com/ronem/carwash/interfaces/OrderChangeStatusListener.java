package com.ronem.carwash.interfaces;

import com.ronem.carwash.model.users.CustomerStationRequest;

/**
 * Created by ronem on 9/10/17.
 */

public interface OrderChangeStatusListener {
    void onOrderAccepted(CustomerStationRequest csr);

    void onOrderRejected(CustomerStationRequest csr);

    void onOrderComplete(CustomerStationRequest csr);
}
