package com.ronem.carwash.view.dashboard;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.rm.rmswitch.RMSwitch;
import com.ronem.carwash.R;
import com.ronem.carwash.adapters.NavAdapter;
import com.ronem.carwash.adapters.StationDeliveredPagerAdapter;
import com.ronem.carwash.model.NavItem;
import com.ronem.carwash.model.users.Customer;
import com.ronem.carwash.utils.BasicUtilityMethods;
import com.ronem.carwash.utils.EventBus;
import com.ronem.carwash.utils.Events;
import com.ronem.carwash.utils.ItemDividerDecoration;
import com.ronem.carwash.utils.MetaData;
import com.ronem.carwash.utils.RecyclerItemClickListener;
import com.ronem.carwash.utils.SessionManager;
import com.ronem.carwash.view.AboutUs;
import com.ronem.carwash.view.ContactUs;
import com.ronem.carwash.view.dashboard.station_delivered.FragmentDelivered;
import com.ronem.carwash.view.dashboard.station_delivered.FragmentStation;
import com.ronem.carwash.view.editprofile.CustomerProfile;
import com.ronem.carwash.view.login.LoginRegisterCustomerActivity;
import com.ronem.carwash.view.order.OrderActivity;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by ram on 8/1/17.
 */

public class DashboardCustomer extends AppCompatActivity
        implements RecyclerItemClickListener.OnItemClickListener {

    private String TAG = getClass().getSimpleName();

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.toolbar_title_tv)
    TextView toolbarTitleView;
    @Bind(R.id.home_biker_layout)
    LinearLayout homeBikerLayout;
    @Bind(R.id.tab_station_delivered)
    TabLayout tabStationDelivered;
    @Bind(R.id.viewpager)
    ViewPager viewPager;

    @Bind(R.id.drawer_layout)
    DrawerLayout drawerLayout;
    @Bind(R.id.nav_view)
    NavigationView navigationView;

    private TextView emailTv;
    private TextView contactTv;

    private List<NavItem> items;


    private SessionManager sessionManager;
    private Customer customer;
    private StationDeliveredPagerAdapter adapter;
    private NavAdapter navAdapter;
    private boolean CURRENT_LANGUAGE;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dashboard);
        ButterKnife.bind(this);
        sessionManager = new SessionManager(this);
        CURRENT_LANGUAGE = sessionManager.getCurrentLanguage();

        customer = sessionManager.getCustomer();
        settingToolbar();

        homeBikerLayout.setVisibility(View.GONE);

        setUpNavigationMenu();

        BasicUtilityMethods.checkifGPSisEnabled(this);

        loadViewPager();
    }


    private void loadViewPager() {
        adapter = new StationDeliveredPagerAdapter(getSupportFragmentManager(), this);
        adapter.addFragments(new FragmentDelivered());
        adapter.addFragments(new FragmentStation());

        viewPager.setAdapter(adapter);
        tabStationDelivered.setupWithViewPager(viewPager);


        tabStationDelivered.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                selectedTab(tab.getCustomView());
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                unselectTab(tab.getCustomView());
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        setUpTab();
    }

    private void unselectTab(View v) {

        RelativeLayout tabbg = (RelativeLayout) v.findViewById(R.id.tab_bg);
        tabbg.setBackgroundResource(R.drawable.tab_unselected);

        TextView tv = (TextView) v.findViewById(R.id.tab_text);
        tv.setTextColor(ContextCompat.getColor(this, R.color.colorPrimary));

        ImageView img = (ImageView) v.findViewById(R.id.tab_icon);
        img.setColorFilter(ContextCompat.getColor(this, R.color.colorPrimary));
    }

    private void selectedTab(View v) {
        RelativeLayout tabbg = (RelativeLayout) v.findViewById(R.id.tab_bg);
        tabbg.setBackgroundResource(R.drawable.tab_selected);

        TextView tv = (TextView) v.findViewById(R.id.tab_text);
        tv.setTextColor(ContextCompat.getColor(this, R.color.color_white));

        ImageView img = (ImageView) v.findViewById(R.id.tab_icon);
        img.setColorFilter(ContextCompat.getColor(this, R.color.color_white));
    }

    private void setUpTab() {
        Log.i(TAG, "Tab update");
        adapter.setLanguage(CURRENT_LANGUAGE);

        for (int i = 0; i < tabStationDelivered.getTabCount(); i++) {
            Log.i(TAG, "updating..." + i);
            tabStationDelivered.getTabAt(i).setCustomView(adapter.getTabView(i));


            //select the first tab by default
            if (i == 0) {
                selectedTab(tabStationDelivered.getTabAt(i).getCustomView());
            } else {
                unselectTab(tabStationDelivered.getTabAt(i).getCustomView());
            }
        }
    }

    private void updateTab() {
        adapter.setLanguage(CURRENT_LANGUAGE);
        for (int i = 0; i < tabStationDelivered.getTabCount(); i++) {
            View v = tabStationDelivered.getTabAt(i).getCustomView();
            adapter.getUpdateText(v, i);
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        ButterKnife.unbind(this);
    }

    private void settingToolbar() {
        //setting toolbar
        setSupportActionBar(toolbar);

        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (NullPointerException ne) {
            ne.printStackTrace();
        }

        updateToolBarTitle();
    }

    private void updateToolBarTitle() {
        if (CURRENT_LANGUAGE) {
            toolbarTitleView.setText(getString(R.string.app_name_arabic));
        } else {
            toolbarTitleView.setText(getString(R.string.app_name));
        }
    }

    private void setUpNavigationMenu() {
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.setDrawerListener(toggle);
        toggle.syncState();

        emailTv = (TextView) navigationView.findViewById(R.id.user_name);
        contactTv = (TextView) navigationView.findViewById(R.id.user_contact);


        /**
         * setting the navigation items to the navigation view
         */
        items = MetaData.getnavItems(MetaData.USER_TYPE_CUSTOMER, CURRENT_LANGUAGE);
        RecyclerView navRecyclerView = (RecyclerView) navigationView.findViewById(R.id.nav_recycler_view);
        navRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        navRecyclerView.addItemDecoration(new ItemDividerDecoration(this, null));
        navRecyclerView.setHasFixedSize(true);
        navRecyclerView.addItemDecoration(new ItemDividerDecoration(this, null));

        navAdapter = new NavAdapter(items);
        navRecyclerView.setAdapter(navAdapter);
        navRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(this, this));

        RMSwitch languageSwitch = (RMSwitch) navigationView.findViewById(R.id.language_switch);
        languageSwitch.setChecked(CURRENT_LANGUAGE);
        languageSwitch.addSwitchObserver(new RMSwitch.RMSwitchObserver() {
            @Override
            public void onCheckStateChange(RMSwitch switchView, boolean isChecked) {
                CURRENT_LANGUAGE = isChecked;

                //we assumed true as arabic
                //and false as english
                if (CURRENT_LANGUAGE) {
                    sessionManager.switchToArabic();
                    updateUIWithArabic();
                    Log.i(TAG, "arabic");
                } else {
                    sessionManager.switchToEnglish();
                    updateUIWithEnglish();
                    Log.i(TAG, "english");
                }

                //update toolbar title
                updateToolBarTitle();

                //update tab
                updateTab();

                //send update to the subscribers
                EventBus.post(new Events.LanguageChangeEvent(CURRENT_LANGUAGE));

                //update navigation
                items.clear();
                items.addAll(MetaData.getnavItems(MetaData.USER_TYPE_CUSTOMER, CURRENT_LANGUAGE));
                navAdapter.notifyDataSetChanged();
            }
        });

    }

    private void updateUIWithArabic() {

    }

    private void updateUIWithEnglish() {

    }

    private void updateUserInfo() {
        String sEmail = customer.getCustomerEmail();
        String sContact = customer.getCustomerMobileNumber();
        emailTv.setText(sEmail);
        contactTv.setText(sContact);
    }

    @Override
    public void onItemClick(RecyclerView recyclerView, View view, int position) {
        drawerLayout.closeDrawer(GravityCompat.START);
        NavItem ni = items.get(position);
        switch (ni.getTitle()) {
            case MetaData.ITEM_ORDERS:
            case MetaData.ITEM_ORDERS_ARABIC:
                startActivity(new Intent(DashboardCustomer.this, OrderActivity.class));
                break;
            case MetaData.ITEM_NOTIFICATIONS:
            case MetaData.ITEM_NOTIFICATIONS_ARABIC:
                Toast.makeText(getApplicationContext(), "Under construction", Toast.LENGTH_SHORT).show();
                break;
            case MetaData.ITEM_EDIT_PROFILE:
            case MetaData.ITEM_EDIT_PROFILE_ARABIC:
                startActivity(new Intent(DashboardCustomer.this, CustomerProfile.class));
                break;
            case MetaData.ITEM_CONTACT_US_ARABIC:
            case MetaData.ITEM_CONTACT_US:
                Intent contactUsIntent = new Intent(this, ContactUs.class);
                contactUsIntent.putExtra("title", ni.getTitle());
                startActivity(contactUsIntent);
                break;
            case MetaData.ITEM_ABOUT_US:
            case MetaData.ITEM_ABOUT_US_ARABIC:
                Intent aboutUsIntent = new Intent(this, AboutUs.class);
                aboutUsIntent.putExtra("title", ni.getTitle());
                startActivity(aboutUsIntent);
                break;
            case MetaData.ITEM_LOG_OUT:
            case MetaData.ITEM_LOG_OUT_ARABIC:
                sessionManager.logOut();
                Intent i = new Intent(DashboardCustomer.this, LoginRegisterCustomerActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
                break;
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        updateUserInfo();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
