package com.ronem.carwash.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import com.ronem.carwash.R;
import com.ronem.carwash.utils.SessionManager;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by ram on 10/20/17.
 */

public class ContactUs extends AppCompatActivity {

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.contact_us_title)
    TextView title;
    @Bind(R.id.contact_us_detail)
    TextView detail;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.contact_us);
        ButterKnife.bind(this);
        String _title = getIntent().getStringExtra("title");
        boolean arabic = new SessionManager(this).getCurrentLanguage();
        String _detail = arabic ? getString(R.string.contact_us_detail_arabic) :
                getString(R.string.contact_us_detail);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        title.setText(_title);
        detail.setText(_detail);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
