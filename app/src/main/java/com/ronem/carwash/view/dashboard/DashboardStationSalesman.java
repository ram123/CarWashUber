package com.ronem.carwash.view.dashboard;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.rm.rmswitch.RMSwitch;
import com.ronem.carwash.R;
import com.ronem.carwash.adapters.NavAdapter;
import com.ronem.carwash.model.NavItem;
import com.ronem.carwash.model.users.SalesManStation;
import com.ronem.carwash.utils.EventBus;
import com.ronem.carwash.utils.Events;
import com.ronem.carwash.utils.ItemDividerDecoration;
import com.ronem.carwash.utils.MetaData;
import com.ronem.carwash.utils.RecyclerItemClickListener;
import com.ronem.carwash.utils.SessionManager;
import com.ronem.carwash.view.AboutUs;
import com.ronem.carwash.view.FinishedOrderListActivity;
import com.ronem.carwash.view.MyAppointmentActivity;
import com.ronem.carwash.view.OrderRejectedListActivity;
import com.ronem.carwash.view.appointment.AppointmentActivity;
import com.ronem.carwash.view.current_order.CurrentOrderActivity;
import com.ronem.carwash.view.editprofile.CustomerProfile;
import com.ronem.carwash.view.login.LoginRegisterCustomerActivity;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by ram on 8/12/17.
 */

public class DashboardStationSalesman extends AppCompatActivity implements RecyclerItemClickListener.OnItemClickListener {

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.toolbar_title_tv)
    TextView toolbarTitleView;

    @Bind(R.id.home_customer_layout)
    LinearLayout homeCustomerLayout;

    @Bind(R.id.drawer_layout)
    DrawerLayout drawerLayout;
    @Bind(R.id.nav_view)
    NavigationView navigationView;


    private TextView emailTv;
    private TextView contactTv;

    private List<NavItem> items;
    private NavAdapter navAdapter;

    private SessionManager sessionManager;
    public static String userType;
    public static SalesManStation salesManStation;
    private boolean CURRENT_LANGUAGE;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dashboard);
        ButterKnife.bind(this);
        sessionManager = new SessionManager(this);
        userType = sessionManager.getUserType();
        salesManStation = sessionManager.getClient();
        homeCustomerLayout.setVisibility(View.GONE);
        CURRENT_LANGUAGE = sessionManager.getCurrentLanguage();
        settingToolbar();
        setUpNavigationMenu();

    }


    private void settingToolbar() {
        //setting toolbar
        setSupportActionBar(toolbar);

        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (NullPointerException ne) {
            ne.printStackTrace();
        }

        updateToolBarTitle();
    }


    private void updateToolBarTitle() {
        if (CURRENT_LANGUAGE) {
            toolbarTitleView.setText(getString(R.string.app_name_arabic));
        } else {
            toolbarTitleView.setText(getString(R.string.app_name));
        }
    }

    private void setUpNavigationMenu() {
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.setDrawerListener(toggle);
        toggle.syncState();

        emailTv = (TextView) navigationView.findViewById(R.id.user_name);
        contactTv = (TextView) navigationView.findViewById(R.id.user_contact);

        /**
         * setting the navigation items to the navigation view
         */
        items = MetaData.getnavItems(userType, CURRENT_LANGUAGE);
        RecyclerView navRecyclerView = (RecyclerView) navigationView.findViewById(R.id.nav_recycler_view);
        navRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        navRecyclerView.addItemDecoration(new ItemDividerDecoration(this, null));
        navRecyclerView.setHasFixedSize(true);
        navRecyclerView.addItemDecoration(new ItemDividerDecoration(this, null));
        navAdapter = new NavAdapter(items);
        navRecyclerView.setAdapter(navAdapter);
        navRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(this, this));

        RMSwitch languageSwitch = (RMSwitch) navigationView.findViewById(R.id.language_switch);
        languageSwitch.setChecked(CURRENT_LANGUAGE);
        languageSwitch.addSwitchObserver(new RMSwitch.RMSwitchObserver() {
            @Override
            public void onCheckStateChange(RMSwitch switchView, boolean isChecked) {
                CURRENT_LANGUAGE = isChecked;

                //we assumed true as arabic
                //and false as english
                if (CURRENT_LANGUAGE) {
                    sessionManager.switchToArabic();
                } else {
                    sessionManager.switchToEnglish();
                }

                //update toolbar title
                updateToolBarTitle();

                //send update to the subscribers
                EventBus.post(new Events.LanguageChangeEvent(CURRENT_LANGUAGE));

                //update navigation
                items.clear();
                items.addAll(MetaData.getnavItems(userType, CURRENT_LANGUAGE));
                navAdapter.notifyDataSetChanged();
            }
        });

    }

    private void updateUserInfo() {
        emailTv.setText(salesManStation.getEmail());
        contactTv.setText(salesManStation.getMobileNumber());
    }

    @Override
    public void onItemClick(RecyclerView recyclerView, View view, int position) {
        drawerLayout.closeDrawer(GravityCompat.START);
        NavItem ni = items.get(position);
        switch (ni.getTitle()) {
            case MetaData.ITEM_MY_APPOINTMENTS:
            case MetaData.ITEM_MY_APPOINTMENTS_ARABIC:
                startActivity(new Intent(this, MyAppointmentActivity.class));
                break;
            case MetaData.ITEM_FINISHED_ORDER:
            case MetaData.ITEM_FINISHED_ORDER_ARABIC:
                startActivity(new Intent(this, FinishedOrderListActivity.class));
                break;

            case MetaData.ITEM_REJECTED_ORDER:
            case MetaData.ITEM_REJECTED_ORDER_ARABIC:
                startActivity(new Intent(this, OrderRejectedListActivity.class));
                break;

            case MetaData.ITEM_NOTIFICATIONS:
            case MetaData.ITEM_NOTIFICATIONS_ARABIC:
                Toast.makeText(getApplicationContext(), "Under construction", Toast.LENGTH_SHORT).show();
                break;
            case MetaData.ITEM_EDIT_PROFILE:
            case MetaData.ITEM_EDIT_PROFILE_ARABIC:
                startActivity(new Intent(DashboardStationSalesman.this, CustomerProfile.class));
                break;
            case MetaData.ITEM_ABOUT_US:
            case MetaData.ITEM_ABOUT_US_ARABIC:
                Intent aboutUsIntent = new Intent(this, AboutUs.class);
                aboutUsIntent.putExtra("title", ni.getTitle());
                startActivity(aboutUsIntent);
                break;
            case MetaData.ITEM_LOG_OUT:
            case MetaData.ITEM_LOG_OUT_ARABIC:
                DatabaseReference dbr;

                //take node reference according to the user type
                if (userType.equals(MetaData.USER_TYPE_CLIENT_SALESMAN)) {
                    dbr = FirebaseDatabase.getInstance().getReference().child(MetaData.AVAILABLE_SALESMANS);

                } else {
                    dbr = FirebaseDatabase.getInstance().getReference().child(MetaData.AVAILABLE_STATIONS);
                }

                dbr.child(salesManStation.getUsername()).removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        sessionManager.logOut();
                        Intent i = new Intent(DashboardStationSalesman.this, LoginRegisterCustomerActivity.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(i);
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {

                        Toast.makeText(getApplicationContext(), CURRENT_LANGUAGE ? getString(R.string.cannot_log_out_arabic) : getString(R.string.cannot_log_out), Toast.LENGTH_SHORT).show();
                    }
                });
                break;
        }
    }


    @OnClick(R.id.btn_current_order)
    public void onBtnCurrentOrderClicked() {
        startActivity(new Intent(this, CurrentOrderActivity.class));
    }

    @OnClick(R.id.btn_appointment_order)
    public void onBtnAppointmentClicked() {
        startActivity(new Intent(this, AppointmentActivity.class));
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateUserInfo();

    }


}
