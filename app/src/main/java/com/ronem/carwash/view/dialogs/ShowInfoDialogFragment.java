package com.ronem.carwash.view.dialogs;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.ronem.carwash.R;
import com.ronem.carwash.adapters.CarTypeAdapter;
import com.ronem.carwash.adapters.NoOfCarsAdapter;
import com.ronem.carwash.adapters.ServiceTypeAdapter;
import com.ronem.carwash.interfaces.CarServiceDateListener;
import com.ronem.carwash.interfaces.ShowDialogEventListener;
import com.ronem.carwash.model.CarType;
import com.ronem.carwash.model.ServiceType;
import com.ronem.carwash.model.users.Customer;
import com.ronem.carwash.utils.MetaData;
import com.ronem.carwash.utils.MyAnimation;
import com.ronem.carwash.utils.SessionManager;

import java.sql.Time;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by ram on 9/9/17.
 */

public class ShowInfoDialogFragment
        extends BaseDialogFragment
        implements AdapterView.OnItemSelectedListener {
    @Bind(R.id.brief_info_layout)
    LinearLayout breifInfoLayout;
    @Bind(R.id.detail_info_layout)
    LinearLayout detailInfoLayout;
    @Bind(R.id.distance)
    TextView distanceView;
    @Bind(R.id.time_to_reach)
    TextView timeToReach;
    @Bind(R.id.access_time)
    LinearLayout accesTimeLayout;
    @Bind(R.id.time_to_reach_location)
    TextView location;
    @Bind(R.id.car_washer)
    TextView carWasherV;
    @Bind(R.id.dialog_btn_show_more)
    Button btnRequest;
    @Bind(R.id.dialog_btn_close)
    Button btnClose;

    //detail part
    @Bind(R.id.station_info)
    TextView stationInfo;
    @Bind(R.id.car_washer_name)
    TextView detailStationName;
    @Bind(R.id.car_washer_contact)
    TextView detailStationContact;

    @Bind(R.id.detail_time_layout)
    LinearLayout detailTimeLayout;
    @Bind(R.id.show_more_distance)
    TextView detailDistance;
    @Bind(R.id.show_more_estimation_time)
    TextView detailEstimationTime;
    @Bind(R.id.pick_date)
    TextView pickDate;
    @Bind(R.id.pick_time)
    TextView pickTime;
    @Bind(R.id.pick_date_card)
    CardView pickDateCard;


    @Bind(R.id.no_of_cars_title)
    TextView noOfCarsTitleTv;
    @Bind(R.id.spinner_number_of_cars)
    Spinner spinnerNoOfCars;

    @Bind(R.id.first_car_badge)
    TextView firstCarBadgeTv;
    @Bind(R.id.first_car_layout)
    CardView firstCarLayout;
    @Bind(R.id.car_type_1_title)
    TextView carType1TitleTv;
    @Bind(R.id.spinner_car_type_1)
    Spinner spinnerCarType1;
//    @Bind(R.id.service_type_1_title)
//    TextView serviceType1TitleTv;
//    @Bind(R.id.spinner_service_type1)
//    Spinner spinnerServiceTYpe1;

    @Bind(R.id.second_car_badge)
    TextView secondCarBadgeTv;
    @Bind(R.id.second_car_layout)
    CardView secondCarLayout;
    @Bind(R.id.car_type_2_title)
    TextView carType2TitleTv;
    @Bind(R.id.spinner_car_type_2)
    Spinner spinnerCarType2;
//    @Bind(R.id.service_type_2_title)
//    TextView serviceType2TitleTv;
//    @Bind(R.id.spinner_service_type2)
//    Spinner spinnerServiceTYpe2;

    @Bind(R.id.third_car_badge)
    TextView thirdCarBadgeTv;
    @Bind(R.id.third_car_layout)
    CardView thirdCarLayout;
    @Bind(R.id.car_type_3_title)
    TextView carType3TitleTv;
    @Bind(R.id.spinner_car_type_3)
    Spinner spinnerCarType3;
//    @Bind(R.id.service_type_3_title)
//    TextView serviceType3TitleTv;
//    @Bind(R.id.spinner_service_type3)
//    Spinner spinnerServiceTYpe3;

    @Bind(R.id.pick_date_title)
    TextView pickDateTitleTv;
    @Bind(R.id.pick_time_title)
    TextView pickTimeTitleTv;
    @Bind(R.id.btn_make_order)
    Button btnMakeRequest;

    private List<Integer> noOfcars;
    private List<CarType> carTypes1, carTypes2, carTypes3;
    //    private List<ServiceType> serviceTypes1, serviceTypes2, serviceTypes3;
    private CarType carType1, carType2, carType3;
    //    private ServiceType serviceType1, serviceType2, serviceType3;
    private String mrqst;
    private String close;
    private HashMap<String, HashMap<String, String>> cars;

    private int noOfCar;
    private String date, time;
    private String stationName;
    private String distance;
    private String duration;
    private String address;
    private String contact;
    private int totalcost = 0;
    int orderType;

    private SessionManager sessionManager;
    private boolean CURRENT_LANGUAGE;
    private Customer customer;

    private MyAnimation animation;


    private ShowDialogEventListener listener;
    private CarServiceDateListener carServiceDateListener;


    public void setOnShowDialogEventListener(ShowDialogEventListener listener) {
        this.listener = listener;
    }

    public void setOnCarServiceDateListener(CarServiceDateListener listener) {
        this.carServiceDateListener = listener;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View vi = inflater.inflate(R.layout.my_dialog, container, false);
        ButterKnife.bind(this, vi);
        animation = new MyAnimation();
        sessionManager = new SessionManager(getContext());
        customer = sessionManager.getCustomer();

        CURRENT_LANGUAGE = sessionManager.getCurrentLanguage();

        getIntentValue();
        return vi;
    }

    private void getIntentValue() {
        Bundle box = getArguments();
        orderType = box.getInt("order_type");
        stationName = box.getString("station");
        distance = box.getString("distance");
        duration = box.getString("time");
        address = box.getString("station_address");
        contact = box.getString("station_contact");
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        detailInfoLayout.setVisibility(View.GONE);
        HashMap<String, HashMap<String, String>> cars = customer.getCars();

        noOfcars = new ArrayList<>();
        int nc = 0;
        List<CarType> allCars = new ArrayList<>();
        for (Map.Entry<String, HashMap<String, String>> _car : cars.entrySet()) {
            HashMap<String, String> car = _car.getValue();
            nc++;
            noOfcars.add(nc);

            String carId = car.get(MetaData.KEY.CAR_ID);
            String carName = car.get(MetaData.KEY.CAR_TYPE);
            int carPrice = getCarPrice(carId);
            String carColor = car.get(MetaData.KEY.CAR_COLOR);
            String carPlate = car.get(MetaData.KEY.CAR_PLATE_NO);

            CarType ct = new CarType(carId, carName, 0, String.valueOf(carPrice), false, carColor, carPlate);
            allCars.add(ct);
        }
        spinnerNoOfCars.setAdapter(new NoOfCarsAdapter(noOfcars, getContext()));
        spinnerNoOfCars.setOnItemSelectedListener(this);

        carTypes1 = allCars;
        carTypes2 = allCars;
        carTypes3 = allCars;
//        serviceTypes1 = MetaData.getServiceType();
//        serviceTypes2 = MetaData.getServiceType();
//        serviceTypes3 = MetaData.getServiceType();


        spinnerCarType1.setAdapter(new CarTypeAdapter(carTypes1, getContext()));
        spinnerCarType2.setAdapter(new CarTypeAdapter(carTypes2, getContext()));
        spinnerCarType3.setAdapter(new CarTypeAdapter(carTypes3, getContext()));

//        spinnerServiceTYpe1.setAdapter(new ServiceTypeAdapter(serviceTypes1, getContext()));
//        spinnerServiceTYpe2.setAdapter(new ServiceTypeAdapter(serviceTypes2, getContext()));
//        spinnerServiceTYpe3.setAdapter(new ServiceTypeAdapter(serviceTypes3, getContext()));

        spinnerCarType1.setOnItemSelectedListener(this);
        spinnerCarType2.setOnItemSelectedListener(this);
        spinnerCarType3.setOnItemSelectedListener(this);

//        spinnerServiceTYpe1.setOnItemSelectedListener(this);
//        spinnerServiceTYpe2.setOnItemSelectedListener(this);
//        spinnerServiceTYpe3.setOnItemSelectedListener(this);

        updateUILanguage();
        setDetail();


    }

    private int getCarPrice(String carId) {
        if (carId.equals("1")) {
            return 25;
        } else if (carId.equals("2")) {
            return 30;
        } else {
            return 20;
        }
    }

    private int getServicePrice(String serviceID) {
        if (serviceID.equals("1")) {
            return 0;
        } else if (serviceID.equals("2")) {
            return 5;
        } else {
            return 10;
        }
    }


    private void updateUILanguage() {
        String ok = CURRENT_LANGUAGE ? getString(R.string.show_more_arabic) :
                getString(R.string.show_more);
        btnRequest.setText(ok);

        close = CURRENT_LANGUAGE ? getString(R.string.close_arabic) :
                getString(R.string.close);
        btnClose.setText(close);

        String si = CURRENT_LANGUAGE ? getString(R.string.station_info_arabic) : getString(R.string.station_info);
        stationInfo.setText(si);

        String noCTi = CURRENT_LANGUAGE ? getString(R.string.number_of_cars_arabic) : getString(R.string.number_of_cars);
        noOfCarsTitleTv.setText(noCTi);

        String cb1 = CURRENT_LANGUAGE ? getString(R.string.car_first_arabic) : getString(R.string.car_first);
        String cb2 = CURRENT_LANGUAGE ? getString(R.string.car_second_arabic) : getString(R.string.car_second);
        String cb3 = CURRENT_LANGUAGE ? getString(R.string.car_third_arabic) : getString(R.string.car_third);

        String ct = CURRENT_LANGUAGE ? getString(R.string.car_type_arabic) : getString(R.string.car_type);
        String st = CURRENT_LANGUAGE ? getString(R.string.service_type_arabic) : getString(R.string.service_type);

        firstCarBadgeTv.setText(cb1);
        secondCarBadgeTv.setText(cb2);
        thirdCarBadgeTv.setText(cb3);

        carType1TitleTv.setText(ct);
        carType2TitleTv.setText(ct);
        carType3TitleTv.setText(ct);

//        serviceType1TitleTv.setText(st);
//        serviceType2TitleTv.setText(st);
//        serviceType3TitleTv.setText(st);

        String pd = CURRENT_LANGUAGE ? getString(R.string.date_arabic) : getString(R.string.date);
        String pt = CURRENT_LANGUAGE ? getString(R.string.time__arabic) : getString(R.string.time_);
        mrqst = CURRENT_LANGUAGE ? getString(R.string.request_an_order_arabic) : getString(R.string.request_an_order);

        pickDateTitleTv.setText(pd);
        pickTimeTitleTv.setText(pt);
        btnMakeRequest.setText(mrqst);

    }

    private void setDetail() {


        carWasherV.setText(stationName);
        location.setText(address);
        distanceView.setText(distance);
        timeToReach.setText(duration);

        detailStationName.setText(stationName);
        detailStationContact.setText(contact);
        detailDistance.setText(distance);

        String et = CURRENT_LANGUAGE ? getString(R.string.estimation_time_arabic, duration) : getString(R.string.estimation_time, duration);
        detailEstimationTime.setText(et);

        //1 its live
        if (orderType == 1) {
            pickDateCard.setVisibility(View.GONE);

            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss", Locale.UK);
            Date _date = new Date();
            String strDate = dateFormat.format(_date);

            String[] dt = strDate.split(" ");
            date = dt[0];
            time = dt[1];
        } else {
            distanceView.setVisibility(View.GONE);
            accesTimeLayout.setVisibility(View.GONE);
            detailTimeLayout.setVisibility(View.GONE);
        }

    }

    @OnClick(R.id.dialog_btn_show_more)
    public void onDialogBtnShowMoreClicked() {
        animation.collapseView(breifInfoLayout);
        animation.expandView(detailInfoLayout);
    }

    @OnClick(R.id.dialog_btn_close)
    public void onDialogBtnCloseClicked() {
        dismiss();
    }

    @OnClick(R.id.dialog_btn_cross)
    public void onDialogBtnCrossClicked() {
        dismiss();
    }

    @OnClick(R.id.btn_make_order)
    public void onBtnMakeOrderClicked() {
        cars = new HashMap<>();
        if (noOfCar == 1) {
            cars.put("Car1", getCar(carType1/*, serviceType1*/));
        } else if (noOfCar == 2) {
            cars.put("Car1", getCar(carType1/*, serviceType1*/));
            cars.put("Car2", getCar(carType2/*, serviceType2*/));
        } else if (noOfCar == 3) {
            cars.put("Car1", getCar(carType1/*, serviceType1*/));
            cars.put("Car2", getCar(carType2/*, serviceType2*/));
            cars.put("Car3", getCar(carType3/*, serviceType3*/));
        }
        if (TextUtils.isEmpty(date)) {
            String msg = CURRENT_LANGUAGE ? getString(R.string.select_date_arabic) : getString(R.string.select_date);
            Toast.makeText(getContext(), msg, Toast.LENGTH_SHORT).show();
        } else if (TextUtils.isEmpty(time)) {
            String msg = CURRENT_LANGUAGE ? getString(R.string.select_time_arabic) : getString(R.string.select_time);
            Toast.makeText(getContext(), msg, Toast.LENGTH_SHORT).show();
        } else {
            showConfirmDialog();
        }


    }

    private void showConfirmDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Total Charge SAR " + getTheTotalPrice());
        StringBuilder msgBuilder = new StringBuilder();
        if (noOfCar == 1) {
            String cs = carType1.getType() + " : " + carType1.getColor() + " : " + carType1.getPlateNo();
            String chrg = " SAR(" + getCarPrice(carType1.getId()) + /*"+" + getServicePrice(serviceType1.getServiceTypeId()) +*/ ")";
            msgBuilder.append(cs + chrg + "\n");
        } else if (noOfCar == 2) {
            String cs1 = carType1.getType() + " : " + carType1.getColor() + " : " + carType1.getPlateNo();
            String chrg1 = " SAR(" + getCarPrice(carType1.getId()) +/* "+" + getServicePrice(serviceType1.getServiceTypeId()) +*/ ")";

            String cs2 = carType2.getType() + " : " + carType2.getColor() + " : " + carType2.getPlateNo();
            String chrg2 = " SAR(" + getCarPrice(carType2.getId()) +/* "+" + getServicePrice(serviceType2.getServiceTypeId()) +*/ ")";

            msgBuilder.append(cs1 + chrg1 + "\n");
            msgBuilder.append(cs2 + chrg2 + "\n");
        } else {
            String cs1 = carType1.getType() + " : " + carType1.getColor() + " : " + carType1.getPlateNo();
            String chrg1 = " SAR(" + getCarPrice(carType1.getId()) +/* "+" + getServicePrice(serviceType1.getServiceTypeId()) +*/ ")";

            String cs2 = carType2.getType() + " : " + carType2.getColor() + " : " + carType2.getPlateNo();
            String chrg2 = " SAR(" + getCarPrice(carType2.getId()) +/* "+" + getServicePrice(serviceType2.getServiceTypeId()) +*/ ")";

            String cs3 = carType3.getType() + " : " + carType3.getColor() + " : " + carType3.getPlateNo();
            String chrg3 = " SAR(" + getCarPrice(carType3.getId()) +/* "+" + getServicePrice(serviceType3.getServiceTypeId()) +*/ ")";

            msgBuilder.append(cs1 + chrg1 + "\n");
            msgBuilder.append(cs2 + chrg2 + "\n");
            msgBuilder.append(cs3 + chrg3 + "\n");
        }
        builder.setMessage(msgBuilder.toString());
        builder.setPositiveButton(mrqst, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                String dateTime = date + " " + time;
                carServiceDateListener.onCarTypeServiceDateReceived(noOfCar, cars, dateTime);
                dismiss();
            }
        });

        builder.setNegativeButton(close, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private HashMap<String, String> getCar(CarType ct/*, ServiceType st*/) {
        HashMap<String, String> car = new HashMap<>();
        car.put(MetaData.KEY.CAR_ID, ct.getId());
        car.put(MetaData.KEY.CAR_TYPE, ct.getType());
        car.put(MetaData.KEY.CAR_PRICE, ct.getPrice());
        car.put(MetaData.KEY.CAR_COLOR, ct.getColor());
        car.put(MetaData.KEY.CAR_PLATE_NO, ct.getPlateNo());

//        car.put(MetaData.KEY.SERVICE_ID, st.getServiceTypeId());
//        car.put(MetaData.KEY.SERVIE_TYPE, st.getServiceType());
//        car.put(MetaData.KEY.SERVICE_CHARGE, st.getServiceCharge());

        car.put(MetaData.KEY.SERVICE_ID, "");
        car.put(MetaData.KEY.SERVIE_TYPE, "");
        car.put(MetaData.KEY.SERVICE_CHARGE, "");
        return car;
    }


    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        switch (adapterView.getId()) {
            case R.id.spinner_number_of_cars:
                noOfCar = noOfcars.get(i);
                showNoOfcars(noOfCar);
                break;
            case R.id.spinner_car_type_1:
                carType1 = carTypes1.get(i);
                break;
            case R.id.spinner_car_type_2:
                carType2 = carTypes2.get(i);
                break;
            case R.id.spinner_car_type_3:
                carType3 = carTypes3.get(i);
                break;
//            case R.id.spinner_service_type1:
//                serviceType1 = serviceTypes1.get(i);
//                break;
//            case R.id.spinner_service_type2:
//                serviceType2 = serviceTypes2.get(i);
//                break;
//            case R.id.spinner_service_type3:
//                serviceType3 = serviceTypes3.get(i);
//                break;
        }
    }

    private int getTheTotalPrice() {
        int carPrice;
//        int serviceCharge;
        switch (noOfCar) {
            case 1:
                carPrice = getCarPrice(carType1.getId());
//                serviceCharge = getServicePrice(serviceType1.getServiceTypeId());
                totalcost = carPrice/* + serviceCharge*/;
                break;
            case 2:

                carPrice = getCarPrice(carType1.getId());
                carPrice = carPrice + getCarPrice(carType2.getId());

//                serviceCharge = getServicePrice(serviceType1.getServiceTypeId());
//                serviceCharge = serviceCharge + getServicePrice(serviceType2.getServiceTypeId());


                totalcost = carPrice /*+ serviceCharge*/;
                break;
            case 3:

                carPrice = getCarPrice(carType1.getId());
                carPrice = carPrice + getCarPrice(carType2.getId());
                carPrice = carPrice + getCarPrice(carType3.getId());

//                serviceCharge = getServicePrice(serviceType1.getServiceTypeId());
//                serviceCharge = serviceCharge + getServicePrice(serviceType2.getServiceTypeId());
//                serviceCharge = serviceCharge + getServicePrice(serviceType3.getServiceTypeId());

                totalcost = carPrice /*+ serviceCharge*/;
                break;
        }
//        totalCostView.setText(getString(R.string.total_charge, String.valueOf(totalcost)));
        return totalcost;
    }

    private void showNoOfcars(int noOfCar) {
        if (noOfCar == 1) {
            secondCarLayout.setVisibility(View.GONE);
            thirdCarLayout.setVisibility(View.GONE);
        } else if (noOfCar == 2) {
            thirdCarLayout.setVisibility(View.GONE);
            secondCarLayout.setVisibility(View.VISIBLE);
        } else if (noOfCar == 3) {
            thirdCarLayout.setVisibility(View.VISIBLE);
            secondCarLayout.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    @OnClick(R.id.pick_date)
    public void onPickDateClicked() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());

        DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                int m = monthOfYear + 1;
                date = year + "/" + m + "/" + dayOfMonth;
                pickDate.setText(date);
            }

        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));

        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        datePickerDialog.show();
    }

    @OnClick(R.id.pick_time)
    public void onPickTimeClicked() {
        Calendar calendar = Calendar.getInstance();
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker = new TimePickerDialog(getContext(), new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int hr, int min) {
                Time _t = new Time(hr, min, 0);

                //little h uses 12 hour format and big H uses 24 hour format
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("h:mma", Locale.CANADA);

                //format takes in a Date, and Time is a sublcass of Date
                time = simpleDateFormat.format(_t);

                pickTime.setText(time);
            }
        }, hour, minute, false);//Yes 24 hour time
        mTimePicker.show();
    }
}
