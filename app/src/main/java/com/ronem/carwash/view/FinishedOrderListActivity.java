package com.ronem.carwash.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.ronem.carwash.R;
import com.ronem.carwash.adapters.CustomerStationRequestAdapter;
import com.ronem.carwash.model.users.CustomerStationRequest;
import com.ronem.carwash.utils.EventBus;
import com.ronem.carwash.utils.MetaData;
import com.ronem.carwash.utils.SessionManager;
import com.ronem.carwash.view.dashboard.DashboardStationSalesman;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by ram on 11/13/17.
 */

public class FinishedOrderListActivity extends AppCompatActivity {
    @Bind(R.id.toolbar)Toolbar toolbar;

    @Bind(R.id.order_recycler_view)
    RecyclerView recyclerView;
    @Bind(R.id.recycler_title)
    TextView recyclerTitle;
    @Bind(R.id.empty_recycler)
    TextView emptyTv;

    private List<CustomerStationRequest> finishedOrders;
    private CustomerStationRequestAdapter adapter;
    private boolean CURRENT_LANGUAGE;
    private SessionManager sessionManager;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_live_order);
        ButterKnife.bind(this);
        sessionManager = new SessionManager(this);
        CURRENT_LANGUAGE = sessionManager.getCurrentLanguage();

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.hasFixedSize();
        finishedOrders = new ArrayList<>();
        adapter = new CustomerStationRequestAdapter(finishedOrders, MetaData.ORDER_STATUS_FINISHED);

        recyclerView.setAdapter(adapter);


        updateRecyclerTitle();

        DatabaseReference dbr;
        if (DashboardStationSalesman.userType.equals(MetaData.USER_TYPE_CLIENT_STATION)) {
            dbr = FirebaseDatabase.getInstance().getReference().child(MetaData.STATION_FINISHED_TASK);
        } else {
            dbr = FirebaseDatabase.getInstance().getReference().child(MetaData.SALESMAN_FINISHED_TASK);
        }
        dbr.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    for (DataSnapshot ds : dataSnapshot.getChildren()) {
                        CustomerStationRequest csr = ds.getValue(CustomerStationRequest.class);
                        if ((csr.getsUserName())
                                .equals(DashboardStationSalesman.salesManStation.getUsername())) {
                            finishedOrders.add(csr);
                            adapter.notifyItemInserted(finishedOrders.size() - 1);
                        }
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==android.R.id.home)this.finish();

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ButterKnife.unbind(this);
    }

    private void updateRecyclerTitle() {

        String rt = CURRENT_LANGUAGE ? getString(R.string.finished_task_arabic) : getString(R.string.finished_task);
        recyclerTitle.setText(rt);

        String wfa = CURRENT_LANGUAGE ? getString(R.string.waiting_for_finished_job_arabic) : getString(R.string.waiting_for_finished_job);
        emptyTv.setText(wfa);
    }
}
