package com.ronem.carwash.view;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.ronem.carwash.R;
import com.ronem.carwash.utils.BasicUtilityMethods;
import com.ronem.carwash.utils.MetaData;
import com.ronem.carwash.utils.MyAnimation;
import com.ronem.carwash.utils.SessionManager;
import com.ronem.carwash.view.dashboard.DirectionAdView;
import com.ronem.carwash.view.dashboard.DirectionPresenter;
import com.ronem.carwash.view.dashboard.DirectionPresenterImpl;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by ram on 8/12/17.
 */

public class OrderMapActivity extends AppCompatActivity implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        com.google.android.gms.location.LocationListener,
        DirectionAdView {

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.distance_time_info_layout)
    LinearLayout distanceTimeInfoLayout;
    @Bind(R.id.dialog_time_away)
    TextView dialogTimeAwayTextView;
    @Bind(R.id.dialog_distance)
    TextView dialogDistanceTextView;
    @Bind(R.id.btn_timer)
    Button btnStartWork;
    @Bind(R.id.timer_view)
    TextView timerView;


    private GoogleApiClient googleApiClient;
    private LocationRequest locationRequest;
    private GoogleMap googleMap;
    private DirectionPresenter presenter;
    private Marker mPositionMarker;
    private Location myLocation;
    private LatLng myLatlang;
    private LatLng customerLatlang;
    private LatLng contactLatlang;

    private String customerName;

    private SessionManager sessionManager;
    private String enableInternet;
    private boolean arabic;
    private boolean isCameraAnimated = false;
    private boolean isRouteDrawnMeToContact = false;
    private boolean isDestinationReached = false;
    private boolean shoudShowDistanceTimeInfo = false;
    private String key;
    private MyAnimation animation;

    private long startTime = 0L;
    private Handler customHandler = new Handler();
    long timeInMilliseconds = 0L;
    long timeSwapBuff = 0L;
    long updatedTime = 0L;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.delivered_layout);

        ButterKnife.bind(this);
        sessionManager = new SessionManager(this);
        animation = new MyAnimation();

        arabic = sessionManager.getCurrentLanguage();
        key = getString(R.string.my_google_api_key);
        settingToolbar();

        presenter = new DirectionPresenterImpl();
        presenter.onAddDirectionView(this);
        BasicUtilityMethods.checkifGPSisEnabled(this);

        double cusLati = getIntent().getDoubleExtra(MetaData.KEY_LATITUDE, 0.0);
        double cusLongi = getIntent().getDoubleExtra(MetaData.KEY_LONGITUDE, 0.0);
        double contactLati = getIntent().getDoubleExtra(MetaData.KEY_CONTACT_LATITUDE, 0.0);
        double contactLongi = getIntent().getDoubleExtra(MetaData.KEY_CONTACT_LONGITUDE, 0.0);
        customerName = getIntent().getStringExtra(MetaData.USER_TYPE_CUSTOMER);
        shoudShowDistanceTimeInfo = getIntent().getBooleanExtra(MetaData.KEY_SHOULD_SHOW_INFO, false);

        if (shoudShowDistanceTimeInfo) {
            animation.expandView(distanceTimeInfoLayout);
        }

        customerLatlang = new LatLng(cusLati, cusLongi);
        contactLatlang = new LatLng(contactLati, contactLongi);

        createGoogleApiClient();
        googleApiClient.connect();

        locationRequest = BasicUtilityMethods.createLocationRequest();

        loadmap();
    }

    private void createGoogleApiClient() {
        googleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
    }

    private void loadmap() {
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ButterKnife.unbind(this);
    }

    private void settingToolbar() {
        //setting toolbar
        setSupportActionBar(toolbar);

        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (NullPointerException ne) {
            ne.printStackTrace();
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        requestLocationupdate();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location l) {
        if (l == null)
            return;


        myLocation = l;
        myLatlang = new LatLng(myLocation.getLatitude(), myLocation.getLongitude());

        if (mPositionMarker != null) {
            mPositionMarker.remove();
        }
        MarkerOptions markerOption = new MarkerOptions()
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED))
                .position(myLatlang);

        mPositionMarker = googleMap.addMarker(markerOption);

        if (!isCameraAnimated) {
            CameraPosition newCamPos = new CameraPosition
                    .Builder()
                    .target(myLatlang)
                    .zoom(MetaData.MAP_ZOOM)
                    .bearing(300)
                    .tilt(0)
                    .build();
            googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(newCamPos), 4000, null);
            isCameraAnimated = true;
        }

        //download the path polygon
        //check if there is contact point
        //if there is contact point then draw route between me and contactpoint
        //if not then draw route between me and customer
//        if (contactLatang.latitude != 0.0 && contactLatlang.longitude != 0.0) {
        String url = BasicUtilityMethods.getUrl(myLatlang, contactLatlang, key);
        if (!isRouteDrawnMeToContact) {
            if (BasicUtilityMethods.isNetworkOnline(OrderMapActivity.this)) {
                presenter.onGetPolyLineOptions(url);
                isRouteDrawnMeToContact = true;
            } else {
                enableInternetMsg();
            }
        }

        if (shoudShowDistanceTimeInfo) {
            // if route is drawn then find the time and distance
            if (isRouteDrawnMeToContact) {
                if (!isDestinationReached)
                    presenter.onGetDistanceTime(url);
            }
        }
//        } else {
//            if (!isRouteDrawnMeToContact) {
//                String url = BasicUtilityMethods.getUrl(myLatlang, customerLatlang, key);
//                if (BasicUtilityMethods.isNetworkOnline(OrderMapActivity.this)) {
//                    presenter.onGetPolyLineOptions(url);
//                    isRouteDrawnMeToContact = true;
//                } else {
//                    enableInternetMsg();
//                }
//            }
//        }

    }

    private void enableInternetMsg() {
        Toast.makeText(getApplicationContext(), arabic ? getString(R.string.enable_internet_arabic) : getString(R.string.enable_internet), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;


        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        googleMap.setMyLocationEnabled(true);
        googleMap.animateCamera(CameraUpdateFactory.zoomTo(MetaData.MAP_ZOOM));


        /**customer marker**/
//        MarkerOptions customerMOption = new MarkerOptions()
//                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN))
//                .position(customerLatlang)
//                .title("Customer : " + customerName);
//        googleMap.addMarker(customerMOption);


        //if contact point is present then
        //place marker first and draw the route between customer and contact
        if (contactLatlang.latitude != 0.0 && contactLatlang.longitude != 0.0) {
            /**contact point marker*/
            MarkerOptions contactMOption = new MarkerOptions()
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE))
                    .position(contactLatlang)
                    .title("Contact Point");
            googleMap.addMarker(contactMOption);

//            /**route between customer and contact**/
//            String url = BasicUtilityMethods.getUrl(customerLatlang, contactLatlang,key);
//            if (BasicUtilityMethods.isNetworkOnline(OrderMapActivity.this)) {
//                presenter.onGetPolyLineOptions(url);
//            } else {
//                enableInternetMsg();
//            }
        }

        if (shoudShowDistanceTimeInfo)
            googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                @Override
                public void onMapClick(LatLng latLng) {
                    if (distanceTimeInfoLayout.getVisibility() == View.VISIBLE) {
                        animation.collapseView(distanceTimeInfoLayout);
                    } else {
                        animation.expandView(distanceTimeInfoLayout);
                    }
                }
            });

    }

    @Override
    public void onPolyLineOptionReceived(PolylineOptions polylineOptions, String distance, String duration) {
        if (polylineOptions == null) {
            isRouteDrawnMeToContact = false;
        } else {
            googleMap.addPolyline(polylineOptions);
        }
    }

    @Override
    public void onTimeDistanceReceived(String distance, String time, int value) {
        if (!TextUtils.isEmpty(distance)) {
            dialogDistanceTextView.setText(distance);

            String msg = arabic ?
                    getString(R.string.time_away_arabic, time)
                    : getString(R.string.time_away, time);
            dialogTimeAwayTextView.setText(msg);
        }

        //check if the value is less than 10
        //indicate the distance is less than 10m
        if (value < 10) {
            isDestinationReached = true;
        }
    }

    @OnClick(R.id.btn_timer)
    public void onBtnTimerClicked() {
        String value = btnStartWork.getText().toString().trim();
        String startWork = getString(R.string.start_work);
        String stopWork = getString(R.string.stop_work);

        if (isDestinationReached) {
            if (value.equals(startWork)) {
                startTime = SystemClock.uptimeMillis();
                customHandler.postDelayed(updateTimerThread, 0);
                btnStartWork.setText(stopWork);
            } else if (value.equals(stopWork)) {
                customHandler.removeCallbacks(updateTimerThread);
                btnStartWork.setText("Task Completed");
            }
        } else {
            Toast.makeText(getApplicationContext(), "Destination not reached yet", Toast.LENGTH_SHORT).show();
        }
    }

    private Runnable updateTimerThread = new Runnable() {

        public void run() {

            timeInMilliseconds = SystemClock.uptimeMillis() - startTime;

            updatedTime = timeSwapBuff + timeInMilliseconds;

            int secs = (int) (updatedTime / 1000);
            int mins = secs / 60;
            secs = secs % 60;
            int milliseconds = (int) (updatedTime % 1000);
            timerView.setText("" + mins + ":"
                    + String.format("%02d", secs) + ":"
                    + String.format("%03d", milliseconds));
            customHandler.postDelayed(this, 0);
        }

    };


    @Override
    protected void onStop() {
        super.onStop();
        googleApiClient.disconnect();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (googleApiClient.isConnected()) {
            requestLocationupdate();
        }

    }

    @SuppressWarnings("MissingPermission")
    private void requestLocationupdate() {
        LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (googleApiClient.isConnected())
            LocationServices.FusedLocationApi.removeLocationUpdates(googleApiClient, this);
    }

    private void animateMarker(final Marker marker, final Location location) {
        final Handler handler = new Handler();
        final long start = SystemClock.uptimeMillis();
        final LatLng startLatLng = marker.getPosition();
        final double startRotation = marker.getRotation();
        final long duration = 500;

        final Interpolator interpolator = new LinearInterpolator();

        handler.post(new Runnable() {
            @Override
            public void run() {
                long elapsed = SystemClock.uptimeMillis() - start;
                float t = interpolator.getInterpolation((float) elapsed
                        / duration);

                double lng = t * location.getLongitude() + (1 - t)
                        * startLatLng.longitude;
                double lat = t * location.getLatitude() + (1 - t)
                        * startLatLng.latitude;

                float rotation = (float) (t * location.getBearing() + (1 - t)
                        * startRotation);

                marker.setPosition(new LatLng(lat, lng));
                marker.setRotation(rotation);

                if (t < 1.0) {
                    // Post again 16ms later.
                    handler.postDelayed(this, 16);
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home)
            onBackPressed();
        return super.onOptionsItemSelected(item);
    }
}
