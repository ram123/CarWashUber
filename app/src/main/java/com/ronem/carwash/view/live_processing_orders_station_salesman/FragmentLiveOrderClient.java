//package com.ronem.carwash.view.live_processing_orders_station_salesman;
//
//import android.content.Intent;
//import android.os.Bundle;
//import android.support.annotation.NonNull;
//import android.support.annotation.Nullable;
//import android.support.v4.app.Fragment;
//import android.support.v7.widget.LinearLayoutManager;
//import android.support.v7.widget.RecyclerView;
//import android.util.Log;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.RelativeLayout;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import com.google.android.gms.tasks.OnCompleteListener;
//import com.google.android.gms.tasks.OnFailureListener;
//import com.google.android.gms.tasks.Task;
//import com.google.firebase.database.DataSnapshot;
//import com.google.firebase.database.DatabaseError;
//import com.google.firebase.database.DatabaseReference;
//import com.google.firebase.database.FirebaseDatabase;
//import com.google.firebase.database.ValueEventListener;
//import com.ronem.carwash.R;
//import com.ronem.carwash.adapters.CustomerStationRequestAdapter;
//import com.ronem.carwash.interfaces.OrderChangeStatusListener;
//import com.ronem.carwash.model.users.CustomerStationRequest;
//import com.ronem.carwash.utils.EventBus;
//import com.ronem.carwash.utils.Events;
//import com.ronem.carwash.utils.MetaData;
//import com.ronem.carwash.utils.SessionManager;
//import com.ronem.carwash.view.OrderMapActivity;
//import com.ronem.carwash.view.dashboard.DashboardStationSalesman;
//import com.ronem.carwash.view.dialogs.OrderRequestDialog;
//import com.squareup.otto.Subscribe;
//
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
//import butterknife.Bind;
//import butterknife.ButterKnife;
//import butterknife.OnClick;
//
///**
// * Created by ram on 8/12/17.
// */
//
//public class FragmentLiveOrderClient extends Fragment implements OrderChangeStatusListener {
//
//    private String TAG = getClass().getSimpleName();
//
//    @Bind(R.id.btn_view_detail_order)
//    TextView btnViewLiveDetail;
//    @Bind(R.id.btn_accept_order)
//    TextView btnAcceptLiveOrder;
//    @Bind(R.id.btn_ignore_order)
//    TextView btnIgnoreLiveOrder;
//
//    @Bind(R.id.recycler_title)
//    TextView recyclerTitleTv;
//    @Bind(R.id.empty_recycler)
//    TextView emptyListView;
//    @Bind(R.id.order_recycler_view)
//    RecyclerView recyclerView;
//    @Bind(R.id.live_layout)
//    RelativeLayout livelayout;
//    @Bind(R.id.customer_name_tv)
//    TextView customerNameTv;
//    @Bind(R.id.customer_service_tv)
//    TextView customerServiceTv;
//
//    private CustomerStationRequest customerStationRequest;
//    private CustomerStationRequest liveRequest;
//    private ArrayList<String> userStack;
//    private List<CustomerStationRequest> appointments;
//    private CustomerStationRequestAdapter adapter;
//
//    private boolean CURRENT_LANGUAGE;
//    private SessionManager sessionManager;
//    private String customer, address, contact, ordermadeby, close;
//    private String orejected, oCompleted;
//
//
//    @Nullable
//    @Override
//    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        View root = inflater.inflate(R.layout.fragment_live_order, container, false);
//        ButterKnife.bind(this, root);
//        EventBus.register(this);
//        sessionManager = new SessionManager(getContext());
//        CURRENT_LANGUAGE = sessionManager.getCurrentLanguage();
//
//
//        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
//        recyclerView.hasFixedSize();
//        return root;
//    }
//
//    @Override
//    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
//        super.onViewCreated(view, savedInstanceState);
//        appointments = new ArrayList<>();
//        adapter = new CustomerStationRequestAdapter(appointments, MetaData.ORDER_STATUS_LIVE);
//        adapter.setLang(CURRENT_LANGUAGE);
//        recyclerView.setAdapter(adapter);
//        adapter.setOnOrderChangeStatusListener(this);
//
//        listeningToCustomers();
//        listeningToLiveCustomers();
//
//        updateRecyclerTitle();
//    }
//
//
//    private void listeningToLiveCustomers() {
//        if (DashboardStationSalesman.userType.equals(MetaData.USER_TYPE_CLIENT_SALESMAN)) {
//            Log.i(TAG, "Salesman");
//            DatabaseReference dbr = FirebaseDatabase
//                    .getInstance()
//                    .getReference()
//                    .child(MetaData.CUSTOMER_SALESMAN_LIVE_ORDER_REQUEST);
//
//            dbr.addValueEventListener(new ValueEventListener() {
//                @Override
//                public void onDataChange(DataSnapshot dataSnapshot) {
//                    Log.i(TAG, "There was live data");
//                    if (dataSnapshot.exists()) {
//                        for (DataSnapshot ds : dataSnapshot.getChildren()) {
//                            CustomerStationRequest csr = ds.getValue(CustomerStationRequest.class);
//                            if ((DashboardStationSalesman.salesManStation.getUsername()).equals(csr.getsUserName())) {
//                                Log.i(TAG, "username matched");
//
//
//                                liveRequest = csr;
//
//                                customerNameTv.setText(liveRequest.getcName());
//
//                                StringBuilder carBuilder = new StringBuilder();
//                                HashMap<String, HashMap<String, String>> cars = liveRequest.getCars();
//                                for (Map.Entry<String, HashMap<String, String>> entry : cars.entrySet()) {
//                                    HashMap<String, String> car = entry.getValue();
//                                    String carType = car.get(MetaData.KEY.CAR_TYPE);
//                                    String serviceType = car.get(MetaData.KEY.SERVIE_TYPE);
//                                    carBuilder.append(carType + " : " + serviceType + "\n");
//
//                                }
//
//                                customerServiceTv.setText(carBuilder.toString());
//
//                                //update the recycler view title and the empty
//                                updateRecyclerTitle();
//
//                                updateView();
//
//                                return;
//                            }
//                        }
//                    } else {
//                        livelayout.setVisibility(View.GONE);
//                    }
//                }
//
//                @Override
//                public void onCancelled(DatabaseError databaseError) {
//
//                }
//            });
//        }
//    }
//
//    private void updateView() {
//        if (getActivity() != null) {
//            String vd = CURRENT_LANGUAGE ? getString(R.string.view_detail_arabic) : getString(R.string.view_detail);
//            String ao = CURRENT_LANGUAGE ? getString(R.string.accept_order_arabic) : getString(R.string.accept_order);
//            String io = CURRENT_LANGUAGE ? getString(R.string.ignore_order_arabic) : getString(R.string.ignore_order);
//            String vr = CURRENT_LANGUAGE ? MetaData.BTN_VIEW_ROUTE_ARABIC : MetaData.BTN_VIEW_ROUTE;
//            String co = CURRENT_LANGUAGE ? MetaData.BTN_COMPLETE_ORDER_ARABIC : MetaData.BTN_COMPLETE_ORDER;
//            btnViewLiveDetail.setText(vd);
//
//            //for the live layout
//            switch (liveRequest.getAccepted()) {
//                case MetaData.ORDER_IDLE:
//                    livelayout.setVisibility(View.VISIBLE);
//                    btnIgnoreLiveOrder.setVisibility(View.VISIBLE);
//
//                    btnAcceptLiveOrder.setText(ao);
//                    btnIgnoreLiveOrder.setText(io);
//                    break;
//
//                case MetaData.ORDER_ACCEPTED:
//                    livelayout.setVisibility(View.VISIBLE);
//                    btnAcceptLiveOrder.setText(vr);
//                    btnIgnoreLiveOrder.setText(co);
//                    break;
//
//                case MetaData.ORDER_REJECTED:
//                    livelayout.setVisibility(View.GONE);
//                    break;
//            }
//        }
//    }
//
//    private void updateRecyclerTitle() {
//        if (getActivity() != null) {
//            String rt = CURRENT_LANGUAGE ? getString(R.string.appointments_arabic) : getString(R.string.appointments);
//            recyclerTitleTv.setText(rt);
//
//            String wfa = CURRENT_LANGUAGE ? getString(R.string.waiting_for_appintments_arabic) : getString(R.string.waiting_for_appintments);
//            emptyListView.setText(wfa);
//
//            customer = CURRENT_LANGUAGE ? getString(R.string.customer_arabic) : getString(R.string.customer);
//            address = CURRENT_LANGUAGE ? getString(R.string.d_address_arabic) : getString(R.string.d_address);
//            contact = CURRENT_LANGUAGE ? getString(R.string.d_contact_arabic) : getString(R.string.d_contact);
//            ordermadeby = CURRENT_LANGUAGE ? getString(R.string.d_order_made_by_arabic) : getString(R.string.d_order_made_by);
//            close = CURRENT_LANGUAGE ? getString(R.string.close_arabic) : getString(R.string.close);
//            orejected = CURRENT_LANGUAGE ? getString(R.string.order_rejected_arabic) : getString(R.string.order_rejected);
//            oCompleted = CURRENT_LANGUAGE ? getString(R.string.order_complete_arabic) : getString(R.string.order_complete);
//        }
//    }
//
//    private void listeningToCustomers() {
//        //storage to list the accepted users
//        userStack = new ArrayList<>();
//
//        DatabaseReference dbr;
//
//        if (DashboardStationSalesman.userType.equals(MetaData.USER_TYPE_CLIENT_STATION)) {
//            dbr = FirebaseDatabase
//                    .getInstance()
//                    .getReference()
//                    .child(MetaData.CUSTOMER_STATION_REQUEST);
//        } else {
//            dbr = FirebaseDatabase
//                    .getInstance()
//                    .getReference()
//                    .child(MetaData.CUSTOMER_SALESMAN_REQUEST);
//        }
//
//        dbr.addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(DataSnapshot dataSnapshot) {
//                Log.i("Client", "request received");
////                if (!accepted) {
//                if (dataSnapshot.exists()) {
//                    for (DataSnapshot ds : dataSnapshot.getChildren()) {
//                        customerStationRequest = ds.getValue(CustomerStationRequest.class);
//                        Log.i("csr", customerStationRequest.toString());
//                        if (customerStationRequest.getsUserName().equals(DashboardStationSalesman.salesManStation.getUsername())) {
//                            if (customerStationRequest.getAccepted() == MetaData.ORDER_IDLE) {
//                                if (!presentInStack(customerStationRequest.getcUserName())) {
//                                    appointments.add(customerStationRequest);
//                                    adapter.notifyItemInserted(appointments.size() - 1);
//                                }
//                            }
//                        }
//                    }
//
//                }
////                }
//            }
//
//            @Override
//            public void onCancelled(DatabaseError databaseError) {
//
//            }
//        });
//
//    }
//
//    private boolean presentInStack(String userName) {
//        for (String s : userStack) {
//            if (s.equals(userName)) {
//                return true;
//            }
//        }
//        userStack.add(userName);
//        return false;
//    }
//
//    @Override
//    public void onOrderAccepted(final CustomerStationRequest csr) {
//        final String childNode = csr.getcUserName() + csr.getsUserName();
//        DatabaseReference dbr;
//        if (DashboardStationSalesman.userType.equals(MetaData.USER_TYPE_CLIENT_STATION)) {
//            dbr = FirebaseDatabase.getInstance().getReference().child(MetaData.CUSTOMER_STATION_REQUEST);
//        } else {
//            dbr = FirebaseDatabase.getInstance().getReference().child(MetaData.CUSTOMER_SALESMAN_REQUEST);
//        }
//
//        csr.setAccepted(MetaData.ORDER_ACCEPTED);
//        dbr.child(childNode).setValue(csr).addOnCompleteListener(new OnCompleteListener<Void>() {
//            @Override
//            public void onComplete(@NonNull Task<Void> task) {
//                appointments.remove(csr);
//                adapter.notifyDataSetChanged();
//            }
//        }).addOnFailureListener(new OnFailureListener() {
//            @Override
//            public void onFailure(@NonNull Exception e) {
//                Toast.makeText(getContext(), "Something went wrong", Toast.LENGTH_SHORT).show();
//            }
//        });
//    }
//
//    @Override
//    public void onOrderRejected(final CustomerStationRequest csr) {
//        final String childNode = csr.getcUserName() + csr.getsUserName();
//        DatabaseReference dbr;
//        if (DashboardStationSalesman.userType.equals(MetaData.USER_TYPE_CLIENT_STATION)) {
//            dbr = FirebaseDatabase.getInstance().getReference().child(MetaData.CUSTOMER_STATION_REQUEST);
//        } else {
//            dbr = FirebaseDatabase.getInstance().getReference().child(MetaData.CUSTOMER_SALESMAN_REQUEST);
//        }
//
//        /**
//         * accepted value
//         * request = 0
//         * accepted = 1
//         * rejected = -1
//         */
//        csr.setAccepted(MetaData.ORDER_REJECTED);
//        dbr.child(childNode).setValue(csr).addOnCompleteListener(new OnCompleteListener<Void>() {
//            @Override
//            public void onComplete(@NonNull Task<Void> task) {
//                appointments.remove(csr);
//                adapter.notifyDataSetChanged();
//                userStack.remove(csr.getcUserName());
//
//                addToRejectedList(csr);
//            }
//        }).addOnFailureListener(new OnFailureListener() {
//            @Override
//            public void onFailure(@NonNull Exception e) {
//                Toast.makeText(getContext(), "Something went wrong", Toast.LENGTH_SHORT).show();
//            }
//        });
//
//        /**
//         * remove the user from the stack so that new listen will be activated
//         */
//
//    }
//
//    private void addToRejectedList(CustomerStationRequest csr) {
//        DatabaseReference dbr;
//        if (DashboardStationSalesman.userType.equals(MetaData.USER_TYPE_CLIENT_STATION)) {
//            dbr = FirebaseDatabase.getInstance().getReference().child(MetaData.STATION_REJECTED_ORDER);
//        } else {
//            dbr = FirebaseDatabase.getInstance().getReference().child(MetaData.SALESMAN_REJECTED_ORDER);
//        }
//
//        dbr.push().setValue(csr);
//    }
//
//    @Override
//    public void onOrderComplete(CustomerStationRequest csr) {
//
//    }
//
//    @OnClick(R.id.btn_view_detail_order)
//    public void onBtnViewDetailOrderClicked() {
//        OrderRequestDialog dialog = new OrderRequestDialog(getContext(), liveRequest);
//        dialog.show();
//    }
//
//    @OnClick(R.id.btn_accept_order)
//    public void onBtnAcceptOrderClicked() {
//        if (liveRequest.getAccepted() == MetaData.ORDER_ACCEPTED) {
//            launchOrderMapActivity();
//        } else {
//            String childNode = liveRequest.getcUserName() + liveRequest.getsUserName();
//            DatabaseReference dbr = FirebaseDatabase
//                    .getInstance()
//                    .getReference()
//                    .child(MetaData.CUSTOMER_SALESMAN_LIVE_ORDER_REQUEST)
//                    .child(childNode);
//            liveRequest.setAccepted(MetaData.ORDER_ACCEPTED);
//            dbr.setValue(liveRequest)
//                    .addOnCompleteListener(new OnCompleteListener<Void>() {
//                        @Override
//                        public void onComplete(@NonNull Task<Void> task) {
//                            launchOrderMapActivity();
//                        }
//                    }).addOnFailureListener(new OnFailureListener() {
//                @Override
//                public void onFailure(@NonNull Exception e) {
//                    Toast.makeText(getContext(), "Something went wrong", Toast.LENGTH_SHORT).show();
//                }
//            });
//        }
//    }
//
//    private void launchOrderMapActivity() {
//        Intent i = new Intent(getContext(), OrderMapActivity.class);
//        i.putExtra(MetaData.KEY_LATITUDE, liveRequest.getcLatitude());
//        i.putExtra(MetaData.KEY_LONGITUDE, liveRequest.getcLongitude());
//        i.putExtra(MetaData.KEY_CONTACT_LATITUDE, liveRequest.getContactLatitude());
//        i.putExtra(MetaData.KEY_CONTACT_LONGITUDE, liveRequest.getContactLongitude());
//        i.putExtra(MetaData.USER_TYPE_CUSTOMER, liveRequest.getcName());
//        i.putExtra(MetaData.KEY_SHOULD_SHOW_INFO, true);
//
//        getContext().startActivity(i);
//    }
//
//    @OnClick(R.id.btn_ignore_order)
//    public void onBtnIgnoreOrderClicked() {
//        if (liveRequest.getAccepted() == MetaData.ORDER_IDLE) {
//            String childNode = liveRequest.getcUserName() + liveRequest.getsUserName();
//            DatabaseReference dbr = FirebaseDatabase
//                    .getInstance()
//                    .getReference()
//                    .child(MetaData.CUSTOMER_SALESMAN_LIVE_ORDER_REQUEST)
//                    .child(childNode);
//            liveRequest.setAccepted(MetaData.ORDER_REJECTED);
//            dbr.setValue(liveRequest)
//                    .addOnCompleteListener(new OnCompleteListener<Void>() {
//                        @Override
//                        public void onComplete(@NonNull Task<Void> task) {
//                            Toast.makeText(getContext(), orejected, Toast.LENGTH_SHORT).show();
//                            addToRejectedList(liveRequest);
//                        }
//                    }).addOnFailureListener(new OnFailureListener() {
//                @Override
//                public void onFailure(@NonNull Exception e) {
//                    Toast.makeText(getContext(), "Something went wrong", Toast.LENGTH_SHORT).show();
//                }
//            });
//        } else if (liveRequest.getAccepted() == MetaData.ORDER_ACCEPTED) {
//            //means the btn behaves as complete order
//            String childNode = liveRequest.getcUserName() + liveRequest.getsUserName();
//            DatabaseReference dbr = FirebaseDatabase
//                    .getInstance()
//                    .getReference()
//                    .child(MetaData.CUSTOMER_SALESMAN_LIVE_ORDER_REQUEST)
//                    .child(childNode);
//            liveRequest.setAccepted(MetaData.ORDER_COMPLETED);
//            dbr.setValue(liveRequest)
//                    .addOnCompleteListener(new OnCompleteListener<Void>() {
//                        @Override
//                        public void onComplete(@NonNull Task<Void> task) {
//                            Toast.makeText(getContext(), oCompleted, Toast.LENGTH_SHORT).show();
//                            livelayout.setVisibility(View.GONE);
//
//                            //push the object to the completed node
//                            DatabaseReference finishDb = FirebaseDatabase.getInstance().getReference().child(MetaData.SALESMAN_FINISHED_TASK);
//                            finishDb.push().setValue(liveRequest);
//
//                        }
//                    }).addOnFailureListener(new OnFailureListener() {
//                @Override
//                public void onFailure(@NonNull Exception e) {
//                    Toast.makeText(getContext(), "Something went wrong", Toast.LENGTH_SHORT).show();
//                }
//            });
//        }
//    }
//
//    @Subscribe
//    public void onLanguageChangeEventReceived(Events.LanguageChangeEvent event) {
//        CURRENT_LANGUAGE = event.isLang();
//        Log.i(TAG, "Reached");
//        updateRecyclerTitle();
//
//        if (liveRequest != null) {
//            updateView();
//        }
//
//        if (appointments != null && appointments.size() > 0) {
//            adapter.setLang(CURRENT_LANGUAGE);
//            adapter.notifyDataSetChanged();
//
//        }
//    }
//
//    @Override
//    public void onDestroyView() {
//        super.onDestroyView();
//        EventBus.unregister(this);
//    }
//}
