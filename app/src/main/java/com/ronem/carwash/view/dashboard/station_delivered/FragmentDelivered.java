package com.ronem.carwash.view.dashboard.station_delivered;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polygon;
import com.google.android.gms.maps.model.PolygonOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.maps.android.PolyUtil;
import com.ronem.carwash.R;
import com.ronem.carwash.interfaces.CarServiceDateListener;
import com.ronem.carwash.model.SessionSavedInstance;
import com.ronem.carwash.model.users.Customer;
import com.ronem.carwash.model.users.CustomerStationRequest;
import com.ronem.carwash.model.users.SalesManStation;
import com.ronem.carwash.utils.BasicUtilityMethods;
import com.ronem.carwash.utils.DistanceCalculator;
import com.ronem.carwash.utils.EventBus;
import com.ronem.carwash.utils.Events;
import com.ronem.carwash.utils.MetaData;
import com.ronem.carwash.utils.MyAnimation;
import com.ronem.carwash.utils.SessionManager;
import com.ronem.carwash.view.CarwashSplash;
import com.ronem.carwash.view.dashboard.DirectionAdView;
import com.ronem.carwash.view.dashboard.DirectionPresenter;
import com.ronem.carwash.view.dashboard.DirectionPresenterImpl;
import com.ronem.carwash.view.dialogs.ShowInfoDialogFragment;
import com.ronem.carwash.view.dialogs.SuccessDialog;
import com.squareup.otto.Subscribe;

import java.security.Key;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by ronem on 8/20/17.
 */

public class FragmentDelivered
        extends Fragment
        implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        com.google.android.gms.location.LocationListener,
        DirectionAdView {

    private String TAG = getClass().getSimpleName();

    private GoogleApiClient googleApiClient;
    private LocationRequest locationRequest;
    private GoogleMap googleMap;
    private List<SalesManStation> salesMans;
    private List<SalesManStation> stations;
    private List<SalesManStation> insideZoneSalesMans;
    private DirectionPresenter presenter;
    private Location myLocation;
    private LatLng myLatlang, destinationLatlang;
    private Polyline polyline;

    private Marker salesManMarker;
    private SessionManager sessionManager;
    private boolean isCameraAnimated = false;


    @Bind(R.id.dialog_info)
    LinearLayout dialogInfo;
    @Bind(R.id.dialog_time_away)
    TextView dialogTimeAwayTextView;
    @Bind(R.id.dialog_distance)
    TextView dialogDistanceTextView;
    @Bind(R.id.dialog_station_name_title)
    TextView dialogStationNameTitleView;
    @Bind(R.id.dialog_station_name)
    TextView dialogStationNameTextView;
    @Bind(R.id.dialog_carwasher_name_title)
    TextView dialogCarwasherNameTextview;
    @Bind(R.id.contact_marker)
    ImageView contactMarkerView;

    @Bind(R.id.dialog_vehicle_type)
    TextView dialogVehicleTypeTextView;
    @Bind(R.id.dialog_contact)
    TextView dialogContactTv;
    @Bind(R.id.dialog_share)
    TextView dialogShareTv;
    @Bind(R.id.progress)
    ProgressBar progressBar;
    @Bind(R.id.layout_order_event)
    LinearLayout layoutOrderEvent;

    @Bind(R.id.btn_order_now)
    Button btnOrderNow;
    @Bind(R.id.btn_order_later)
    Button btnOrderLater;
    @Bind(R.id.toggle_map)
    TextView toggleMap;
    private boolean isSatelliteView = false;

    private Customer customer;
    private List<Marker> stationsMarkerList;
    private List<Polygon> stationsPolygonList;
    private Marker nearestMarker;
    private GoogleMap.OnMarkerClickListener markerClickListener;
    private String distance = "0km";
    private String duration = "0m";
    private int noOfCars;
    private HashMap<String, HashMap<String, String>> cars;
    private String dateTime;

    private LatLng contactPointLatLang;
    //    private boolean wasContactPointMarker = false;
    private boolean isAdding = false;
    private SalesManStation nearestSalesMan;
    private List<SalesManStation> tempSalesMans;
    private CustomerStationRequest csr;
    private int ORDER_TYPE = 1;
    //    private boolean shouldMarkerMove = true;
    private MyAnimation animation;
    private boolean isInside = false;
    private ProgressDialog progressDialog;
    private SuccessDialog successDialog;
    private boolean CURRENT_LANGUAGE;
    private String MSG_ENGAGED, MSG_DEST_10M, MSG_CNT_FIND_ROUTE, MSG_WATING_RESPONSE,
            strTimeAwayFormat,
            MSG_OUT_COVERAGE, MSG_NO_OF_SALESMAN_FOUND;
    private String key;
    private SessionSavedInstance sessionSavedInstance;
    private boolean isAccepted = false;
    private boolean isFromSession = false;
    private Marker contactPointMarker;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_station, container, false);
        ButterKnife.bind(this, v);
        EventBus.register(this);
        key = getString(R.string.my_google_api_key);

        progressDialog = new ProgressDialog(getContext());
        progressDialog.setCancelable(true);

        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        contactMarkerView.setVisibility(View.VISIBLE);
        sessionManager = new SessionManager(getContext());
        animation = new MyAnimation();
        customer = sessionManager.getCustomer();
        CURRENT_LANGUAGE = sessionManager.getCurrentLanguage();

        presenter = new DirectionPresenterImpl();
        presenter.onAddDirectionView(this);

        createGoogleApiClient();
        googleApiClient.connect();

        locationRequest = BasicUtilityMethods.createLocationRequest();

        progressBar.setVisibility(View.VISIBLE);
        loadmap();

        layoutOrderEvent.setVisibility(View.VISIBLE);

        //update the ui accordingly
        updateUILang();

        //check for the sessionSavedState
        sessionSavedInstance = sessionManager.getMotorycycleSavedInstance();

        if (sessionSavedInstance == null) {
            return;
        }

        isFromSession = true;
        isAdding = sessionSavedInstance.isAdding();

        if (ORDER_TYPE != 0) {
            destinationLatlang = sessionSavedInstance.getDestination();
            contactPointLatLang = sessionSavedInstance.getContact();

        }

    }

    private void updateUILang() {
        //if arabic
        if (CURRENT_LANGUAGE) {
            btnOrderNow.setText(getContext().getString(R.string.order_now_arabic));
            btnOrderLater.setText(getContext().getString(R.string.order_later_arabic));

        } else {
            btnOrderNow.setText(getContext().getString(R.string.order_now));
            btnOrderLater.setText(getContext().getString(R.string.order_later));
        }

        MSG_ENGAGED = CURRENT_LANGUAGE ? getString(R.string.motorcycle_engaged_arabic) : getString(R.string.motorcycle_engaged);
        MSG_DEST_10M = CURRENT_LANGUAGE ? getString(R.string.destination_10m_ahead_arabic) : getString(R.string.destination_10m_ahead);
        MSG_CNT_FIND_ROUTE = CURRENT_LANGUAGE ? getString(R.string.cannot_find_route_arabic) : getString(R.string.cannot_find_route);
        MSG_WATING_RESPONSE = CURRENT_LANGUAGE ? getString(R.string.waiting_for_response_arabic) : getString(R.string.waiting_for_response);
        MSG_OUT_COVERAGE = CURRENT_LANGUAGE ? getString(R.string.out_of_coverage_arabic) : getString(R.string.out_of_coverage);

        String pleasewait = CURRENT_LANGUAGE ? getString(R.string.please_wait_arabic) : getString(R.string.please_wait);
        progressDialog.setMessage(pleasewait);
    }


    protected synchronized void createGoogleApiClient() {
        googleApiClient = new GoogleApiClient.Builder(getContext())
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
    }

    private void loadmap() {
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.station_delivered_map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        requestLocationupdate();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location l) {
        if (l == null)
            return;

        Log.i(TAG, "Location received in delivered");
        myLocation = l;

        myLatlang = new LatLng(myLocation.getLatitude(), myLocation.getLongitude());


        if (!isCameraAnimated) {
            progressBar.setVisibility(View.GONE);
            CameraPosition newCamPos = new CameraPosition
                    .Builder()
                    .target(myLatlang)
                    .zoom(MetaData.MAP_ZOOM)
                    .bearing(300)
                    .tilt(0)
                    .build();
            googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(newCamPos), 4000, null);

            isCameraAnimated = true;
        }

        //added--------------
        //check if the order was accepted
        //and check if the mylocation marker is not drawn previously
        //if the order was accepted then show the current location marker


        if (isFromSession) {

            String childNode = sessionSavedInstance.getChildNode();

            if (ORDER_TYPE == 1) {
                waitingForLiveResponse(childNode);

            } else if (ORDER_TYPE == 2) {
                waitingForResponse(childNode);
            }


            isFromSession = false;

        }


        if (isAccepted) {
            String url = BasicUtilityMethods.getUrl(myLatlang, contactPointLatLang, key);
            Log.i(TAG, "Distance URL::" + url);
            presenter.onGetDistanceTime(url);
        }
//        if (destinationLatlang != null) {
//            double distance = DistanceCalculator.getDistance(myLatlang.latitude, myLatlang.longitude, destinationLatlang.latitude, destinationLatlang.longitude, "K");
//            String d = String.format(Locale.CANADA, "%.2f", distance);
//
//            dialogDistanceTextView.setText(d + "km");
//            if (distance < 0.01) {
//                showToast(MSG_DEST_10M);
//            }
//        }


    }


    @Override
    public void onMapReady(final GoogleMap gMap) {
        this.googleMap = gMap;
        googleMap.setMyLocationEnabled(true);
        googleMap.getUiSettings().setMyLocationButtonEnabled(true);

        Log.i(TAG, "Map Ready in station");
        if (ActivityCompat.checkSelfPermission(getContext(),
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(getContext(),
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        populateStations();

        populateSalesman();

        setUpMovableMap();

        setOnTouchListenerForMap();
    }

    private void setOnTouchListenerForMap() {
        googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                if (isAccepted) {
                    if (dialogInfo.getVisibility() == View.VISIBLE) {
                        animation.collapseView(dialogInfo);
                    } else {
                        animation.expandView(dialogInfo);
                    }
                }
            }
        });
    }

    private void setUpMovableMap() {
        googleMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
            @Override
            public void onCameraIdle() {
                Log.i(TAG, "TotalSalesman" + salesMans.size());

                //if already accepted then no need to make further calculations
                //and show the messages
                if (isAccepted) {
                    return;
                }

                if (salesMans.size() <= 0) {
                    showNoSalesManFoundMsg();
                    return;
                }


                /**
                 * Check if the dragged marker lies with in the circle
                 * if present within the circle then show the dialog
                 * if not then show the dialog with the message saying
                 * "sorry you are out of the radius and you are not reachable"
                 */
                contactPointLatLang = googleMap.getCameraPosition().target;
                Log.i(TAG, "Lat: " + contactPointLatLang.latitude + "\nLongitude: " + contactPointLatLang.longitude);


                //test
                insideZoneSalesMans = new ArrayList<>();
                for (Polygon p1 : stationsPolygonList) {
                    isInside = PolyUtil.containsLocation(contactPointLatLang, p1.getPoints(), false);
                    if (isInside) {
                        for (SalesManStation s : salesMans) {
                            double lati = s.getLatitude();
                            double longi = s.getLongitude();
                            LatLng sLL = new LatLng(lati, longi);
                            boolean isPresent = PolyUtil.containsLocation(sLL, p1.getPoints(), false);
                            if (isPresent) {
                                insideZoneSalesMans.add(s);
                            }
                        }

                        //check if the salesman is present inside the zone
                        if (insideZoneSalesMans.size() <= 0) {
                            showNoSalesManFoundMsg();
                        } else {

                            String total = String.valueOf(insideZoneSalesMans.size());
                            String msg;
                            if (CURRENT_LANGUAGE) {
                                msg = getString(R.string.no_of_salesman_found_arabic, total);
                            } else {
                                msg = getString(R.string.no_of_salesman_found, total);
                            }
                            showToast(msg);
                        }

                        /**
                         * We need to break out of the loop
                         * since we already found the contact point inside the circle
                         */
                        break;

                    }
                }
                if (!isInside) {
                    showToast(MSG_OUT_COVERAGE);
                }


            }
        });
    }

    private void showToast(String s) {
        if (getActivity() != null) {
            Toast.makeText(getContext(), s, Toast.LENGTH_SHORT).show();
        }
    }


//    private void setDragableMarker() {
//
//        googleMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
//            @Override
//            public void onMapLongClick(LatLng latLng) {
//                if (stationsCircleList.size() > 1) {
//                    if (contactPointMarker == null) {
//                        if (shouldMarkerMove) {
//                            MarkerOptions markerOption = new MarkerOptions()
//                                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE))
//                                    .position(latLng)
//                                    .draggable(true);
//
//                            contactPointMarker = googleMap.addMarker(markerOption);
//
//                            contactPointLatLang = latLng;
//                            /**
//                             * Check if the dragged marker lies with in the circle
//                             * if present within the circle then show the dialog
//                             * if not then show the dialog with the message saying
//                             * "sorry you are out of the radius and you are not reachable"
//                             */
//                            for (int i = 0; i < stationsCircleList.size(); i++) {
//                                Circle c = stationsCircleList.get(i);
//                                float[] distance = new float[2];
//                                LatLng cLatLang = c.getCenter();
//                                double cLatitude = cLatLang.latitude;
//                                double cLongitude = cLatLang.longitude;
//                                Location.distanceBetween(
//                                        contactPointLatLang.latitude,
//                                        contactPointLatLang.longitude,
//                                        cLatitude,
//                                        cLongitude,
//                                        distance);
//
//                                if (distance[0] > c.getRadius()) {
//                                    isInside = false;
//                                } else {
//                                    isInside = true;
//                                    String url = BasicUtilityMethods.getUrl(myLatlang, contactPointLatLang, key);
//                                    wasContactPointMarker = true;
//                                    presenter.onGetPolyLineOptions(url);
//                                    break;
//                                }
//                            }
//
//                            if (!isInside) {
//                                Toast.makeText(getActivity(), MSG_OUT_COVERAGE, Toast.LENGTH_SHORT).show();
//                            }
//                        }
//                    }
//                } else {
//                    Toast.makeText(getContext(), "Sorry there is no any stations and motorcycle present", Toast.LENGTH_LONG).show();
//                }
//            }
//        });
//
//        googleMap.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {
//            @Override
//            public void onMarkerDragStart(Marker marker) {
//
//            }
//
//            @Override
//            public void onMarkerDrag(Marker marker) {
//
//            }
//
//            @Override
//            public void onMarkerDragEnd(Marker marker) {
//
//                /**
//                 * Check if the dragged marker lies with in the circle
//                 * if present within the circle then show the dialog
//                 * if not then show the dialog with the message saying
//                 * "sorry you are out of the radius and you are not reachable"
//                 */
//                for (int i = 0; i < stationsCircleList.size(); i++) {
//
//                    float[] distance = new float[2];
//                    Circle c = stationsCircleList.get(i);
//                    LatLng cLatLang = c.getCenter();
//                    double cLatitude = cLatLang.latitude;
//                    double cLongitude = cLatLang.longitude;
//
//                    contactPointLatLang = marker.getPosition();
//
//                    Location.distanceBetween(
//                            contactPointLatLang.latitude,
//                            contactPointLatLang.longitude,
//                            cLatitude,
//                            cLongitude,
//                            distance);
//
//
//                    if (distance[0] > c.getRadius()) {
//                        isInside = false;
//                    } else {
//                        isInside = true;
//                        String url = BasicUtilityMethods.getUrl(myLatlang, contactPointLatLang, key);
//                        wasContactPointMarker = true;
//                        presenter.onGetPolyLineOptions(url);
//                        break;
//                    }
//                }
//
//                if (!isInside) {
//                    Toast.makeText(getActivity(), MSG_OUT_COVERAGE, Toast.LENGTH_SHORT).show();
//                }
//
//            }
//        });
//    }

    private void populateStations() {
        stationsPolygonList = new ArrayList<>();

        DatabaseReference dbr = FirebaseDatabase.getInstance().getReference().child(MetaData.USER_TYPE_CLIENT_STATION);
        dbr.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Log.i(TAG, "dataChanged received");


                if (dataSnapshot.exists()) {

                    //first clear all the circles if present
                    if (stationsPolygonList != null && stationsPolygonList.size() > 0) {
                        for (Polygon p : stationsPolygonList) {
                            p.remove();
                        }
                        stationsMarkerList.clear();
                    }


                    for (DataSnapshot ds : dataSnapshot.getChildren()) {
                        SalesManStation sms = ds.getValue(SalesManStation.class);

                        /**
                         * Adding polygon around the stations
                         * with the given list of polygon points
                         */
                        HashMap<String, HashMap<String, String>> points = sms.getPolygonPoints();


                        List<LatLng> ls = new ArrayList<>();
                        for (Map.Entry<String, HashMap<String, String>> ps : points.entrySet()) {
                            HashMap<String, String> p = ps.getValue();
                            String _lati = p.get(MetaData.KEY_LATITUDE);
                            String _longi = p.get(MetaData.KEY_LONGITUDE);
                            double lati = Double.parseDouble(_lati);
                            double longi = Double.parseDouble(_longi);
                            ls.add(new LatLng(lati, longi));
                        }


                        PolygonOptions polygonOptions = new PolygonOptions().addAll(ls);

                        Polygon p = googleMap.addPolygon(polygonOptions);
                        p.setFillColor(MetaData.SHADE_COLOR);
                        p.setStrokeWidth(2);
                        p.setStrokeColor(MetaData.STROKE_COLOR);
                        stationsPolygonList.add(p);
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void populateSalesman() {

        salesMans = new ArrayList<>();
        DatabaseReference dbr = FirebaseDatabase.getInstance().getReference().child(MetaData.AVAILABLE_SALESMANS);
        dbr.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Log.i(TAG, "dataChanged received");


                if (dataSnapshot.exists()) {
                    salesMans.clear();
                    for (DataSnapshot ds : dataSnapshot.getChildren()) {
                        SalesManStation sms = ds.getValue(SalesManStation.class);
                        salesMans.add(sms);

                    }
                }
                if (contactPointLatLang != null) {
                    CameraPosition newCamPos = new CameraPosition
                            .Builder()
                            .target(contactPointLatLang)
                            .zoom(MetaData.MAP_ZOOM)
                            .bearing(300)
                            .tilt(0)
                            .build();
                    googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(newCamPos), 4000, null);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @SuppressWarnings("MissingPermission")
    private void requestLocationupdate() {
        LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);
    }

    @Override
    public void onPolyLineOptionReceived(PolylineOptions polylineOptions, String distance, String duration) {
        if (progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
        this.distance = distance;
        this.duration = duration;

        //check if the polyline is for order accepted
        if (isAccepted) {
            if (polyline != null) polyline.remove();
            polyline = googleMap.addPolyline(polylineOptions);
            return;
        }

        if (polylineOptions == null) {
            //reset the flag so that user can make another order
            isAdding = false;
            showToast(MSG_CNT_FIND_ROUTE);
            return;
        }

        if (polyline != null) {
            polyline.remove();
            salesManMarker.remove();
        }

        MarkerOptions mOption = new MarkerOptions()
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN))
                .position(destinationLatlang);

        salesManMarker = googleMap.addMarker(mOption);
        polyline = googleMap.addPolyline(polylineOptions);

        //only show the info dialog if the it is not from the saved session
        if (!isFromSession) {
            showInfoDialog(distance, duration);
        }

    }

    @Override
    public void onTimeDistanceReceived(String distance, String time, int value) {
        if (isAccepted) {
            if (!TextUtils.isEmpty(distance)) {
                dialogDistanceTextView.setText(distance);

                String msg = CURRENT_LANGUAGE ?
                        getString(R.string.time_away_arabic, time)
                        : getString(R.string.time_away, time);
                dialogTimeAwayTextView.setText(msg);
            }

            //check if the value is less than 10
            //indicate the distance is less than 10m
            if (value < 10) {
                if (getActivity() != null) {
                    Toast.makeText(getContext(), "you reached the point", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    private void showInfoDialog(String distance, String duration) {
        ShowInfoDialogFragment dialogFragment = new ShowInfoDialogFragment();
        Bundle box = new Bundle();
        box.putInt("order_type", ORDER_TYPE);
        box.putString("station", nearestSalesMan.getName());
        box.putString("station_email", nearestSalesMan.getEmail());
        box.putString("station_contact", nearestSalesMan.getMobileNumber());
        box.putString("station_address", nearestSalesMan.getAddress());
        box.putString("distance", distance);
        box.putString("time", duration);

        dialogFragment.setArguments(box);
        dialogFragment.setOnCarServiceDateListener(new CarServiceDateListener() {
            @Override
            public void onCarTypeServiceDateReceived(int nCars, HashMap<String, HashMap<String, String>> crs, String d) {

                noOfCars = nCars;
                cars = crs;
                dateTime = d;
                makeOrderRequest();
            }
        });
        dialogFragment.show(getFragmentManager(), "");
    }

    private void makeOrderRequest() {


        /**isAdding is set to true
         /*indicating order request is made
         /*and cannot make the further request untill the client either
         /*accept/ignore
         */
        isAdding = true;

        DatabaseReference dbr;
        if (ORDER_TYPE == 1) {
            dbr = FirebaseDatabase.getInstance().getReference().child(MetaData.CUSTOMER_SALESMAN_LIVE_ORDER_REQUEST);
        } else {
            dbr = FirebaseDatabase.getInstance().getReference().child(MetaData.CUSTOMER_SALESMAN_REQUEST);
        }
        CustomerStationRequest csr = new CustomerStationRequest(
                customer.getUserName(),
                customer.getCustomerName(),
                "",
                customer.getCustomerMobileNumber(),
                customer.getLatitude(),
                customer.getLongitude(),
                contactPointLatLang.latitude, contactPointLatLang.longitude,
                dateTime,
                noOfCars,
                cars,
                nearestSalesMan.getUsername(),
                nearestSalesMan.getName(),
                nearestSalesMan.getAddress(),
                nearestSalesMan.getLatitude(),
                nearestSalesMan.getLongitude(),
                nearestSalesMan.getMobileNumber(),
                nearestSalesMan.getEmail(),
                MetaData.ORDER_IDLE
        );
        final String childNode = customer.getUserName() + nearestSalesMan.getUsername();
        dbr.child(childNode).setValue(csr).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                String msg = sessionManager.getCurrentLanguage() ?
                        getString(R.string.order_sent_arabic) : getString(R.string.order_sent);

                successDialog = new SuccessDialog(getContext(), msg, true);
                successDialog.show();

                sessionManager.setMotorcycleSavedInstance(ORDER_TYPE, destinationLatlang, contactPointLatLang, isAdding, childNode);
                if (ORDER_TYPE == 1) {
                    waitingForLiveResponse(childNode);
                } else {
                    waitingForResponse(childNode);
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                showToast("Something went wrong");
            }
        });

    }

    private void waitingForResponse(String childNode) {
        final DatabaseReference dbr = FirebaseDatabase
                .getInstance()
                .getReference()
                .child(MetaData.CUSTOMER_SALESMAN_REQUEST)
                .child(childNode);

        dbr.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Log.i(TAG, "waiting response");
                if (dataSnapshot.exists()) {
                    csr = dataSnapshot.getValue(CustomerStationRequest.class);

                    switch (csr.getAccepted()) {
                        case MetaData.ORDER_IDLE:
                            orderWaitingForClientResponse();
                            break;
                        case MetaData.ORDER_ACCEPTED:
                            orderAccepted();
                            break;
                        case MetaData.ORDER_REJECTED:
                            orderRejected(dbr);
                            break;
                        case MetaData.ORDER_COMPLETED:
                            orderCompleted(dbr);
                            break;
                    }

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void waitingForLiveResponse(String childNode) {
        final DatabaseReference dbr = FirebaseDatabase
                .getInstance()
                .getReference()
                .child(MetaData.CUSTOMER_SALESMAN_LIVE_ORDER_REQUEST)
                .child(childNode);

        dbr.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Log.i(TAG, "waiting response");

                if (dataSnapshot.exists()) {
                    csr = dataSnapshot.getValue(CustomerStationRequest.class);

                    switch (csr.getAccepted()) {
                        case MetaData.ORDER_IDLE:
                            orderWaitingForClientResponse();
                            break;
                        case MetaData.ORDER_ACCEPTED:
                            orderAccepted();
                            break;
                        case MetaData.ORDER_REJECTED:
                            orderRejected(dbr);
                            break;
                        case MetaData.ORDER_COMPLETED:
                            orderCompleted(dbr);
                            break;
                    }

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }


    private void orderWaitingForClientResponse() {
        isAccepted = false;
        showToast(MSG_WATING_RESPONSE);

    }

    private void orderCompleted(DatabaseReference dbr) {

        isAccepted = false;
        isAdding = false;


        dialogInfo.setVisibility(View.GONE);

        if (getActivity() != null) {

            String title = CURRENT_LANGUAGE ?
                    getResources().getString(R.string.task_status_arabic) :
                    getResources().getString(R.string.task_status);

            String msg = CURRENT_LANGUAGE ?
                    getResources().getString(R.string.order_complete_arabic)
                    : getResources().getString(R.string.order_complete);

            String close = CURRENT_LANGUAGE ?
                    getResources().getString(R.string.close_arabic)
                    : getResources().getString(R.string.close);

            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle(title);
            builder.setMessage(msg);
            builder.setPositiveButton(close, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                    Intent homeIntent = new Intent(getContext(), CarwashSplash.class);
                    homeIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(homeIntent);
                }
            });

            AlertDialog dialog = builder.create();
            dialog.show();
        }

        //update the sessionSavedState
        sessionManager.setMotorcycleSavedInstance(0, destinationLatlang, contactPointLatLang, isAdding, "");

        dbr.removeValue();
    }

    private void orderRejected(DatabaseReference dbr) {

        isAccepted = false;

        //remove the markers and the polyline
        if (salesManMarker != null) salesManMarker.remove();
        if (polyline != null) polyline.remove();


        if (successDialog != null && successDialog.isShowing()) {
            successDialog.dismiss();
        }

        if (getActivity() != null) {
            String msg = CURRENT_LANGUAGE ? getString(R.string.order_rejected_arabic) : getString(R.string.order_rejected);
            successDialog = new SuccessDialog(getContext(), msg, false);
            successDialog.show();
        }

        isAdding = false;

        //update the sessionSavedState
        sessionManager.setMotorcycleSavedInstance(0, destinationLatlang, contactPointLatLang, isAdding, "");
        dbr.removeValue();

    }

    private void orderAccepted() {

        isAccepted = true;

        if (successDialog != null && successDialog.isShowing()) {
            successDialog.dismiss();
        }

        if (getActivity() != null) {
            updateDialogINfoLang();

            dialogCarwasherNameTextview.setText(csr.getsName());
            dialogStationNameTextView.setText(csr.getsAddress());
            dialogDistanceTextView.setText(distance);

            dialogInfo.setVisibility(View.VISIBLE);
            layoutOrderEvent.setVisibility(View.GONE);

            //draw the marker
            //contact point
            //and current

            //if contact point was saved in the session means you have accepted previously
            //so use the contact point marker with that latLang
            //but if the session was null means request is accepted now
            MarkerOptions contactMarkerOption = new MarkerOptions()
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED))
                    .position(contactPointLatLang);

            contactPointMarker = googleMap.addMarker(contactMarkerOption);

            //remove the salesman marker location if present
            if (salesManMarker != null) {
                salesManMarker.remove();
            }

            //url between contact point and the destination of salesman
            String url = BasicUtilityMethods.getUrl(myLatlang, contactPointLatLang, key);
            Log.i(TAG, "URL::" + url);
            presenter.onGetPolyLineOptions(url);
        }
    }


    private void updateDialogINfoLang() {
        //updating the text for the station name
        String smTitle = CURRENT_LANGUAGE ?
                getString(R.string.station_name_arabic) :
                getString(R.string.station_name);
        dialogStationNameTitleView.setText(smTitle);

        //updating the vehicle type
        String vt = CURRENT_LANGUAGE ?
                getString(R.string.vehicle_type_arabic) :
                getString(R.string.vehicle_type);
        dialogVehicleTypeTextView.setText(vt);

        //updating the contact captain button text
        String cc = CURRENT_LANGUAGE ?
                getString(R.string.contact_captain_arabic) :
                getString(R.string.contact_captain);
        dialogContactTv.setText(cc);

        //updating the share contact button text
        String s = CURRENT_LANGUAGE ?
                getString(R.string.share_tracker_arabic) :
                getString(R.string.share_tracker);
        dialogShareTv.setText(s);


//        String msg = CURRENT_LANGUAGE ?
//                getString(R.string.time_away_arabic, duration)
//                : getString(R.string.time_away, duration);
//        dialogTimeAwayTextView.setText(msg);
    }

    @Override
    public void onStart() {
        super.onStart();
        googleApiClient.connect();
    }

    @Override
    public void onStop() {
        super.onStop();
        googleApiClient.disconnect();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (googleApiClient.isConnected()) {
            requestLocationupdate();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (googleApiClient.isConnected())
            LocationServices.FusedLocationApi.removeLocationUpdates(googleApiClient, this);
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        EventBus.unregister(this);
    }

    @OnClick(R.id.btn_order_now)
    public void onBtnOrderNowClicked() {

        if (contactPointLatLang == null) {
            createContactPointMsg();
            return;
        }

        if (!isInside) {
            showToast(MSG_OUT_COVERAGE);
            return;
        }

        if (isAdding) {
            showAlreadyMadeMsg();
            return;
        }

        //flag to indicate that event is of live order
        ORDER_TYPE = 1;

        /**isFromSession is reset to false
         /*indicating new order is made
         */
        isFromSession = false;

        if (insideZoneSalesMans == null || insideZoneSalesMans.size() < 1) {

            showNoSalesManFoundMsg();
            return;
        }
        Log.i(TAG, "TOTAL_SALESMAN:" + insideZoneSalesMans.size());

        if (!progressDialog.isShowing()) {
            progressDialog.show();
        }

        DatabaseReference dbr = FirebaseDatabase.getInstance().getReference().child(MetaData.CUSTOMER_SALESMAN_LIVE_ORDER_REQUEST);
        dbr.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                tempSalesMans = new ArrayList<>();
                tempSalesMans.addAll(insideZoneSalesMans);

                //listen only if customer of this app has clicked
                //and if it was not from adding the data to working/live node
                if (ORDER_TYPE == 1 && !isAdding) {
                    List<CustomerStationRequest> workingSalesMans = new ArrayList<>();
                    if (dataSnapshot.exists()) {
                        for (DataSnapshot ds : dataSnapshot.getChildren()) {
                            CustomerStationRequest csr = ds.getValue(CustomerStationRequest.class);
                            workingSalesMans.add(csr);

                        }
                    }

                    boolean loop = true;
                    while (loop) {
                        loop = false;

                        Log.i(TAG, "TEMPSalesMan" + tempSalesMans.size());
                        //take first item as a nearest one
                        //then loop to find the next nearest one

                        nearestSalesMan = tempSalesMans.get(0);
                        double shortestDistance = DistanceCalculator
                                .getDistance(contactPointLatLang.latitude,
                                        contactPointLatLang.longitude,
                                        tempSalesMans.get(0).getLatitude(),
                                        tempSalesMans.get(0).getLongitude(),
                                        "K");

                        //loop to get the next nearest one
                        for (int i = 0; i < tempSalesMans.size(); i++) {

                            double actualDistance = DistanceCalculator
                                    .getDistance(contactPointLatLang.latitude,
                                            contactPointLatLang.longitude,
                                            tempSalesMans.get(i).getLatitude(),
                                            tempSalesMans.get(i).getLongitude(),
                                            "K");

                            if (actualDistance <= shortestDistance) {
                                shortestDistance = actualDistance;
                                nearestSalesMan = tempSalesMans.get(i);
                            }
                        }


                        if (workingSalesMans.size() > 0) {
                            for (int i = 0; i < workingSalesMans.size(); i++) {
                                if (nearestSalesMan.getUsername().equals(workingSalesMans.get(i).getsUserName())) {
                                    tempSalesMans.remove(nearestSalesMan);

                                    if (tempSalesMans.size() > 0) {
                                        loop = true;
                                    } else {
                                        if (progressDialog.isShowing()) progressDialog.dismiss();

                                        showToast(MSG_ENGAGED);
                                        /**need to re-add all the {@link insideZoneSalesMans} because after the working node is completed
                                         *the data is deleted and again onData change is called so we need to
                                         *re-add the all available salesman
                                         *if not added will will get ArrayIndex out of bound exception
                                         * */
                                        tempSalesMans.addAll(insideZoneSalesMans);
                                        return;
                                    }
                                    break;
                                }
                            }
                        }
                    }


                    //url between contact point and the destination of salesman
                    destinationLatlang = new LatLng(nearestSalesMan.getLatitude(), nearestSalesMan.getLongitude());
                    String url = BasicUtilityMethods.getUrl(contactPointLatLang, destinationLatlang, key);
                    Log.i(TAG, "URL::" + url);
                    presenter.onGetPolyLineOptions(url);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        //if nearest salesman is present in the workingSalesman node then get the next nearest salesman and repeat from 2.
        //if not present in the working salesman node then make a orderRequest
    }

    private void showNoSalesManFoundMsg() {
        String m = CURRENT_LANGUAGE ?
                getString(R.string.no_sales_man_found_arabic)
                : getString(R.string.no_sales_man_found);

        showToast(m);
    }

    private void showAlreadyMadeMsg() {
        String m = CURRENT_LANGUAGE ?
                getString(R.string.already_made_order_arabic) : getString(R.string.already_made_order);
        showToast(m);
    }

    private void createContactPointMsg() {
        String m = CURRENT_LANGUAGE ?
                getString(R.string.create_contact_point_arabic) : getString(R.string.create_contact_point);

        showToast(m);
    }

    @OnClick(R.id.btn_order_later)
    public void onBtnOrderLaterClicked() {
        //remove the marker if present for the salesman
        //because there should not be marker and the contact point path while
        //making the appointment
        if (salesManMarker != null) {
            salesManMarker.remove();
        }
        if (polyline != null) {
            polyline.remove();
        }


        if (contactPointLatLang == null) {
            createContactPointMsg();
            return;
        }

        if (!isInside) {
            showToast(MSG_OUT_COVERAGE);
            return;
        }

//        if (isAdding) {
//            showAlreadyMadeMsg();
//            return;
//        }

//        if (!shouldMarkerMove) {
//            showAlreadyMadeMsg();
//            return;
//        }

        ORDER_TYPE = 2;
        if (insideZoneSalesMans == null || insideZoneSalesMans.size() < 1) {
            showNoSalesManFoundMsg();
            return;
        }

        tempSalesMans = new ArrayList<>();
        tempSalesMans.addAll(insideZoneSalesMans);

        //check for the nearest salesman
        nearestSalesMan = tempSalesMans.get(0);
        double shortestDistance = DistanceCalculator
                .getDistance(contactPointLatLang.latitude,
                        contactPointLatLang.longitude,
                        tempSalesMans.get(0).getLatitude(),
                        tempSalesMans.get(0).getLongitude(),
                        "K");

        for (int i = 0; i < tempSalesMans.size(); i++) {

            double actualDistance = DistanceCalculator
                    .getDistance(contactPointLatLang.latitude,
                            contactPointLatLang.longitude,
                            tempSalesMans.get(i).getLatitude(),
                            tempSalesMans.get(i).getLongitude(),
                            "K");

            if (actualDistance <= shortestDistance) {
                shortestDistance = actualDistance;
                nearestSalesMan = tempSalesMans.get(i);
            }
        }


        /*
        //url between contact point and the destination of salesman
        destinationLatlang = new LatLng(nearestSalesMan.getLatitude(), nearestSalesMan.getLongitude());
        String url = BasicUtilityMethods.getUrl(contactPointLatLang, destinationLatlang, key);
        Log.i(TAG, "URL::" + url);

        if (!progressDialog.isShowing()) {
            progressDialog.show();
        }
        presenter.onGetPolyLineOptions(url);
        */
        //here we will not draw the route between the contact point and the salesman
        //because we don't know where the salesman will be at the time of appointment
        //arrived

        showInfoDialog("", "");


    }

    @OnClick(R.id.toggle_map)
    public void onToggleMapClicked() {
        if (isSatelliteView) {
            isSatelliteView = false;
            googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
            toggleMap.setText(R.string.satellite);
        } else {
            isSatelliteView = true;
            googleMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
            toggleMap.setText(R.string.defaultmap);
        }
    }

    @Subscribe
    public void onLanguageChangeEventReceived(Events.LanguageChangeEvent event) {
        CURRENT_LANGUAGE = event.isLang();
        updateUILang();
        if (dialogInfo.getVisibility() == View.VISIBLE) {
            updateDialogINfoLang();
        }
    }

}
