//package com.ronem.carwash.view.live_processing_orders_station_salesman;
//
//import android.os.Bundle;
//import android.support.annotation.Nullable;
//import android.support.design.widget.TabLayout;
//import android.support.v4.app.Fragment;
//import android.support.v4.view.ViewPager;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//
//import com.ronem.carwash.R;
//import com.ronem.carwash.adapters.OrderFragmentPagerAdapterClient;
//import com.ronem.carwash.utils.EventBus;
//import com.ronem.carwash.utils.Events;
//import com.ronem.carwash.utils.SessionManager;
//import com.squareup.otto.Subscribe;
//
//import butterknife.Bind;
//import butterknife.ButterKnife;
//
///**
// * Created by ram on 8/12/17.
// */
//
//public class FragmentLiveProcessingOrderClient extends Fragment {
//
//    @Bind(R.id.tab_layout)
//    TabLayout adTabLayout;
//    @Bind(R.id.view_pager)
//    ViewPager viewPager;
//
//    private OrderFragmentPagerAdapterClient adapter;
//    private boolean CURRENT_LANGUAGE;
//    private SessionManager sessionManager;
//
//    @Nullable
//    @Override
//    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        View root = inflater.inflate(R.layout.order_layout_live_processing_client, container, false);
//        ButterKnife.bind(this, root);
//        EventBus.register(this);
//        sessionManager = new SessionManager(getContext());
//        CURRENT_LANGUAGE = sessionManager.getCurrentLanguage();
//
//        return root;
//    }
//
//    @Override
//    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
//        super.onViewCreated(view, savedInstanceState);
//
//        setUpViewPager();
//
//    }
//
//
//    private void setUpViewPager() {
//        adapter = new OrderFragmentPagerAdapterClient(getChildFragmentManager(),getContext());
//        adapter.setLanguage(CURRENT_LANGUAGE);
//        viewPager.setAdapter(adapter);
//        adTabLayout.setupWithViewPager(viewPager);
//        adTabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
//            @Override
//            public void onTabSelected(TabLayout.Tab tab) {
//                viewPager.setCurrentItem(tab.getPosition());
//            }
//
//            @Override
//            public void onTabUnselected(TabLayout.Tab tab) {
//
//            }
//
//            @Override
//            public void onTabReselected(TabLayout.Tab tab) {
//
//            }
//        });
//        updateTABLanguage();
//    }
//
//    @Subscribe
//    public void onLanguageChangeEventReceived(Events.LanguageChangeEvent event) {
//        CURRENT_LANGUAGE = event.isLang();
//
//        updateTABLanguage();
//    }
//
//    private void updateTABLanguage() {
//        adapter.setLanguage(CURRENT_LANGUAGE);
//        for (int i = 0; i < adTabLayout.getTabCount(); i++) {
//            adTabLayout.getTabAt(i).setText(adapter.getTabTitle(i));
//        }
//    }
//
//    @Override
//    public void onDestroyView() {
//        super.onDestroyView();
//        EventBus.unregister(this);
//    }
//}
