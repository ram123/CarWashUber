package com.ronem.carwash.view.dialogs;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.TimePicker;
import android.widget.Toast;

import com.ronem.carwash.R;
import com.ronem.carwash.interfaces.OnDateTimeListener;
import com.ronem.carwash.utils.SessionManager;
import com.roomorama.caldroid.CaldroidFragment;
import com.roomorama.caldroid.CaldroidListener;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by ronem on 9/10/17.
 */

public class DateTimeDialogFragment extends BaseDialogFragment {

    private OnDateTimeListener dateTimeListener;
    private CaldroidFragment caldroidFragment;
    private Date TODAYS_DATE;
    private ColorDrawable TODAYS_DATE_BG_COLOR;

    private String date, time;
    @Bind(R.id.timePicker)
    TimePicker timePicker;
    @Bind(R.id.datePicker)
    DatePicker datePicker;

    private SessionManager sessionManager;
    private String selectDate,selectTime;
    private boolean arabic;

    public void setOnDateTimeListener(OnDateTimeListener dateTimeListener) {
        this.dateTimeListener = dateTimeListener;
    }

    private void setCustomResourceForDates() {

        Calendar cal = Calendar.getInstance();
        TODAYS_DATE = cal.getTime();


        if (caldroidFragment != null) {
            //defining cell color
            TODAYS_DATE_BG_COLOR = new ColorDrawable(getResources().getColor(R.color.colorPrimary));

            //set the background of cell and text color
            caldroidFragment.setBackgroundDrawableForDate(TODAYS_DATE_BG_COLOR, TODAYS_DATE);
            caldroidFragment.setTextColorForDate(R.color.color_white, TODAYS_DATE);


        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.date_time_picker_layout, container, false);
        ButterKnife.bind(this, v);
        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        sessionManager = new SessionManager(getContext());
        arabic = sessionManager.getCurrentLanguage();

        selectDate = arabic?getString(R.string.select_date_arabic):getString(R.string.select_date);
        selectTime = arabic?getString(R.string.select_time_arabic):getString(R.string.select_time);

        final SimpleDateFormat formatter = new SimpleDateFormat("dd/MMM/yyyy");
        caldroidFragment = new CaldroidFragment();
        if (savedInstanceState != null) {
            caldroidFragment.restoreStatesFromKey(savedInstanceState,
                    "CALDROID_SAVED_STATE");
        }
        // If activity is created from fresh
        else {
            Bundle args = new Bundle();
            Calendar cal = Calendar.getInstance();
            args.putInt(CaldroidFragment.MONTH, cal.get(Calendar.MONTH) + 1);
            args.putInt(CaldroidFragment.YEAR, cal.get(Calendar.YEAR));
            args.putBoolean(CaldroidFragment.ENABLE_SWIPE, true);
            args.putBoolean(CaldroidFragment.SIX_WEEKS_IN_CALENDAR, true);
            args.putBoolean(CaldroidFragment.SQUARE_TEXT_VIEW_CELL, false);

//            args.putInt(CaldroidFragment.THEME_RESOURCE, com.caldroid.R.style.CaldroidDefaultDark);

            caldroidFragment.setArguments(args);
        }

        setCustomResourceForDates();

        // Attach to the activity
        FragmentTransaction t = getChildFragmentManager().beginTransaction();
        t.replace(R.id.calendar, caldroidFragment);
        t.commit();

        // Setup listener
        final CaldroidListener listener = new CaldroidListener() {

            @Override
            public void onSelectDate(Date d, View view) {

                caldroidFragment.clearDisableDates();
                caldroidFragment.clearSelectedDates();

                ColorDrawable green = new ColorDrawable(Color.GREEN);
                caldroidFragment.setBackgroundDrawableForDate(green, d);
                caldroidFragment.refreshView();

                date = formatter.format(d);

            }

            @Override
            public void onChangeMonth(int month, int year) {
                //// TODO: 3/30/17  when month is scrolled left - right
            }

            @Override
            public void onLongClickDate(Date date, View view) {
            }

            @Override
            public void onCaldroidViewCreated() {
            }

        };

        // Setup Caldroid
        caldroidFragment.setCaldroidListener(listener);

        timePicker.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
            @Override
            public void onTimeChanged(TimePicker timePicker, int i, int i1) {
                time = i + " : " + i1;
            }
        });

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        datePicker.init(
                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH),
                new DatePicker.OnDateChangedListener() {
                    @Override
                    public void onDateChanged(DatePicker datePicker, int year, int month, int day) {
                        date = year +"/"+ month +"/"+ day;
                    }
                });
    }

    @OnClick(R.id.dialog_btn_close)
    public void onDialogClose() {
        dismiss();
    }

    @OnClick(R.id.btn_done)
    public void onBtnDoneClicked() {
        if (TextUtils.isEmpty(date)) {
            Toast.makeText(getContext(), selectDate, Toast.LENGTH_SHORT).show();
        } else if (TextUtils.isEmpty(time)) {
            Toast.makeText(getContext(), selectTime, Toast.LENGTH_SHORT).show();
        } else {
            dateTimeListener.onDateTimeReceived(date, time);
            dismiss();
        }
    }

}
