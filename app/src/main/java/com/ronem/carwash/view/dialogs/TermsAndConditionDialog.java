package com.ronem.carwash.view.dialogs;

import android.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.ronem.carwash.R;
import com.ronem.carwash.utils.SessionManager;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by ram on 10/20/17.
 */

public class TermsAndConditionDialog extends DialogFragment {
    @Bind(R.id.terms_condition_title)
    TextView titleView;
    @Bind(R.id.terms_condition_detail)
    TextView detailView;
    @Bind(R.id.terms_condition_close)
    Button btnTerms;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.terms_condition_layout, container, false);
        ButterKnife.bind(this, root);
        return root;

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        SessionManager sessionManager = new SessionManager(getActivity());
        boolean arabic = sessionManager.getCurrentLanguage();
        String title = arabic ? getString(R.string.terms_condition_arabic) : getString(R.string.terms_condition);
        String detail = arabic ? getString(R.string.terms_condition_detail_arabic) : getString(R.string.terms_condition_detail);
        String btn = arabic ? getString(R.string.close_arabic) : getString(R.string.close);

        titleView.setText(title);
        detailView.setText(detail);
        btnTerms.setText(btn);

    }

    @OnClick(R.id.terms_condition_close)
    public void onTermsConditionCloseClicked() {
        dismiss();
    }
}
