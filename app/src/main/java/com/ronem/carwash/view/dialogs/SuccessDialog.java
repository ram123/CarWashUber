package com.ronem.carwash.view.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.content.ContextCompat;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.ronem.carwash.R;
import com.ronem.carwash.utils.SessionManager;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by ram on 9/20/17.
 */

public class SuccessDialog extends Dialog {

    @Bind(R.id.dialog_title)TextView dialogTitleView;
    @Bind(R.id.dialog_message)
    TextView messageView;
    @Bind(R.id.dialog_icon)
    ImageView icon;

    private SessionManager sessionManager;
    private boolean arabic;


    public SuccessDialog(Context context, String message, boolean isSuccess) {
        super(context, R.style.slideAnimation);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setCancelable(true);
        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        setContentView(R.layout.success_dialog);
        ButterKnife.bind(this);
        sessionManager = new SessionManager(getContext());
        arabic = sessionManager.getCurrentLanguage();


        dialogTitleView.setText(arabic?context.getString(R.string.app_name_arabic):context.getString(R.string.app_name));

        messageView.setText(message);

        if (!isSuccess) {
            icon.setImageResource(R.mipmap.ic_warning_black_36dp);
            icon.setColorFilter(ContextCompat.getColor(context, R.color.color_login));
        }

    }
    @Override
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        ButterKnife.unbind(this);
    }
    @OnClick(R.id.dialog_btn_ok)
    public void onDialogBtnOkclicked() {
        dismiss();
    }
}
