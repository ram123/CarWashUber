package com.ronem.carwash.view.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.Window;

import com.ronem.carwash.R;

import butterknife.ButterKnife;

/**
 * Created by ram on 9/9/17.
 */

public class DialogShowMore extends Dialog {

    public DialogShowMore(Context context, String name, String contact, String distance, String time, int rating) {
        super(context, R.style.slideAnimation);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setCancelable(true);
        try {
            getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        setContentView(R.layout.show_detail_activity);
        ButterKnife.bind(this);
    }
}
