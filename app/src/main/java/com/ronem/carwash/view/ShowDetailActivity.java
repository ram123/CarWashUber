package com.ronem.carwash.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.RatingBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.ronem.carwash.R;
import com.ronem.carwash.adapters.CarTypeAdapter;
import com.ronem.carwash.adapters.ServiceTypeAdapter;
import com.ronem.carwash.model.Address;
import com.ronem.carwash.model.CarType;
import com.ronem.carwash.model.DeliveredStationLocation;
import com.ronem.carwash.model.Order;
import com.ronem.carwash.model.PaymentMethod;
import com.ronem.carwash.model.ServiceType;
import com.ronem.carwash.utils.EventBus;
import com.ronem.carwash.utils.Events;
import com.ronem.carwash.utils.MetaData;
import com.ronem.carwash.utils.MyAnimation;
import com.ronem.carwash.utils.SessionManager;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by ronem on 8/2/17.
 */

public class ShowDetailActivity extends AppCompatActivity
        implements AdapterView.OnItemSelectedListener {

    @Bind(R.id.show_more_distance)
    TextView distanceView;
    @Bind(R.id.show_more_estimation_time)
    TextView timeView;
    @Bind(R.id.car_washer_name)
    TextView carwasherNameView;
    @Bind(R.id.car_washer_contact)
    TextView contactView;
    @Bind(R.id.rating)
    RatingBar ratingBar;

    @Bind(R.id.spinner_car_type)
    Spinner spinnerCarType;
    @Bind(R.id.spinner_service_type)
    Spinner spinnerServiceTYpe;
//    @Bind(R.id.spinner_payment_method)
//    Spinner spinnerPaymentMethod;


    private List<CarType> carTypes;
    private List<ServiceType> serviceTypes;

    private CarType carType;
    private Address address;
    private ServiceType serviceTye;
    private String price;
    private String paymentMethod;
    private List<PaymentMethod> paymentMethods;
    private SessionManager sessionManager;
    private Order order;
    private MyAnimation animation;

    private DeliveredStationLocation deliveredStationLocation;
    private String distance;
    private String time;
    private String orderType;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.show_detail_activity);
        ButterKnife.bind(this);
        animation = new MyAnimation();

        deliveredStationLocation = getIntent().getParcelableExtra(MetaData.KEY_ADDRESS);
        distance = getIntent().getStringExtra(MetaData.KEY_DISTANCE);
        time = getIntent().getStringExtra(MetaData.KEY_DURATION);
        orderType = getIntent().getStringExtra(MetaData.KEY_ORDER_TYPE);
        setIntetnData();

        sessionManager = new SessionManager(this);
        carTypes = MetaData.getCarType();
        serviceTypes = MetaData.getServiceType();
        paymentMethods = MetaData.getpaymentMEthods();

        spinnerCarType.setAdapter(new CarTypeAdapter(carTypes, this));
        spinnerServiceTYpe.setAdapter(new ServiceTypeAdapter(serviceTypes, this));
//        spinnerPaymentMethod.setAdapter(new PaymentSpinnerAdapter(paymentMethods, this));

        spinnerCarType.setOnItemSelectedListener(this);
        spinnerServiceTYpe.setOnItemSelectedListener(this);
//        spinnerPaymentMethod.setOnItemSelectedListener(this);


    }

    private void setIntetnData() {
        distanceView.setText(distance);
        timeView.setText(time);
        carwasherNameView.setText(deliveredStationLocation.getCarWasher());
        contactView.setText(deliveredStationLocation.getContact());
        ratingBar.setNumStars(deliveredStationLocation.getRating());
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        switch (adapterView.getId()) {
            case R.id.spinner_car_type:
                carType = carTypes.get(i);
                break;
            case R.id.spinner_service_type:
                serviceTye = serviceTypes.get(i);
                break;
//            case R.id.spinner_payment_method:
//                paymentMethod = paymentMethods.get(i).getPaymentMethod();
//                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    @OnClick(R.id.btn_make_payment)
    public void onBtnMakePaymentClicked() {
        price = carType.getPrice();

        String totalPrice = getPrice(price, serviceTye.getServiceCharge());
        if (!TextUtils.isEmpty(paymentMethod)) {

            sessionManager.setPaymentDone();

            Order order = new Order(getOrderId(), orderType, deliveredStationLocation.getCarWasher(), "",
                    deliveredStationLocation.getContact(), deliveredStationLocation.getAddress(), carType.getType(),
                    paymentMethod, totalPrice, serviceTye.getServiceType(),
                    sessionManager.getLatitude(), sessionManager.getLongitude(),
                    MetaData.ORDER_STATUS_LIVE, sessionManager.getFullName());
            order.save();
            EventBus.post(new Events.CaptainInfoEvent(orderType, distance, time, deliveredStationLocation.getCarWasher(), deliveredStationLocation.getAddress(), carType.getType()));
            onBackPressed();

        } else {
            Toast.makeText(getApplicationContext(), "Please select at least one payment method", Toast.LENGTH_SHORT).show();
        }
    }

    private String getPrice(String price, String serviceCharge) {

        String p = price;
        String sp = serviceCharge;

        p = p.replace("SR", "");
        sp = sp.replace("SR", "");

        int iP = Integer.parseInt(p);
        int isP = Integer.parseInt(sp);

        int t = iP + isP;

        return String.valueOf(t);
    }

    private int getOrderId() {
        int counter = sessionManager.getLatestCounter();
        counter++;
        sessionManager.setOrderCounter(counter);

        return counter;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home)
            onBackPressed();
        return super.onOptionsItemSelected(item);
    }
}
