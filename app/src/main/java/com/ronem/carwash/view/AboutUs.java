package com.ronem.carwash.view;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import com.ronem.carwash.R;
import com.ronem.carwash.utils.SessionManager;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by ram on 10/20/17.
 */

public class AboutUs extends AppCompatActivity {
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.about_us_title)
    TextView aboutUsTitleView;
    @Bind(R.id.about_us_detail)
    TextView aboutUsDetailView;
    @Bind(R.id.about_us_version)
    TextView versionTV;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.about_us);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        String title = getIntent().getStringExtra("title");
        aboutUsTitleView.setText(title);

        SessionManager sessionManager = new SessionManager(this);
        boolean arabic = sessionManager.getCurrentLanguage();

        if (arabic) {
            aboutUsDetailView.setText(getString(R.string.about_us_detail_arabic));
        } else {
            aboutUsDetailView.setText(getString(R.string.about_us_detail));
        }

        PackageManager manager = getPackageManager();
        PackageInfo info = null;
        try {
            info = manager.getPackageInfo(
                    getPackageName(), 0);
            String version = info.versionName;
            versionTV.setText(getString(R.string.version_,version));
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) finish();
        return super.onOptionsItemSelected(item);
    }
}
