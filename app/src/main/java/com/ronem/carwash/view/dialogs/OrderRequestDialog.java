package com.ronem.carwash.view.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TableRow;
import android.widget.TextView;

import com.ronem.carwash.R;
import com.ronem.carwash.model.users.CustomerStationRequest;
import com.ronem.carwash.utils.MetaData;
import com.ronem.carwash.utils.SessionManager;

import java.util.HashMap;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by ram on 10/8/17.
 */

public class OrderRequestDialog extends Dialog {

    private String TAG = getClass().getSimpleName();

    @Bind(R.id.dialog_title_name)
    TextView dialogTitleNameView;
    @Bind(R.id.order_made_by)
    TextView ordermadeByTextView;
    @Bind(R.id.order_made_by_customer_name)
    TextView ordermadeByCustomerView;
    @Bind(R.id.dialog_order_date_key)
    TextView orderDateKey;
    @Bind(R.id.dialog_order_date_value)
    TextView orderDateValue;
    @Bind(R.id.dialog_order_address_key)
    TextView orderAddressKey;
    @Bind(R.id.dialog_order_address_value)
    TextView orderAddressValue;
    @Bind(R.id.dialog_order_vehicles_key)
    TextView orderVehicleTitle;
    @Bind(R.id.dialog_order_services_value)
    TextView orderServiceTitle;
    @Bind(R.id.row_2)
    TableRow tableRow2;
    @Bind(R.id.row_3)
    TableRow tableRow3;
    @Bind(R.id.dialog_order_car1_key)
    TextView orderCar1Key;
    @Bind(R.id.dialog_order_car2_key)
    TextView orderCar2Key;
    @Bind(R.id.dialog_order_car3_key)
    TextView orderCar3Key;
    @Bind(R.id.dialog_order_service1_value)
    TextView orderService1value;
    @Bind(R.id.dialog_order_service2_value)
    TextView orderService2value;
    @Bind(R.id.dialog_order_service3_value)
    TextView orderService3value;
    @Bind(R.id.dialog_close)
    Button btnDialogClose;


    private SessionManager sessionManager;
    private boolean arabic;
    private CustomerStationRequest csr;

    public OrderRequestDialog(@NonNull Context context, CustomerStationRequest csr) {
        super(context, R.style.slideAnimation);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setCancelable(true);
        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        setContentView(R.layout.order_request_dialog);
        ButterKnife.bind(this);
        this.csr = csr;
        sessionManager = new SessionManager(getContext());
        arabic = sessionManager.getCurrentLanguage();

        setDetail();

    }

    private void setDetail() {
        dialogTitleNameView.setText(arabic ? getContext().getString(R.string.app_name_arabic) : getContext().getString(R.string.app_name));
        ordermadeByTextView.setText(arabic ? getContext().getString(R.string.d_order_made_by_arabic) : getContext().getString(R.string.d_order_made_by));
        ordermadeByCustomerView.setText(csr.getcName());
        orderDateKey.setText(arabic ? getContext().getString(R.string.date_arabic) : getContext().getString(R.string.date));
        orderDateValue.setText(csr.getDateTime());
        orderAddressKey.setText(arabic ? getContext().getString(R.string.d_address_arabic) : getContext().getString(R.string.d_address));
        orderAddressValue.setText(csr.getcAddress());
        orderVehicleTitle.setText(arabic ? getContext().getString(R.string.vehicle_type_arabic) : getContext().getString(R.string.vehicle_type));
        orderServiceTitle.setText(arabic ? getContext().getString(R.string.service_type_arabic) : getContext().getString(R.string.service_type));

        HashMap<String, HashMap<String, String>> cars = csr.getCars();
        int i = 1;
        for (Map.Entry<String, HashMap<String, String>> entry : cars.entrySet()) {
            HashMap<String, String> car = entry.getValue();
            String carType = car.get(MetaData.KEY.CAR_TYPE);
            String serviceType = car.get(MetaData.KEY.SERVIE_TYPE);
            if (i == 1) {
                orderCar1Key.setText(carType);
                orderService1value.setText(serviceType);
            } else if (i == 2) {
                orderCar2Key.setText(carType);
                orderService2value.setText(serviceType);
            } else if (i == 3) {
                orderCar3Key.setText(carType);
                orderService3value.setText(serviceType);
            }
            i++;
        }

        int size = cars.size();
        if (size == 1) {
            tableRow2.setVisibility(View.GONE);
            tableRow3.setVisibility(View.GONE);
        } else if (size == 2) {
            tableRow3.setVisibility(View.GONE);
        }


        btnDialogClose.setText(arabic ? getContext().getString(R.string.close_arabic) : getContext().getString(R.string.close));
    }

    @OnClick(R.id.dialog_close)
    public void onDialogClose() {
        dismiss();
    }
}
