package com.ronem.carwash.view.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ronem.carwash.R;
import com.ronem.carwash.interfaces.ShowDialogEventListener;
import com.ronem.carwash.model.users.SalesManStation;
import com.ronem.carwash.utils.MyAnimation;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by ronem on 9/6/17.
 */

public class ShowDetailDialog extends Dialog {
    @Bind(R.id.brief_info_layout)
    LinearLayout breifInfoLayout;
    @Bind(R.id.detail_info_layout)
    LinearLayout detailInfoLayout;
    @Bind(R.id.distance)
    TextView distanceView;
    @Bind(R.id.time_to_reach)
    TextView timeToReach;
    @Bind(R.id.time_to_reach_location)
    TextView location;
    @Bind(R.id.car_washer)
    TextView carWasherV;
    @Bind(R.id.dialog_btn_show_more)
    Button btnRequest;


    private ShowDialogEventListener listener;

    private SalesManStation salesManStation;
    private String distance;
    private String duration;
    private String orderType;
    private MyAnimation animation;

    public ShowDetailDialog(Context context, String distance, String duration, SalesManStation salesManStation, String orderType) {
        super(context, R.style.slideAnimation);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setCancelable(true);
        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        setContentView(R.layout.my_dialog);
        ButterKnife.bind(this);

        this.salesManStation = salesManStation;
        this.distance = distance;
        this.duration = duration;
        this.orderType = orderType;

        animation = new MyAnimation();
        detailInfoLayout.setVisibility(View.GONE);

        distanceView.setText(distance);
        timeToReach.setText(duration);

        location.setText(salesManStation.getAddress());
        carWasherV.setText(salesManStation.getName());
        btnRequest.setText("Request");

    }

    public void setOnShowDialogEventListener(ShowDialogEventListener listener) {
        this.listener = listener;
    }

    @Override
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        ButterKnife.unbind(this);
    }

    @OnClick(R.id.dialog_btn_show_more)
    public void onDialogBtnShowMoreClicked() {
//        listener.onRequestClicked();
//        dismiss();
//        animation.collapseView(breifInfoLayout);
        animation.expandView(detailInfoLayout);
    }

    @OnClick(R.id.dialog_btn_close)
    public void onDialogBtnCloseClicked() {
        listener.onCancelClicked();
        dismiss();
    }

}
