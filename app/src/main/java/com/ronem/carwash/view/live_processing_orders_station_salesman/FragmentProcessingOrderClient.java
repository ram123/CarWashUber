//package com.ronem.carwash.view.live_processing_orders_station_salesman;
//
//import android.os.Bundle;
//import android.support.annotation.NonNull;
//import android.support.annotation.Nullable;
//import android.support.v4.app.Fragment;
//import android.support.v7.widget.LinearLayoutManager;
//import android.support.v7.widget.RecyclerView;
//import android.util.Log;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import com.google.android.gms.tasks.OnCompleteListener;
//import com.google.android.gms.tasks.Task;
//import com.google.firebase.database.DataSnapshot;
//import com.google.firebase.database.DatabaseError;
//import com.google.firebase.database.DatabaseReference;
//import com.google.firebase.database.FirebaseDatabase;
//import com.google.firebase.database.ValueEventListener;
//import com.ronem.carwash.R;
//import com.ronem.carwash.adapters.CustomerStationRequestAdapter;
//import com.ronem.carwash.interfaces.OrderChangeStatusListener;
//import com.ronem.carwash.model.users.CustomerStationRequest;
//import com.ronem.carwash.utils.EventBus;
//import com.ronem.carwash.utils.Events;
//import com.ronem.carwash.utils.MetaData;
//import com.ronem.carwash.utils.SessionManager;
//import com.ronem.carwash.view.dashboard.DashboardStationSalesman;
//import com.squareup.otto.Subscribe;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import butterknife.Bind;
//import butterknife.ButterKnife;
//
///**
// * Created by ram on 8/12/17.
// */
//
//public class FragmentProcessingOrderClient extends Fragment implements OrderChangeStatusListener {
//
//    @Bind(R.id.order_recycler_view)
//    RecyclerView recyclerView;
//    @Bind(R.id.recycler_title)
//    TextView recyclerTitle;
//    @Bind(R.id.empty_recycler)
//    TextView emptyTv;
//
//    private SessionManager sessionManager;
//    private boolean CURRENT_LANGUAGE;
//
//    private CustomerStationRequest customerStationRequest;
//    private ArrayList<String> userStack;
//    private List<CustomerStationRequest> processingRequests;
//    private CustomerStationRequestAdapter adapter;
//
//    @Nullable
//    @Override
//    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        View root = inflater.inflate(R.layout.fragment_live_order, container, false);
//        ButterKnife.bind(this, root);
//        EventBus.register(this);
//        sessionManager = new SessionManager(getContext());
//        CURRENT_LANGUAGE = sessionManager.getCurrentLanguage();
//
//        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
//        recyclerView.hasFixedSize();
//        return root;
//    }
//
//    private void updateRecyclerTitle() {
//
//        String rt = CURRENT_LANGUAGE ? getString(R.string.processing_title_arabic) : getString(R.string.processing_title);
//        recyclerTitle.setText(rt);
//
//        String wfa = CURRENT_LANGUAGE ? getString(R.string.processing_empty_arabic) : getString(R.string.processing_empty);
//        emptyTv.setText(wfa);
//    }
//
//
//    @Override
//    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
//        super.onViewCreated(view, savedInstanceState);
//
//        processingRequests = new ArrayList<>();
//        adapter = new CustomerStationRequestAdapter(processingRequests, MetaData.ORDER_STATUS_PROCESSING);
//        adapter.shouldShowInfo();
//        adapter.setLang(CURRENT_LANGUAGE);
//
//        recyclerView.setAdapter(adapter);
//        adapter.setOnOrderChangeStatusListener(this);
//
//        listeningToCustomers();
//
//        updateRecyclerTitle();
//    }
//
//
//    private void listeningToCustomers() {
//        //storage to list the accepted users
//        userStack = new ArrayList<>();
//
//
//        DatabaseReference dbr;
//        if (DashboardStationSalesman.userType.equals(MetaData.USER_TYPE_CLIENT_STATION)) {
//            dbr = FirebaseDatabase
//                    .getInstance()
//                    .getReference()
//                    .child(MetaData.CUSTOMER_STATION_REQUEST);
//        } else {
//            dbr = FirebaseDatabase
//                    .getInstance()
//                    .getReference()
//                    .child(MetaData.CUSTOMER_SALESMAN_REQUEST);
//        }
//        dbr.addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(DataSnapshot dataSnapshot) {
//                Log.i("Client", "request received");
//                if (dataSnapshot.exists()) {
//                    for (DataSnapshot ds : dataSnapshot.getChildren()) {
//                        customerStationRequest = ds.getValue(CustomerStationRequest.class);
//                        Log.i("csr", customerStationRequest.toString());
//                        if (customerStationRequest.getsUserName().equals(DashboardStationSalesman.salesManStation.getUsername())) {
//                            if (customerStationRequest.getAccepted() == MetaData.ORDER_ACCEPTED) {
//                                if (!presentInStack(customerStationRequest.getcUserName())) {
//                                    processingRequests.add(customerStationRequest);
//                                    adapter.notifyItemInserted(processingRequests.size() - 1);
//                                }
//                            }
//                        }
//                    }
//
//                }
//            }
//
//            @Override
//            public void onCancelled(DatabaseError databaseError) {
//
//            }
//        });
//
//    }
//
//    private boolean presentInStack(String userName) {
//        for (String s : userStack) {
//            if (s.equals(userName)) {
//                return true;
//            }
//        }
//        userStack.add(userName);
//        return false;
//    }
//
//    @Override
//    public void onOrderAccepted(CustomerStationRequest csr) {
//
//    }
//
//    @Override
//    public void onOrderRejected(CustomerStationRequest csr) {
//
//    }
//
//    @Override
//    public void onOrderComplete(final CustomerStationRequest csr) {
//        DatabaseReference dbr;
//        if (DashboardStationSalesman.userType.equals(MetaData.USER_TYPE_CLIENT_STATION)) {
//            dbr = FirebaseDatabase.getInstance().getReference().child(MetaData.CUSTOMER_STATION_REQUEST);
//        } else {
//            dbr = FirebaseDatabase.getInstance().getReference().child(MetaData.CUSTOMER_SALESMAN_REQUEST);
//        }
//
//        csr.setAccepted(MetaData.ORDER_COMPLETED);
//        String childNode = csr.getcUserName() + csr.getsUserName();
//        dbr.child(childNode).setValue(csr).addOnCompleteListener(new OnCompleteListener<Void>() {
//            @Override
//            public void onComplete(@NonNull Task<Void> task) {
//                Toast.makeText(getContext(), CURRENT_LANGUAGE ? getString(R.string.order_complete_arabic) : getString(R.string.order_complete), Toast.LENGTH_SHORT).show();
//                DatabaseReference finishDb;
//                if (DashboardStationSalesman.userType.equals(MetaData.USER_TYPE_CLIENT_STATION)) {
//                    finishDb = FirebaseDatabase.getInstance().getReference().child(MetaData.STATION_FINISHED_TASK);
//                } else {
//                    finishDb = FirebaseDatabase.getInstance().getReference().child(MetaData.SALESMAN_FINISHED_TASK);
//                }
//                finishDb.push().setValue(csr);
//
//                if (processingRequests.size() == 1) {
//                    processingRequests.remove(csr);
//                    adapter.notifyDataSetChanged();
//                }
//            }
//        });
//
//    }
//
//    @Subscribe
//    public void onLanguageChangeEventReceived(Events.LanguageChangeEvent event) {
//        CURRENT_LANGUAGE = event.isLang();
//        updateRecyclerTitle();
//        if (processingRequests != null && processingRequests.size() > 0) {
//            adapter.setLang(CURRENT_LANGUAGE);
//            adapter.notifyDataSetChanged();
//        }
//    }
//
//    @Override
//    public void onDestroyView() {
//        super.onDestroyView();
//        EventBus.unregister(this);
//        ButterKnife.unbind(this);
//    }
//}
