package com.ronem.carwash.view.login;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.rm.rmswitch.RMSwitch;
import com.ronem.carwash.BuildConfig;
import com.ronem.carwash.R;
import com.ronem.carwash.adapters.CarCompanyAdapter;
import com.ronem.carwash.adapters.CarTypeRecyclerAdapter;
import com.ronem.carwash.adapters.ColorPlateAdapter;
import com.ronem.carwash.model.CarCompanyModel;
import com.ronem.carwash.model.CarType;
import com.ronem.carwash.model.ColorPlate;
import com.ronem.carwash.model.users.Customer;
import com.ronem.carwash.model.users.SalesManStation;
import com.ronem.carwash.utils.BasicUtilityMethods;
import com.ronem.carwash.utils.MetaData;
import com.ronem.carwash.utils.RecyclerItemClickListener;
import com.ronem.carwash.utils.SessionManager;
import com.ronem.carwash.view.dashboard.DashboardCustomer;
import com.ronem.carwash.view.dashboard.DashboardStationSalesman;
import com.ronem.carwash.view.dialogs.TermsAndConditionDialog;

import java.util.HashMap;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by ram on 7/31/17.
 */

public class LoginRegisterCustomerActivity
        extends AppCompatActivity
        implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        com.google.android.gms.location.LocationListener,
        RecyclerItemClickListener.OnItemClickListener {

    private String TAG = getClass().getSimpleName();

    @Bind(R.id.create_account_layout)
    LinearLayout createAccLayout;
    @Bind(R.id.login_layout)
    LinearLayout loginLayout;

    @Bind(R.id.personal_info_layout)
    LinearLayout personalInfoLayout;

    @Bind(R.id.create_head_title)
    TextView createAccHeadTitle;
    @Bind(R.id.btn_previous_page)
    Button btnPreviousPage;
    @Bind(R.id.btn_next_page)
    Button btnNextPage;
    @Bind(R.id.btn_create_account)
    Button btnCreateAccount;
    @Bind(R.id.btn_already_member)
    Button btnAlreadyMember;
    @Bind(R.id.edt_full_name)
    EditText edtFullName;
    @Bind(R.id.edt_email)
    EditText edtEmail;
    @Bind(R.id.edt_user_name)
    EditText edtUserName;
    @Bind(R.id.edt_password)
    EditText edtPassword;
    @Bind(R.id.edt_confirm_password)
    EditText edtConfirmPassword;
    @Bind(R.id.edt_contact)
    EditText edtContact;

    //    car types
    @Bind(R.id.car_type_layout)
    LinearLayout carTypeLayout;
    @Bind(R.id.car_type_layout_1)
    LinearLayout carTypeLayout1;
    @Bind(R.id.car_type_layout_2)
    LinearLayout carTypeLayout2;
    @Bind(R.id.car_type_layout_3)
    LinearLayout carTypeLayout3;

    @Bind(R.id.choose_vehicle_type_1)
    TextView chooseVehicleType1;
    @Bind(R.id.choose_vehicle_type_2)
    TextView chooseVehicleType2;
    @Bind(R.id.choose_vehicle_type_3)
    TextView chooseVehicleType3;

    @Bind(R.id.car_type_recycler_1)
    RecyclerView carTypeRecycler1;
    @Bind(R.id.car_type_recycler_2)
    RecyclerView carTypeRecycler2;
    @Bind(R.id.car_type_recycler_3)
    RecyclerView carTypeRecycler3;

    @Bind(R.id.car_company_title1)
    TextView carCompanyTitle1;
    @Bind(R.id.car_company_title2)
    TextView carCompanyTitle2;
    @Bind(R.id.car_company_title3)
    TextView carCompanyTitle3;
    @Bind(R.id.second_car)
    TextView secondCar;
    @Bind(R.id.third_car)
    TextView thirdCar;
    @Bind(R.id.btn_add_more_car_type)
    Button addMoreCar;
    @Bind(R.id.car_company_recycler_view_1)
    RecyclerView companyRecyclerView1;
    @Bind(R.id.car_company_recycler_view_2)
    RecyclerView companyRecyclerView2;
    @Bind(R.id.car_company_recycler_view_3)
    RecyclerView companyRecyclerView3;
    @Bind(R.id.edt_plate_no_1)
    EditText edtPlateNo1;
    @Bind(R.id.edt_plate_no_2)
    EditText edtPlateNo2;
    @Bind(R.id.edt_plate_no_3)
    EditText edtPlateNo3;

    @Bind(R.id.choose_color_plate_1)
    TextView chooseColorPlate1;
    @Bind(R.id.choose_color_plate_2)
    TextView chooseColorPlate2;
    @Bind(R.id.choose_color_plate_3)
    TextView chooseColorPlate3;
    @Bind(R.id.car_plate_title1)
    TextView carPlateTitle1;
    @Bind(R.id.car_plate_title2)
    TextView carPlateTitle2;
    @Bind(R.id.car_plate_title3)
    TextView carPlateTitle3;
    @Bind(R.id.color_plate_1)
    RecyclerView colorPlate1;
    @Bind(R.id.color_plate_2)
    RecyclerView colorPlate2;
    @Bind(R.id.color_plate_3)
    RecyclerView colorPlate3;
    @Bind(R.id.terms_checkbox)
    CheckBox termsCheckBox;

    //    login section
    @Bind(R.id.login_head_title)
    TextView loginHeadTV;
    @Bind(R.id.edt_login_email)
    EditText edtLoginEmail;
    @Bind(R.id.edt_login_password)
    EditText edtLoginPassword;
    @Bind(R.id.btn_login)
    Button btnLogin;
    @Bind(R.id.btn_new_to_carwash_wash)
    Button btnNewToCarwash;

    @Bind(R.id.language_switch)
    RMSwitch languageSwitch;

    private SessionManager sessionManager;
    private final int PERMISSION_REQUEST_CODE = 100;
    private List<CarType> carTypes1, carTypes2, carTypes3;
    private List<CarCompanyModel> carCompanies1, carCompanies2, carCompanies3;
    private String carCompany1, carCompany2, carCompany3;
    private List<ColorPlate> colorPlates1, colorPlates2, colorPlates3;
    private String color1, color2, color3;
    private CarType carType1, carType2, carType3;
    private double myLatitude, myLongitude;

    private GoogleApiClient googleApiClient;
    private LocationRequest locationRequest;
    private int totalCars = 1;
    private boolean isFromLogin = false;
    private boolean isTermsCheckBoxClicked = false;
    private DatabaseReference dbr;
    private ProgressDialog progressDialog;
    private boolean CURRENT_LANGUAGE;
    private String userType;

    private String pw, vr, emv, cbe4, sbc1, sbc2, sbc3, aec, aeu, pwal, sclr, urs, upwrong, agps, eInternet, msg_terms_condition;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        super.onCreate(savedInstanceState);

        setContentView(R.layout.login_register_layout);
        ButterKnife.bind(this);
        sessionManager = new SessionManager(this);

        setUpUiLang();

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(pw);
        sessionManager = new SessionManager(this);

//        String eventType = getIntent().getStringExtra(MetaData.KEY.EVENT_TYPE);
//        if (eventType.equals(MetaData.VALUE.EVENT_TYPE_LOGIN)) {
//            loginLayout.setVisibility(View.VISIBLE);
//            createAccLayout.setVisibility(View.INVISIBLE);
//        } else {
//            loginLayout.setVisibility(View.INVISIBLE);
//            createAccLayout.setVisibility(View.VISIBLE);
//            enableNext();
//        }

        loginLayout.setVisibility(View.VISIBLE);
        createAccLayout.setVisibility(View.INVISIBLE);

        /**Car type RecyclerView **/
        configureRecyclerView(carTypeRecycler1);
        configureRecyclerView(carTypeRecycler2);
        configureRecyclerView(carTypeRecycler3);

        carTypes1 = MetaData.getCarType();
        carTypes2 = MetaData.getCarType();
        carTypes3 = MetaData.getCarType();

        carTypeRecycler1.setAdapter(new CarTypeRecyclerAdapter(carTypes1, this));
        carTypeRecycler2.setAdapter(new CarTypeRecyclerAdapter(carTypes2, this));
        carTypeRecycler3.setAdapter(new CarTypeRecyclerAdapter(carTypes3, this));

        /**Car company RecyclerView **/
        configureRecyclerView(companyRecyclerView1);
        configureRecyclerView(companyRecyclerView2);
        configureRecyclerView(companyRecyclerView3);

        carCompanies1 = MetaData.getCarCompany();
        carCompanies2 = MetaData.getCarCompany();
        carCompanies3 = MetaData.getCarCompany();

        companyRecyclerView1.setAdapter(new CarCompanyAdapter(carCompanies1, this));
        companyRecyclerView2.setAdapter(new CarCompanyAdapter(carCompanies2, this));
        companyRecyclerView3.setAdapter(new CarCompanyAdapter(carCompanies3, this));

        /**Car color plate RecyclerView **/
        configureRecyclerView(colorPlate1);
        configureRecyclerView(colorPlate2);
        configureRecyclerView(colorPlate3);

        /**Car color plates data list **/
        colorPlates1 = MetaData.getColorPlates();
        colorPlates2 = MetaData.getColorPlates();
        colorPlates3 = MetaData.getColorPlates();

        /** color plates recycler view's adapter **/
        colorPlate1.setAdapter(new ColorPlateAdapter(colorPlates1, this));
        colorPlate2.setAdapter(new ColorPlateAdapter(colorPlates2, this));
        colorPlate3.setAdapter(new ColorPlateAdapter(colorPlates3, this));

        createGoogleApiClient();

        checkRunTimePermissionLaunchDashboard();
        locationRequest = BasicUtilityMethods.createLocationRequest();

        languageSwitch();

        termsCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                isTermsCheckBoxClicked = b;
                if (isTermsCheckBoxClicked) {
                    TermsAndConditionDialog dialog = new TermsAndConditionDialog();
                    dialog.setStyle(DialogFragment.STYLE_NO_TITLE, 0);
                    dialog.show(getFragmentManager(), "");
                }
            }
        });
        //adding tem data
        if (BuildConfig.DEBUG) {
            MetaData.addingTempStationsAndSalesman();
        }
    }

    private void languageSwitch() {
        languageSwitch.setChecked(CURRENT_LANGUAGE);
        languageSwitch.addSwitchObserver(new RMSwitch.RMSwitchObserver() {
            @Override
            public void onCheckStateChange(RMSwitch switchView, boolean isChecked) {
                CURRENT_LANGUAGE = isChecked;

                //we assumed true as arabic
                //and false as english
                if (CURRENT_LANGUAGE) {
                    sessionManager.switchToArabic();
                } else {
                    sessionManager.switchToEnglish();
                }
                setUpUiLang();
            }
        });
    }

    private void setUpUiLang() {
        CURRENT_LANGUAGE = sessionManager.getCurrentLanguage();

        termsCheckBox.setText(
                CURRENT_LANGUAGE ? getString(R.string.accept_terms_condition_arabic) : getString(R.string.accept_terms_condition)
        );
        /**update the login view
         * with language English or Arabic
         */

        pw = CURRENT_LANGUAGE ? getString(R.string.please_wait_arabic) : getString(R.string.please_wait);
        String lt = CURRENT_LANGUAGE ? getString(R.string.login_arabic) : getString(R.string.login);
        String elu = CURRENT_LANGUAGE ? getString(R.string.user_name_mobile_number_arabic) : getString(R.string.user_name_mobile_number);
        String elp = CURRENT_LANGUAGE ? getString(R.string.password_arabic) : getString(R.string.password);
        String bl = CURRENT_LANGUAGE ? getString(R.string.btn_login_arabic) : getString(R.string.btn_login);
        String bc = CURRENT_LANGUAGE ? getString(R.string.new_member_arabic) : getString(R.string.new_member);
        loginHeadTV.setText(lt);
        edtLoginEmail.setHint(elu);
        edtLoginPassword.setHint(elp);
        btnLogin.setText(bl);
        btnNewToCarwash.setText(bc);

        /**
         * update the create new account view
         * with languge English or Arabic
         */

        String ct = CURRENT_LANGUAGE ? getString(R.string.create_account_arabic) : getString(R.string.create_account);
        String efn = CURRENT_LANGUAGE ? getString(R.string.full_name_arabic) : getString(R.string.full_name);
        String ee = CURRENT_LANGUAGE ? getString(R.string.email_arabic) : getString(R.string.email);
        String eun = CURRENT_LANGUAGE ? getString(R.string.edt_user_name_arabic) : getString(R.string.edt_user_name);
        String ec = CURRENT_LANGUAGE ? getString(R.string.contact_arabic) : getString(R.string.contact);
        String ep = CURRENT_LANGUAGE ? getString(R.string.password_arabic) : getString(R.string.password);
        String ecp = CURRENT_LANGUAGE ? getString(R.string.confirm_password_arabic) : getString(R.string.confirm_password);
        String bprev = CURRENT_LANGUAGE ? getString(R.string.back_arabic) : getString(R.string.back);
        String bnext = CURRENT_LANGUAGE ? getString(R.string.next_page_arabic) : getString(R.string.next_page);
        String bjoin = CURRENT_LANGUAGE ? getString(R.string.confirm_arabic) : getString(R.string.confirm);
        String bam = CURRENT_LANGUAGE ? getString(R.string.already_member_arabic) : getString(R.string.already_member);

        String company = CURRENT_LANGUAGE ? getString(R.string.choose_car_company_arabic) : getString(R.string.choose_car_company);

        String sc = CURRENT_LANGUAGE ? getString(R.string.car_second_arabic) : getString(R.string.car_second);
        String tc = CURRENT_LANGUAGE ? getString(R.string.car_third_arabic) : getString(R.string.car_third);
        String cp = CURRENT_LANGUAGE ? getString(R.string.car_plate_arabic) : getString(R.string.car_plate);
        String cc = CURRENT_LANGUAGE ? getString(R.string.choose_color_plate_arabic) : getString(R.string.choose_color_plate);
        String vt = CURRENT_LANGUAGE ? getString(R.string.vehicle_type_arabic) : getString(R.string.vehicle_type);
        String addmore = CURRENT_LANGUAGE ? getString(R.string.add_more_car_type_arabic) : getString(R.string.add_more_car_type);

        //add string to the view
        createAccHeadTitle.setText(ct);
        edtFullName.setHint(efn);
        edtEmail.setHint(ee);
        edtUserName.setHint(eun);
        edtContact.setHint(ec);
        edtPassword.setHint(ep);
        edtConfirmPassword.setHint(ecp);
        btnPreviousPage.setText(bprev);
        btnNextPage.setText(bnext);
        btnCreateAccount.setText(bjoin);
        btnAlreadyMember.setText(bam);
        carCompanyTitle1.setText(company);
        carCompanyTitle2.setText(company);
        carCompanyTitle3.setText(company);
        secondCar.setText(sc);
        thirdCar.setText(tc);
        chooseVehicleType1.setText(vt);
        chooseVehicleType2.setText(vt);
        chooseVehicleType3.setText(vt);
        carPlateTitle1.setText(cp);
        carPlateTitle2.setText(cp);
        carPlateTitle3.setText(cp);
        chooseColorPlate1.setText(cc);
        chooseColorPlate2.setText(cc);
        chooseColorPlate3.setText(cc);


        addMoreCar.setText(addmore);

        //message
        eInternet = CURRENT_LANGUAGE ? getString(R.string.enable_internet_arabic) : getString(R.string.enable_internet);
        emv = CURRENT_LANGUAGE ? getString(R.string.email_not_valid_arabic) : getString(R.string.email_not_valid);
        sbc1 = CURRENT_LANGUAGE ? getString(R.string.select_brand_1_arabic) : getString(R.string.select_brand_1);
        sbc2 = CURRENT_LANGUAGE ? getString(R.string.select_brand_2_arabic) : getString(R.string.select_brand_2);
        sbc3 = CURRENT_LANGUAGE ? getString(R.string.select_brand_3_arabic) : getString(R.string.select_brand_3);
        vr = CURRENT_LANGUAGE ? getString(R.string.value_required_arabic) : getString(R.string.value_required);
        cbe4 = CURRENT_LANGUAGE ? getString(R.string.cannot_accept_greater_than_4_arabic) : getString(R.string.cannot_accept_greater_than_4);
        aec = CURRENT_LANGUAGE ? getString(R.string.user_already_exist_contact_arabic) : getString(R.string.user_already_exist_contact);
        aeu = CURRENT_LANGUAGE ? getString(R.string.user_already_exist_user_name_arabic) : getString(R.string.user_already_exist_user_name);
        pwal = CURRENT_LANGUAGE ? getString(R.string.please_wait_accessing_location_arabic) : getString(R.string.please_wait_accessing_location);
        sclr = CURRENT_LANGUAGE ? getString(R.string.select_car_color_arabic) : getString(R.string.select_car_color);
        urs = CURRENT_LANGUAGE ? getString(R.string.user_registered_success_arabic) : getString(R.string.user_registered_success);
        upwrong = CURRENT_LANGUAGE ? getString(R.string.user_password_wrong_arabic) : getString(R.string.user_password_wrong);
        agps = CURRENT_LANGUAGE ? getString(R.string.allow_gps_arabic) : getString(R.string.allow_gps);
        msg_terms_condition = CURRENT_LANGUAGE ? getString(R.string.need_to_accept_arabic) : getString(R.string.need_to_accept);
    }

    private void configureRecyclerView(RecyclerView recyclerView) {
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        recyclerView.setHasFixedSize(true);
        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(this, this));
    }


    private void createGoogleApiClient() {
        googleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        ButterKnife.unbind(this);
    }

    private int getUserID() {

        int counter = sessionManager.getLatestUserCounter();
        counter++;
        sessionManager.setUserCounter(counter);

        return counter;
    }

    private void checkRunTimePermissionLaunchDashboard() {
        if (Build.VERSION.SDK_INT >= 23
                && ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
                showMessageOKCancel(agps,
                        new DialogInterface.OnClickListener() {
                            @RequiresApi(api = Build.VERSION_CODES.M)
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                requestPermissions(new String[]{
                                        android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION
                                }, PERMISSION_REQUEST_CODE);
                            }
                        });
            }

            requestPermissions(new String[]{
                    android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION
            }, PERMISSION_REQUEST_CODE);

        } else {
            BasicUtilityMethods.checkifGPSisEnabled(this);
            googleApiClient.connect();
        }
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSION_REQUEST_CODE) {

            //if user has granted the permissions
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Log.i("Permission", "received");
                if (!googleApiClient.isConnected()) {
                    Log.i("GoogleClient", "was not connected");
                    googleApiClient.connect();
                }
                if (!BasicUtilityMethods.isGPSEnabled(this)) {
                    BasicUtilityMethods.openGPSSettingDialog(this);
                }
            } else {
                checkRunTimePermissionLaunchDashboard();
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @OnClick(R.id.btn_login)
    public void onBtnLoginClicked() {
        isFromLogin = true;
        final String userName = edtLoginEmail.getText().toString();
        final String password = edtLoginPassword.getText().toString();

        if (TextUtils.isEmpty(userName)
                || TextUtils.isEmpty(password)) {
            showMessage(MetaData.MSG_EMPTY_FIELD);
        } else {
            if (!BasicUtilityMethods.isNetworkOnline(this)) {
                showMessage(eInternet);
                return;
            }
            progressDialog.show();
            DatabaseReference dbr = FirebaseDatabase.getInstance().getReference().child(MetaData.USER_TYPE_CUSTOMER);
            dbr.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    userType = MetaData.USER_TYPE_CUSTOMER;

                    /**
                     * Need to check because if the database is changed then this callback method will
                     * be invoked and if the username password is null then app may crash
                     */
                    if (TextUtils.isEmpty(userName) && TextUtils.isEmpty(password))
                        return;

                    if (isFromLogin) {
                        if (dataSnapshot.exists()) {
                            for (DataSnapshot ds : dataSnapshot.getChildren()) {
                                Customer cus = ds.getValue(Customer.class);

                                Log.i("Customer", cus.toString());
                                if (cus.getCustomerMobileNumber().equalsIgnoreCase(userName)
                                        && cus.getPassword().equals(password)) {
                                    sessionManager.setCustomerLogin(userType, cus);
                                    launchCustomerDashboard();
                                    return;

                                } else if (cus.getUserName().equals(userName)
                                        && cus.getPassword().equals(password)) {
                                    Log.i("matched", "name");
                                    sessionManager.setCustomerLogin(userType, cus);
                                    launchCustomerDashboard();
                                    return;

                                }
                            }
//                            Toast.makeText(getApplicationContext(), upwrong, Toast.LENGTH_SHORT).show();
                            /**
                             * If the user is not present in the customer table
                             * then open the client station reference and search for the
                             * user name in station to identify if the user is station client
                             */
                            checkForStationUser(userName, password);
                        }
                    }

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    if (progressDialog.isShowing()) progressDialog.dismiss();
                    if (isFromLogin) {
                        Log.i("Error", String.valueOf(databaseError.toException()));
                        databaseError.toException().printStackTrace();
                    }
                }
            });

        }
    }

    private void checkForStationUser(final String userName, final String password) {
        DatabaseReference dbr = FirebaseDatabase.getInstance().getReference().child(MetaData.USER_TYPE_CLIENT_STATION);
        dbr.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                userType = MetaData.USER_TYPE_CLIENT_STATION;

                /**
                 * Need to check because if the database is changed then this callback method will
                 * be invoked and if the username password is null then app may crash
                 */
                if (TextUtils.isEmpty(userName) && TextUtils.isEmpty(password))
                    return;

                for (DataSnapshot ds : dataSnapshot.getChildren()) {
                    SalesManStation sms = ds.getValue(SalesManStation.class);

                    if (sms.getMobileNumber().equalsIgnoreCase(userName)
                            && sms.getPassword().equals(password)) {
                        sessionManager.setClientLogin(userType, sms);
                        launchDashboard(sms);
                        return;

                    } else if (sms.getUsername().equals(userName)
                            && sms.getPassword().equals(password)) {
                        Log.i("matched", "name");
                        sessionManager.setClientLogin(userType, sms);
                        launchDashboard(sms);
                        return;

                    }
                }
//                Toast.makeText(getApplicationContext(), "Sorry user not found with this username and password", Toast.LENGTH_SHORT).show();
                /**
                 * If the user is not present in the station table
                 * then open the client salesman reference and search for the
                 * user name in salesman to identify if the user is salesman client
                 */

                checkForSalesManUser(userName, password);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                if (progressDialog.isShowing()) progressDialog.dismiss();
            }
        });
    }

    private void checkForSalesManUser(final String userName, final String password) {
        DatabaseReference dbr = FirebaseDatabase.getInstance().getReference().child(MetaData.USER_TYPE_CLIENT_SALESMAN);
        dbr.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                userType = MetaData.USER_TYPE_CLIENT_SALESMAN;
                /**
                 * Need to check because if the database is changed then this callback method will
                 * be invoked and if the username password is null then app may crash
                 */
                if (TextUtils.isEmpty(userName) && TextUtils.isEmpty(password))
                    return;

                for (DataSnapshot ds : dataSnapshot.getChildren()) {
                    SalesManStation sms = ds.getValue(SalesManStation.class);

                    if (sms.getMobileNumber().equalsIgnoreCase(userName)
                            && sms.getPassword().equals(password)) {
                        sessionManager.setClientLogin(userType, sms);
                        launchDashboard(sms);
                        return;

                    } else if (sms.getUsername().equals(userName)
                            && sms.getPassword().equals(password)) {
                        Log.i("matched", "name");
                        sessionManager.setClientLogin(userType, sms);
                        launchDashboard(sms);
                        return;

                    }
                }
                Toast.makeText(getApplicationContext(), upwrong, Toast.LENGTH_SHORT).show();
                if (progressDialog.isShowing()) progressDialog.dismiss();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                if (progressDialog.isShowing()) progressDialog.dismiss();
            }
        });
    }

    @OnClick(R.id.btn_new_to_carwash_wash)
    public void onBtnNewToCarwashClicked() {
        createAccLayout.setVisibility(View.VISIBLE);
        loginLayout.setVisibility(View.GONE);

        enableNext();
    }

    private void enableNext() {
        personalInfoLayout.setVisibility(View.VISIBLE);
        carTypeLayout.setVisibility(View.GONE);
        btnPreviousPage.setVisibility(View.INVISIBLE);
        btnNextPage.setVisibility(View.VISIBLE);
    }

    private void enablePrevious() {
        personalInfoLayout.setVisibility(View.GONE);
        carTypeLayout.setVisibility(View.VISIBLE);
        btnPreviousPage.setVisibility(View.VISIBLE);
        btnNextPage.setVisibility(View.INVISIBLE);
    }


    @OnClick(R.id.btn_create_account)
    public void onBtnCreateAccountClicked() {
        isFromLogin = false;
        final String fullName = edtFullName.getText().toString();
        final String email = edtEmail.getText().toString();
        final String userName = edtUserName.getText().toString();
        String password = edtPassword.getText().toString();
        String confirmPassword = edtConfirmPassword.getText().toString();
        final String contact = edtContact.getText().toString();

        final String carPlateNo1 = edtPlateNo1.getText().toString();
        final String carPlateNo2 = edtPlateNo2.getText().toString();
        final String carPlateNo3 = edtPlateNo3.getText().toString();


        if (TextUtils.isEmpty(fullName)) {
            edtFullName.setError(vr);
        } else if (TextUtils.isEmpty(email)) {
            edtEmail.setError(vr);
        } else if (!BasicUtilityMethods.isValidEmail(email)) {
            edtEmail.setError(emv);
        } else if (TextUtils.isEmpty(password)) {
            edtPassword.setError(vr);
        } else if (TextUtils.isEmpty(confirmPassword)) {
            edtConfirmPassword.setError(vr);
        } else if (TextUtils.isEmpty(contact)) {
            edtContact.setError(vr);
        } else if (!password.equals(confirmPassword)) {
            showMessage(MetaData.MSG_PASSWORD_NOT_MATCHED);
        } else if (personalInfoLayout.getVisibility() == View.VISIBLE) {
            enablePrevious();
        } else {

            if (carType1 == null) {
                showMessage(MetaData.MSG_SELECT_CAR_TYPE);
            } else if (TextUtils.isEmpty(carCompany1)) {
                showMessage(sbc1);
            } else if (TextUtils.isEmpty(carPlateNo1)) {
                edtPlateNo1.setError(vr);
            } else if (carPlateNo1.length() > 4) {
                edtPlateNo1.setError(cbe4);
            } else if (TextUtils.isEmpty(color1)) {
                showMessage(sclr);
            } else {
                if (carTypeLayout2.getVisibility() == View.VISIBLE) {
                    if (carType2 == null) {
                        showMessage(MetaData.MSG_SELECT_CAR_TYPE2);
                    } else if (TextUtils.isEmpty(carCompany2)) {
                        showMessage(sbc2);
                    } else if (TextUtils.isEmpty(carPlateNo2)) {
                        edtPlateNo2.setError(vr);
                    } else if (carPlateNo2.length() > 4) {
                        edtPlateNo2.setError(cbe4);
                    } else if (TextUtils.isEmpty(color2)) {
                        showMessage(sclr);
                    } else {
                        if (carTypeLayout3.getVisibility() == View.VISIBLE) {
                            if (carType3 == null) {
                                showMessage(MetaData.MSG_SELECT_CAR_TYPE3);
                            } else if (TextUtils.isEmpty(carCompany3)) {
                                showMessage(sbc3);
                            } else if (TextUtils.isEmpty(carPlateNo3)) {
                                edtPlateNo3.setError(vr);
                            } else if (carPlateNo3.length() > 4) {
                                edtPlateNo3.setError(cbe4);
                            } else if (TextUtils.isEmpty(color3)) {
                                showMessage(sclr);
                            } else {
                                checkAndRegister(fullName, password, contact, userName, email, carPlateNo1, carPlateNo2, carPlateNo3, color1, color2, color3);
                            }
                        } else {
                            checkAndRegister(fullName, password, contact, userName, email, carPlateNo1, carPlateNo2, carPlateNo3, color1, color2, color3);
                        }
                    }
                } else {
                    checkAndRegister(fullName, password, contact, userName, email, carPlateNo1, carPlateNo2, carPlateNo3, color1, color2, color3);
                }
            }

        }
    }

    private void checkAndRegister(final String fullName, final String password, final String contact, final String userName, final String email, final String carPlateNo1, final String carPlateNo2, final String carPlateNo3, final String carColor1, final String carColor2, final String carColor3) {

        if (!isTermsCheckBoxClicked) {
            showMessage(msg_terms_condition);
            return;
        }
        if (!BasicUtilityMethods.isNetworkOnline(this)) {
            showMessage(eInternet);
            return;
        }
        if (BasicUtilityMethods.isGPSEnabled(this)) {
            if (myLatitude == 0 && myLongitude == 0) {
                showMessage(pwal);
            } else {

                progressDialog.show();
                //when only first car is selected

                //first check if user already exists
                dbr = FirebaseDatabase.getInstance().getReference().child(MetaData.USER_TYPE_CUSTOMER);

                dbr.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (progressDialog.isShowing()) progressDialog.dismiss();
                        //prevent from listening from login OnDataChange
                        if (!isFromLogin) {

                            //just to prevent from listening again again
                            if (dataSnapshot.exists()) {
                                for (DataSnapshot ds : dataSnapshot.getChildren()) {
                                    Customer cus = ds.getValue(Customer.class);
                                    if (cus.getCustomerMobileNumber().equals(contact)) {
                                        Toast.makeText(getApplicationContext(), aec, Toast.LENGTH_SHORT).show();
                                        return;

                                    } else if (cus.getUserName().equals(userName)) {
                                        Toast.makeText(getApplicationContext(), aeu, Toast.LENGTH_SHORT).show();
                                        return;
                                    }
                                }
                                //you are here because user was not exists
                                dbr.removeEventListener(this);
                                registerNewUser(fullName, password, userName, email, contact, carPlateNo1, carPlateNo2, carPlateNo3, carColor1, carColor2, carColor3);


                            } else {
                                //if there is not customer child present then make and register new
                                dbr.removeEventListener(this);
                                registerNewUser(fullName, password, userName, email, contact, carPlateNo1, carPlateNo2, carPlateNo3, carColor1, carColor2, carColor3);
                            }
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        if (progressDialog.isShowing()) progressDialog.dismiss();
                        if (!isFromLogin) {
                            databaseError.toException().printStackTrace();
                        }
                    }
                });
            }
        } else {
            BasicUtilityMethods.openGPSSettingDialog(this);
        }
    }

    private void registerNewUser(String fullName, String password, String userName, String email, String contact, String p1, String p2, String p3, String c1, String c2, String c3) {
        DatabaseReference dbr = FirebaseDatabase.getInstance().getReference().child(MetaData.USER_TYPE_CUSTOMER).child(userName);

        HashMap<String, HashMap<String, String>> carsMap = new HashMap<>();

        for (int i = 1; i <= totalCars; i++) {
            if (i == 1) {
                carsMap.put("car1", getCar(carType1, carCompany1, p1, c1));
            } else if (i == 2) {
                carsMap.put("car2", getCar(carType2, carCompany2, p2, c2));
            } else if (i == 3) {
                carsMap.put("car3", getCar(carType3, carCompany3, p3, c3));
            }
        }

        Log.i("carsSize", carsMap.size() + "");
        Log.i("cars", carsMap.toString());

        Log.i("LatLangFromLocation", myLatitude + ":" + myLongitude);
        final Customer customer = new Customer(fullName, "NA", password, userName, email, contact, myLatitude, myLongitude, carsMap);
        Log.i("LatLangFromCustomer", customer.getLatitude() + ":" + customer.getLongitude());
        dbr.setValue(customer)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            Toast.makeText(getApplicationContext(), urs, Toast.LENGTH_SHORT).show();
                            sessionManager.setCustomerLogin(MetaData.USER_TYPE_CUSTOMER, customer);
                            launchCustomerDashboard();
                        }
                    }

                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        e.printStackTrace();
                    }
                });
    }


    private HashMap<String, String> getCar(CarType ct, String company, String plateNo, String color) {

        HashMap<String, String> car1 = new HashMap<>();
        car1.put(MetaData.KEY.CAR_ID, ct.getId());
        car1.put(MetaData.KEY.CAR_TYPE, ct.getType());
        car1.put(MetaData.KEY.CAR_COMPANY, company);
        car1.put(MetaData.KEY.CAR_PLATE_NO, plateNo);
        car1.put(MetaData.KEY.CAR_COLOR, color);
        return car1;
    }

    @OnClick(R.id.btn_add_more_car_type)
    public void onBtnAddMoreCarTyesClicked() {
        if (carTypeLayout1.getVisibility() == View.VISIBLE
                && carTypeLayout2.getVisibility() == View.GONE) {
            carTypeLayout2.setVisibility(View.VISIBLE);
            totalCars = 2;
        } else if (carTypeLayout2.getVisibility() == View.VISIBLE
                && carTypeLayout3.getVisibility() == View.GONE) {
            carTypeLayout3.setVisibility(View.VISIBLE);
            totalCars = 3;
        } else {
            Toast.makeText(getApplicationContext(), "cannot add more than 3 cars", Toast.LENGTH_SHORT).show();
        }
    }

    @OnClick(R.id.btn_next_page)
    public void onBtnNextPageClicked() {
        String fullName = edtFullName.getText().toString();
        String email = edtEmail.getText().toString();
        String password = edtPassword.getText().toString();
        String confirmPassword = edtConfirmPassword.getText().toString();
        String contact = edtContact.getText().toString();


        if (TextUtils.isEmpty(fullName)) {
            edtFullName.setError(vr);
        } else if (TextUtils.isEmpty(email)) {
            edtEmail.setError(vr);
        } else if (!BasicUtilityMethods.isValidEmail(email)) {
            edtEmail.setError(emv);
        } else if (TextUtils.isEmpty(password)) {
            edtPassword.setError(vr);
        } else if (TextUtils.isEmpty(confirmPassword)) {
            edtConfirmPassword.setError(vr);
        } else if (TextUtils.isEmpty(contact)) {
            edtContact.setError(vr);
        } else if (!password.equals(confirmPassword)) {
            showMessage(MetaData.MSG_PASSWORD_NOT_MATCHED);
        } else if (personalInfoLayout.getVisibility() == View.VISIBLE) {
            enablePrevious();
        }

    }

    @OnClick(R.id.btn_previous_page)
    public void onBtnPreviousPageClicked() {
        if (carTypeLayout.getVisibility() == View.VISIBLE) {
            enableNext();
        }
    }

    @OnClick(R.id.btn_already_member)
    public void onBtnAlreadyMemberClicked() {
        createAccLayout.setVisibility(View.GONE);
        loginLayout.setVisibility(View.VISIBLE);
    }

    private void launchCustomerDashboard() {
        if (progressDialog.isShowing()) progressDialog.dismiss();
        Intent i = new Intent(this, DashboardCustomer.class);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
    }

    private void launchDashboard(SalesManStation sms) {
        if (progressDialog.isShowing()) progressDialog.dismiss();
        DatabaseReference dbr;

        if (userType.equals(MetaData.USER_TYPE_CLIENT_SALESMAN)) {
            dbr = FirebaseDatabase.getInstance().getReference().child(MetaData.AVAILABLE_SALESMANS).child(sms.getUsername());
        } else {
            dbr = FirebaseDatabase.getInstance().getReference().child(MetaData.AVAILABLE_STATIONS).child(sms.getUsername());
        }
        dbr.setValue(sms)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        Toast.makeText(getApplicationContext(), "You are available to customer", Toast.LENGTH_SHORT).show();
                        Intent i = new Intent(LoginRegisterCustomerActivity.this, DashboardStationSalesman.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(i);
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(getApplicationContext(), "Something went wrong try again", Toast.LENGTH_SHORT).show();
            }
        });


    }

    private void showMessage(String msg) {
        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
    }


    @Override
    public void onLocationChanged(Location location) {
        if (location == null)
            return;
        myLatitude = location.getLatitude();
        myLongitude = location.getLongitude();
        Log.i("LOCATION:", myLatitude + myLongitude + "");
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.i("GoogleClient", "Connected");
        requestLocationupdate();
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.i("GoogleClient", "Suspended");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.i("GoogleClient", "Connection Failed");
    }

    @SuppressWarnings("MissingPermission")
    private void requestLocationupdate() {
        LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);
    }

    @Override
    protected void onResume() {
        super.onResume();
//        checkRunTimePermissionLaunchDashboard();
    }

    @Override
    public void onItemClick(RecyclerView recyclerView, View view, int position) {
        switch (recyclerView.getId()) {
            case R.id.car_type_recycler_1:
                carType1 = carTypes1.get(position);
                for (int i = 0; i < carTypes1.size(); i++) {
                    carTypes1.get(i).setChecked(false);
                }
                carType1.setChecked(true);
                carTypeRecycler1.getAdapter().notifyDataSetChanged();
                break;
            case R.id.car_type_recycler_2:
                carType2 = carTypes2.get(position);
                for (int i = 0; i < carTypes2.size(); i++) {
                    carTypes2.get(i).setChecked(false);
                }
                carType2.setChecked(true);
                carTypeRecycler2.getAdapter().notifyDataSetChanged();
                break;
            case R.id.car_type_recycler_3:
                carType3 = carTypes3.get(position);
                for (int i = 0; i < carTypes3.size(); i++) {
                    carTypes3.get(i).setChecked(false);
                }
                carType3.setChecked(true);
                carTypeRecycler3.getAdapter().notifyDataSetChanged();
                break;
            case R.id.car_company_recycler_view_1:
                carCompany1 = carCompanies1.get(position).getName();

                for (int i = 0; i < carCompanies1.size(); i++) {
                    carCompanies1.get(i).setSelected(false);
                }
                carCompanies1.get(position).setSelected(true);
                companyRecyclerView1.getAdapter().notifyDataSetChanged();
                break;

            case R.id.car_company_recycler_view_2:

                for (int i = 0; i < carCompanies2.size(); i++) {
                    carCompanies2.get(i).setSelected(false);
                }
                carCompanies2.get(position).setSelected(true);
                companyRecyclerView2.getAdapter().notifyDataSetChanged();
                carCompany2 = carCompanies2.get(position).getName();
                break;

            case R.id.car_company_recycler_view_3:

                for (int i = 0; i < carCompanies3.size(); i++) {
                    carCompanies3.get(i).setSelected(false);
                }
                carCompanies3.get(position).setSelected(true);
                companyRecyclerView3.getAdapter().notifyDataSetChanged();
                carCompany3 = carCompanies3.get(position).getName();
                break;

            case R.id.color_plate_1:
                notifyColorPlates(1, position, colorPlates1, colorPlate1);
                break;
            case R.id.color_plate_2:
                notifyColorPlates(2, position, colorPlates2, colorPlate2);
                break;
            case R.id.color_plate_3:
                notifyColorPlates(3, position, colorPlates3, colorPlate3);
                break;
        }
    }

    private void notifyColorPlates(int which, int position, List<ColorPlate> cps, RecyclerView recyclerView) {
        ColorPlate cp = cps.get(position);
        String name = cp.getColorName();
        int code = cp.getColor();
        String hexColor = String.format("#%06X", (0xFFFFFF & code));

        switch (which) {
            case 1:
                color1 = name + hexColor;
                Log.i(TAG, "1" + color1);
                break;
            case 2:
                color2 = name + hexColor;
                Log.i(TAG, "2" + color2);
                break;
            case 3:
                color3 = name + hexColor;
                Log.i(TAG, "3" + color3);
                break;
        }
        for (int i = 0; i < cps.size(); i++) {
            cps.get(i).setChecked(false);
        }
        cp.setChecked(true);
        recyclerView.getAdapter().notifyDataSetChanged();
    }
}
