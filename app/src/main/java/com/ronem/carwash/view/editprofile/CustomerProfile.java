package com.ronem.carwash.view.editprofile;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ronem.carwash.R;
import com.ronem.carwash.model.CarType;
import com.ronem.carwash.model.users.Customer;
import com.ronem.carwash.model.users.SalesManStation;
import com.ronem.carwash.utils.MetaData;
import com.ronem.carwash.utils.SessionManager;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by ram on 8/5/17.
 */

public class CustomerProfile extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.edit_profile_title)TextView edtProfileTitleV;
    @Bind(R.id.edt_full_name)
    EditText edtFullName;
    @Bind(R.id.edt_email)
    EditText edtEmail;
    @Bind(R.id.edt_contact)
    EditText edtContact;
    @Bind(R.id.car_type_layout)
    LinearLayout carTypeLayout;

    private SessionManager sessionManager;
    private List<CarType> carTypes;
    private CarType carType;
    private String userType;
    private Customer customer;
    private SalesManStation client;
    private boolean arabic;
    private String pUs;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_profile_layout);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        sessionManager = new SessionManager(this);
        arabic = sessionManager.getCurrentLanguage();

        updateUiLang();

        configureSCarTypeSpinner();
        userType = sessionManager.getUserType();
        carTypeLayout.setVisibility(View.GONE);

        if (!userType.equals(MetaData.USER_TYPE_CUSTOMER)) {
            carTypeLayout.setVisibility(View.GONE);

            customer = sessionManager.getCustomer();
            Log.i("CusProfile",customer.toString());
            edtFullName.setText(customer.getCustomerName());
            edtEmail.setText(customer.getCustomerEmail());
            edtContact.setText(customer.getCustomerMobileNumber());
        } else {
            client = sessionManager.getClient();
            Log.i("ClientProfile",client.toString());
            edtFullName.setText(client.getName());
            edtEmail.setText(client.getEmail());
            edtContact.setText(client.getMobileNumber());
        }
    }

    private void updateUiLang() {
        String title = arabic?getString(R.string.edit_profile_arabic):getString(R.string.edit_profile);
        String hFName = arabic?getString(R.string.full_name_arabic):getString(R.string.full_name);
        String hEmail = arabic?getString(R.string.email_arabic):getString(R.string.email);
        String hcontact = arabic?getString(R.string.contact_arabic):getString(R.string.contact);

        edtProfileTitleV.setHint(title);
        edtFullName.setHint(hFName);
        edtEmail.setHint(hEmail);
        edtContact.setHint(hcontact);

        pUs =arabic?getString(R.string.profile_update_arabic):getString(R.string.profile_update);
    }

    private void configureSCarTypeSpinner() {
        carTypes = MetaData.getCarType();

//        spinnerCarType.setAdapter(new CarTypeAdapterRegister(carTypes, this));
//        spinnerCarType.setOnItemSelectedListener(this);
//        spinnerCarType.setSelection(sessionManager.getCarType());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ButterKnife.unbind(this);
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        carType = carTypes.get(i);
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    @OnClick(R.id.btn_update_profile)
    public void onBtnUpdateProfileClicked() {
        String fullName = edtFullName.getText().toString();
        String email = edtEmail.getText().toString();
        String contact = edtContact.getText().toString();

        if (TextUtils.isEmpty(fullName)
                || TextUtils.isEmpty(email)
                || TextUtils.isEmpty(contact)) {
            showMessage(MetaData.MSG_EMPTY_FIELD);
        } else {
            if (userType.equals(MetaData.USER_TYPE_CUSTOMER)) {
                if (carType.getType().equals(MetaData.SELECT_CAR_TYPE)) {
                    showMessage(MetaData.MSG_SELECT_CAR_TYPE);
                } else {
                    customer.setCustomerName(fullName);
                    customer.setCustomerEmail(email);
                    customer.setCustomerMobileNumber(contact);
                    sessionManager.setCustomerLogin(userType, customer);
                    Toast.makeText(getApplicationContext(), pUs, Toast.LENGTH_SHORT).show();
                    onBackPressed();
                }
            } else {
                sessionManager.setCustomerLogin(userType, null);
                Toast.makeText(getApplicationContext(), pUs, Toast.LENGTH_SHORT).show();
                onBackPressed();
            }

            onBackPressed();
        }
    }

    private void showMessage(String msg) {
        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
}
