package com.ronem.carwash.view.current_order;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.ronem.carwash.R;
import com.ronem.carwash.adapters.CarAndServiceAdapter;
import com.ronem.carwash.model.CarAndService;
import com.ronem.carwash.model.users.CustomerStationRequest;
import com.ronem.carwash.utils.MetaData;
import com.ronem.carwash.utils.SessionManager;
import com.ronem.carwash.view.OrderMapActivity;
import com.ronem.carwash.view.dashboard.DashboardStationSalesman;
import com.ronem.carwash.view.dialogs.OrderRequestDialog;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by ram on 11/12/17.
 */

public class CurrentOrderActivity extends AppCompatActivity {

    private String TAG = getClass().getSimpleName();

    @Bind(R.id.toolbar)
    Toolbar toolbar;

    @Bind(R.id.btn_layout_not_accepted)
    LinearLayout layoutNotAccepted;
    @Bind(R.id.btn_layout_accepted)
    LinearLayout layoutAccepted;
    @Bind(R.id.btn_view_detail_order)
    TextView btnViewLiveDetail;
    @Bind(R.id.btn_view_accept_order)
    TextView btnAcceptLiveOrder;
    @Bind(R.id.btn_view_reject_order)
    TextView btnIgnoreLiveOrder;
    @Bind(R.id.btn_call_customer)
    TextView btnCallCustomer;

    @Bind(R.id.live_layout)
    RelativeLayout livelayout;
    @Bind(R.id.customer_name_tv)
    TextView customerNameTv;
    @Bind(R.id.customer_address)
    TextView customerAddress;
    @Bind(R.id.customer_email)
    TextView customerEmail;
    @Bind(R.id.date_of_appointment_key)
    TextView dateOfAppointmentKey;
    @Bind(R.id.date_of_appointment_value)
    TextView dateOfAppointmentValue;

    @Bind(R.id.car_info_recycler_view)
    RecyclerView carInfoRecyclerView;

    private CustomerStationRequest liveRequest;
    private List<CarAndService> carAndServices;

    private boolean CURRENT_LANGUAGE;
    private SessionManager sessionManager;
    private String customer, address, contact, ordermadeby, close;
    private String orejected, oCompleted;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_appointment_layout);
        ButterKnife.bind(this);
        sessionManager = new SessionManager(this);
        CURRENT_LANGUAGE = sessionManager.getCurrentLanguage();

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        livelayout.setVisibility(View.GONE);

        listeningToLiveCustomers();

        updateRecyclerTitle();

        configureRecyclerView();
    }

    private void configureRecyclerView() {
        carInfoRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        carInfoRecyclerView.setHasFixedSize(true);

    }


    private void listeningToLiveCustomers() {
        if (DashboardStationSalesman.userType.equals(MetaData.USER_TYPE_CLIENT_SALESMAN)) {
            Log.i(TAG, "Salesman");
            DatabaseReference dbr = FirebaseDatabase
                    .getInstance()
                    .getReference()
                    .child(MetaData.CUSTOMER_SALESMAN_LIVE_ORDER_REQUEST);

            dbr.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    Log.i(TAG, "There was live data");
                    if (dataSnapshot.exists()) {
                        for (DataSnapshot ds : dataSnapshot.getChildren()) {
                            CustomerStationRequest csr = ds.getValue(CustomerStationRequest.class);
                            if ((DashboardStationSalesman.salesManStation.getUsername()).equals(csr.getsUserName())) {
                                Log.i(TAG, "username matched");


                                liveRequest = csr;

                                setUpCurrentOrder();
                                return;
                            }
                        }
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }
    }

    private void setUpCurrentOrder() {

        customerNameTv.setText(liveRequest.getcName());
        int addressId = CURRENT_LANGUAGE ? R.string.address_format_arabic : R.string.address_format_eng;
        String addressFormat = getString(addressId, liveRequest.getcAddress());
        customerAddress.setText(addressFormat);

        int contactId = CURRENT_LANGUAGE ? R.string.contact_format_arabic : R.string.contact_format_eng;
        String contactFormat = getString(contactId, liveRequest.getcContact());
        customerEmail.setText(contactFormat);

        dateOfAppointmentValue.setText(liveRequest.getDateTime());

        //holds all the cars and the services
        //along with the carType and its color
        carAndServices = new ArrayList<>();

        HashMap<String, HashMap<String, String>> cars = liveRequest.getCars();
        for (Map.Entry<String, HashMap<String, String>> entry : cars.entrySet()) {
            HashMap<String, String> car = entry.getValue();
            String carType = car.get(MetaData.KEY.CAR_TYPE);
            String serviceType = car.get(MetaData.KEY.SERVIE_TYPE);
            String carColor = car.get(MetaData.KEY.CAR_COLOR);
            String carPlate = car.get(MetaData.KEY.CAR_PLATE_NO);

            carAndServices.add(new CarAndService(carType, carColor, carPlate, serviceType));
        }

        //update the recycler view title and the empty
        updateRecyclerTitle();

        updateView();
        livelayout.setVisibility(View.VISIBLE);
    }

    private void updateView() {
        String vd = CURRENT_LANGUAGE ? getString(R.string.view_detail_arabic) : getString(R.string.view_detail);
        String ao = CURRENT_LANGUAGE ? getString(R.string.accept_order_arabic) : getString(R.string.accept_order);
        String io = CURRENT_LANGUAGE ? getString(R.string.ignore_order_arabic) : getString(R.string.ignore_order);
        String vr = CURRENT_LANGUAGE ? MetaData.BTN_VIEW_ROUTE_ARABIC : MetaData.BTN_VIEW_ROUTE;
        String co = CURRENT_LANGUAGE ? MetaData.BTN_COMPLETE_ORDER_ARABIC : MetaData.BTN_COMPLETE_ORDER;
        btnViewLiveDetail.setText(vd);

        //for the live layout
        switch (liveRequest.getAccepted()) {
            case MetaData.ORDER_IDLE:
                livelayout.setVisibility(View.VISIBLE);
                btnIgnoreLiveOrder.setVisibility(View.VISIBLE);

                btnAcceptLiveOrder.setText(ao);
                btnIgnoreLiveOrder.setText(io);

                layoutNotAccepted.setVisibility(View.VISIBLE);
                layoutAccepted.setVisibility(View.GONE);
                break;

            case MetaData.ORDER_ACCEPTED:
                livelayout.setVisibility(View.VISIBLE);
                btnAcceptLiveOrder.setText(vr);
                btnIgnoreLiveOrder.setText(co);

                layoutNotAccepted.setVisibility(View.GONE);
                layoutAccepted.setVisibility(View.VISIBLE);
                break;

            case MetaData.ORDER_REJECTED:
                livelayout.setVisibility(View.GONE);
                break;
        }

        carInfoRecyclerView.setAdapter(new CarAndServiceAdapter(this, carAndServices));
        carInfoRecyclerView.setNestedScrollingEnabled(false);
    }

    private void updateRecyclerTitle() {
        String rt = CURRENT_LANGUAGE ? getString(R.string.appointments_arabic) : getString(R.string.appointments);

        String wfa = CURRENT_LANGUAGE ? getString(R.string.waiting_for_appintments_arabic) : getString(R.string.waiting_for_appintments);

        customer = CURRENT_LANGUAGE ? getString(R.string.customer_arabic) : getString(R.string.customer);
        address = CURRENT_LANGUAGE ? getString(R.string.d_address_arabic) : getString(R.string.d_address);
        contact = CURRENT_LANGUAGE ? getString(R.string.d_contact_arabic) : getString(R.string.d_contact);
        ordermadeby = CURRENT_LANGUAGE ? getString(R.string.d_order_made_by_arabic) : getString(R.string.d_order_made_by);
        close = CURRENT_LANGUAGE ? getString(R.string.close_arabic) : getString(R.string.close);
        orejected = CURRENT_LANGUAGE ? getString(R.string.order_rejected_arabic) : getString(R.string.order_rejected);
        oCompleted = CURRENT_LANGUAGE ? getString(R.string.order_complete_arabic) : getString(R.string.order_complete);
    }


    private void addToRejectedList(CustomerStationRequest csr) {
        DatabaseReference dbr;
        if (DashboardStationSalesman.userType.equals(MetaData.USER_TYPE_CLIENT_STATION)) {
            dbr = FirebaseDatabase.getInstance().getReference().child(MetaData.STATION_REJECTED_ORDER);
        } else {
            dbr = FirebaseDatabase.getInstance().getReference().child(MetaData.SALESMAN_REJECTED_ORDER);
        }

        dbr.push().setValue(csr);
        livelayout.setVisibility(View.GONE);
    }

    @OnClick(R.id.btn_view_detail_order)
    public void onBtnViewDetailOrderClicked() {
        launchOrderMapActivity();
    }

    @OnClick(R.id.btn_view_accept_order)
    public void onBtnAcceptOrderClicked() {
        String childNode = liveRequest.getcUserName() + liveRequest.getsUserName();
        DatabaseReference dbr = FirebaseDatabase
                .getInstance()
                .getReference()
                .child(MetaData.CUSTOMER_SALESMAN_LIVE_ORDER_REQUEST)
                .child(childNode);
        liveRequest.setAccepted(MetaData.ORDER_ACCEPTED);
        dbr.setValue(liveRequest)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        launchOrderMapActivity();
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(CurrentOrderActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @OnClick(R.id.btn_view_reject_order)
    public void onBtnIgnoreOrderClicked() {
        if (liveRequest.getAccepted() == MetaData.ORDER_IDLE) {
            String childNode = liveRequest.getcUserName() + liveRequest.getsUserName();
            DatabaseReference dbr = FirebaseDatabase
                    .getInstance()
                    .getReference()
                    .child(MetaData.CUSTOMER_SALESMAN_LIVE_ORDER_REQUEST)
                    .child(childNode);
            liveRequest.setAccepted(MetaData.ORDER_REJECTED);
            dbr.setValue(liveRequest)
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            Toast.makeText(CurrentOrderActivity.this, orejected, Toast.LENGTH_SHORT).show();
                            addToRejectedList(liveRequest);
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Toast.makeText(CurrentOrderActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                }
            });
        } else if (liveRequest.getAccepted() == MetaData.ORDER_ACCEPTED) {
            //means the btn behaves as complete order
            String childNode = liveRequest.getcUserName() + liveRequest.getsUserName();
            DatabaseReference dbr = FirebaseDatabase
                    .getInstance()
                    .getReference()
                    .child(MetaData.CUSTOMER_SALESMAN_LIVE_ORDER_REQUEST)
                    .child(childNode);
            liveRequest.setAccepted(MetaData.ORDER_COMPLETED);
            dbr.setValue(liveRequest)
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            Toast.makeText(CurrentOrderActivity.this, oCompleted, Toast.LENGTH_SHORT).show();
                            livelayout.setVisibility(View.GONE);

                            //push the object to the completed node
                            DatabaseReference finishDb = FirebaseDatabase.getInstance().getReference().child(MetaData.SALESMAN_FINISHED_TASK);
                            finishDb.push().setValue(liveRequest);

                        }
                    }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Toast.makeText(CurrentOrderActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    @OnClick(R.id.btn_call_customer)
    public void onBtnCallCustomerClicked() {
        String phone = liveRequest.getcContact();
        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null));
        startActivity(intent);
    }

    private void launchOrderMapActivity() {
        Intent i = new Intent(this, OrderMapActivity.class);
        i.putExtra(MetaData.KEY_LATITUDE, liveRequest.getcLatitude());
        i.putExtra(MetaData.KEY_LONGITUDE, liveRequest.getcLongitude());
        i.putExtra(MetaData.KEY_CONTACT_LATITUDE, liveRequest.getContactLatitude());
        i.putExtra(MetaData.KEY_CONTACT_LONGITUDE, liveRequest.getContactLongitude());
        i.putExtra(MetaData.USER_TYPE_CUSTOMER, liveRequest.getcName());
        i.putExtra(MetaData.KEY_SHOULD_SHOW_INFO, true);

        startActivity(i);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) this.finish();
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ButterKnife.unbind(this);
    }
}

