package com.ronem.carwash.view.dashboard.station_delivered;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.ronem.carwash.R;
import com.ronem.carwash.interfaces.CarServiceDateListener;
import com.ronem.carwash.model.users.Customer;
import com.ronem.carwash.model.users.CustomerStationRequest;
import com.ronem.carwash.model.users.SalesManStation;
import com.ronem.carwash.utils.BasicUtilityMethods;
import com.ronem.carwash.utils.DistanceCalculator;
import com.ronem.carwash.utils.EventBus;
import com.ronem.carwash.utils.Events;
import com.ronem.carwash.utils.MetaData;
import com.ronem.carwash.utils.SessionManager;
import com.ronem.carwash.view.CarwashSplash;
import com.ronem.carwash.view.dialogs.SuccessDialog;
import com.ronem.carwash.view.dashboard.DirectionAdView;
import com.ronem.carwash.view.dashboard.DirectionPresenter;
import com.ronem.carwash.view.dashboard.DirectionPresenterImpl;
import com.ronem.carwash.view.dialogs.ShowInfoDialogFragment;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by ronem on 8/20/17.
 */

public class FragmentStation
        extends Fragment
        implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        com.google.android.gms.location.LocationListener,
        DirectionAdView {

    private String TAG = getClass().getSimpleName();

    private GoogleApiClient googleApiClient;
    private LocationRequest locationRequest;
    private GoogleMap googleMap;
    private List<SalesManStation> salesManStations;
    private DirectionPresenter presenter;
    private Marker mPositionMarker;
    private Circle circle;
    private Location myLocation;
    private LatLng myLatlang, destinationLatlang;
    private SalesManStation salesManStation;
    private PolylineOptions polylineOptions;
    private Polyline polyline;
    private SessionManager sessionManager;
    private boolean isCameraAnimated = false;

    @Bind(R.id.dialog_info)
    LinearLayout dialogInfo;
    @Bind(R.id.dialog_station_name_title)
    TextView dialogStationNameTitleView;
    @Bind(R.id.dialog_contact)
    TextView dialogContactTv;
    @Bind(R.id.dialog_share)
    TextView dialogShareTv;
    @Bind(R.id.dialog_time_away)
    TextView dialogTimeAwayTextView;
    @Bind(R.id.dialog_distance)
    TextView dialogDistanceTextView;
    @Bind(R.id.dialog_station_name)
    TextView dialogStationNameTextView;
    @Bind(R.id.dialog_carwasher_name_title)
    TextView dialogCarwasherNameTextview;
    @Bind(R.id.dialog_vehicle_type)
    TextView dialogVehicleTypeTextView;
    @Bind(R.id.progress)
    ProgressBar progressBar;

    @Bind(R.id.toggle_map)
    TextView toggleMap;
    private boolean isSatelliteView = false;

    private Customer customer;
    private List<Marker> markers;
    private List<Circle> stationsCircleList;
    private CustomerStationRequest csr;
    private GoogleMap.OnMarkerClickListener markerClickListener;
    private String distance = "0km";
    private String duration = "0m";
    private int noOfCars;
    private HashMap<String, HashMap<String, String>> cars;
    private String dateTime;
    private boolean wasAccepted = false;
    private ProgressDialog progressDialog;
    private SuccessDialog successDialog;
    private String key;
    private boolean CURRENT_LANGUAGE;
    private String APP_NAME, MSG_DEST_10M, MSG_CNT_FIND_ROUTE, MSG_WATING_RESPONSE,
            MSG_INTERNET_CONNECTION, MSG_CANNOT_MAKE_NXT_ORDER, MSG_LOCATION_NOT_FOUND,
            MSG_TASK_STATUS,
            MSG_ORDER_COMPLETED,
            MSG_CLOSE, MSG_ORDER_SENT, MSG_ORDER_REJECTED, MSG_APPOINTMENT_CONFIRMED;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_station, container, false);
        ButterKnife.bind(this, v);
        EventBus.register(this);
        key = getString(R.string.my_google_api_key);

        progressDialog = new ProgressDialog(getContext());
        progressDialog.setCancelable(true);

        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        sessionManager = new SessionManager(getContext());
        CURRENT_LANGUAGE = sessionManager.getCurrentLanguage();
        customer = sessionManager.getCustomer();

        presenter = new DirectionPresenterImpl();
        presenter.onAddDirectionView(this);

        createGoogleApiClient();
        googleApiClient.connect();

        locationRequest = BasicUtilityMethods.createLocationRequest();

        progressBar.setVisibility(View.VISIBLE);
        loadmap();
        updateUILang();
    }

    private void createGoogleApiClient() {
        googleApiClient = new GoogleApiClient.Builder(getContext())
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
    }

    private void loadmap() {
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.station_delivered_map);
        mapFragment.getMapAsync(this);
    }

    private void animateMarker(final Marker marker, final Location location) {
        final Handler handler = new Handler();
        final long start = SystemClock.uptimeMillis();
        final LatLng startLatLng = marker.getPosition();
        final double startRotation = marker.getRotation();
        final long duration = 500;

        final Interpolator interpolator = new LinearInterpolator();

        handler.post(new Runnable() {
            @Override
            public void run() {
                long elapsed = SystemClock.uptimeMillis() - start;
                float t = interpolator.getInterpolation((float) elapsed
                        / duration);

                double lng = t * location.getLongitude() + (1 - t)
                        * startLatLng.longitude;
                double lat = t * location.getLatitude() + (1 - t)
                        * startLatLng.latitude;

                float rotation = (float) (t * location.getBearing() + (1 - t)
                        * startRotation);

                marker.setPosition(new LatLng(lat, lng));
                marker.setRotation(rotation);

                if (t < 1.0) {
                    // Post again 16ms later.
                    handler.postDelayed(this, 16);
                }
            }
        });
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        requestLocationupdate();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location l) {
        if (l == null)
            return;

        Log.i(TAG, "Location received in station");
        myLocation = l;

        myLatlang = new LatLng(myLocation.getLatitude(), myLocation.getLongitude());
//        myLatlang = new LatLng(24.8021183013916, 46.783164978027344);

        if (mPositionMarker != null) {
            mPositionMarker.remove();
        }
        //creating marker for the location
        MarkerOptions markerOption = new MarkerOptions()
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED))
                .position(myLatlang);

        //creating circle for the markers

        if (circle != null) {
            circle.remove();
        }
        CircleOptions circleOptions = new CircleOptions()
                .center(myLatlang)
                .radius(MetaData.RADIUS_5)
                .fillColor(MetaData.SHADE_COLOR_TRANS)
                .strokeColor(MetaData.STROKE_COLOR_TRANS)
                .strokeWidth(2);

        circle = googleMap.addCircle(circleOptions);

        mPositionMarker = googleMap.addMarker(markerOption);


        if (!isCameraAnimated) {
            progressBar.setVisibility(View.GONE);
            CameraPosition newCamPos = new CameraPosition.Builder().target(myLatlang)
                    .zoom(BasicUtilityMethods.getZoomLevel(circle))
                    .bearing(300)
                    .tilt(0)
                    .build();
            googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(newCamPos), 4000, null);

            isCameraAnimated = true;
        }

        if (destinationLatlang != null) {
            double distance = DistanceCalculator.getDistance(myLatlang.latitude, myLatlang.longitude, destinationLatlang.latitude, destinationLatlang.longitude, "K");
            String d = String.format(Locale.CANADA, "%.2f", distance);

            dialogDistanceTextView.setText(d + "km");
            if (distance < 0.01) {
                showToast(MSG_DEST_10M);
                wasAccepted = false;
//                if (dialogInfo.getVisibility() == View.VISIBLE) {
//                    dialogInfo.setVisibility(View.GONE);
//                }
            }
        }

        //moving the marker to the current location
//        animateMarker(mPositionMarker, myLocation); // Helper method for smooth

    }

    @Override
    public void onMapReady(final GoogleMap googleMap) {
        this.googleMap = googleMap;
//        googleMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
        googleMap.setMyLocationEnabled(true);

        Log.i(TAG, "Map Ready in station");
        if (ActivityCompat.checkSelfPermission(getContext(),
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(getContext(),
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }


        markerClickListener = new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {

//                if (isStationAvailable()) {
                String tag = (String) marker.getTag();

                //enable user to click only if user was not accepted
                if (!TextUtils.isEmpty(tag) && !tag.equals(customer.getUserName())) {
                    //check if request was already made
                    if (myLocation == null) {
                        Toast.makeText(getContext(), MSG_LOCATION_NOT_FOUND, Toast.LENGTH_SHORT).show();
                    } else {
                        if (!wasAccepted) {
                            destinationLatlang = new LatLng(marker.getPosition().latitude, marker.getPosition().longitude);
                            String url = BasicUtilityMethods.getUrl(myLatlang, destinationLatlang, key);
                            Log.i(TAG, "URL::" + url);
                            if (BasicUtilityMethods.isNetworkOnline(getContext())) {
                                for (SalesManStation sms : salesManStations) {
                                    if (sms.getUsername().equals(tag)) {
                                        salesManStation = sms;
                                    }
                                }
                                progressDialog.show();
                                presenter.onGetPolyLineOptions(url);
                            } else {
                                Toast.makeText(getContext(), MSG_INTERNET_CONNECTION, Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(getContext(), MSG_CANNOT_MAKE_NXT_ORDER, Toast.LENGTH_SHORT).show();
                        }
                    }
                }
//                }
                return true;
            }
        };

        googleMap.setOnMarkerClickListener(markerClickListener);

        populateStations();

    }

    private void populateStations() {

        DatabaseReference dbr = FirebaseDatabase.getInstance().getReference().child(MetaData.AVAILABLE_STATIONS);
        dbr.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Log.i(TAG, "dataChanged received");
                //first clear all the stations if present
                if (markers != null && markers.size() > 0) {
                    for (Marker m : markers) {
                        m.remove();
                    }
                    markers.clear();
                } else {
                    markers = new ArrayList<>();
                }

                //first clear all the circles if present
                if (stationsCircleList != null && stationsCircleList.size() > 0) {
                    for (Circle c : stationsCircleList) {
                        c.remove();
                    }
                    stationsCircleList.clear();
                } else {
                    stationsCircleList = new ArrayList<>();
                }

                if (dataSnapshot.exists()) {
                    salesManStations = new ArrayList<>();

                    for (DataSnapshot ds : dataSnapshot.getChildren()) {
                        SalesManStation sms = ds.getValue(SalesManStation.class);
                        salesManStations.add(sms);

                        LatLng l = new LatLng(sms.getLatitude(), sms.getLongitude());

                        MarkerOptions markerOption = new MarkerOptions()
                                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN))
                                .position(l);
                        Marker m = googleMap.addMarker(markerOption);
                        markers.add(m);
                        m.setTag(sms.getUsername());

                        /**
                         * Adding circle around the stations
                         * with the radius of 2.5 km
                         */
                        CircleOptions circleOptions = new CircleOptions()
                                .center(l)
                                .radius(MetaData.RADIUS_5)
                                .fillColor(MetaData.SHADE_COLOR_IMG)
                                .strokeColor(MetaData.STROKE_COLOR_IMG)
                                .strokeWidth(2);
                        Circle c = googleMap.addCircle(circleOptions);
                        stationsCircleList.add(c);
                        Log.i(TAG, "MarkerAdded");
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @SuppressWarnings("MissingPermission")
    private void requestLocationupdate() {
        LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);
    }

    @Override
    public void onPolyLineOptionReceived(PolylineOptions polylineOptions, String distance, String duration) {
        this.polylineOptions = polylineOptions;
        this.distance = distance;
        this.duration = duration;

        progressDialog.dismiss();

        ShowInfoDialogFragment dialogFragment = new ShowInfoDialogFragment();
        Bundle box = new Bundle();
        box.putString("station", salesManStation.getName());
        box.putString("station_email", salesManStation.getEmail());
        box.putString("station_contact", salesManStation.getMobileNumber());
        box.putString("station_address", salesManStation.getAddress());
        box.putString("distance", distance);
        box.putString("time", duration);

        dialogFragment.setArguments(box);
        dialogFragment.setOnCarServiceDateListener(new CarServiceDateListener() {
            @Override
            public void onCarTypeServiceDateReceived(int nCars, HashMap<String, HashMap<String, String>> _cars, String d) {
                noOfCars = nCars;
                cars = _cars;
                dateTime = d;
                makeOrderRequest();
            }
        });
        dialogFragment.show(getFragmentManager(), "");


    }

    @Override
    public void onTimeDistanceReceived(String distance, String time,int value) {

    }

    private void makeOrderRequest() {
        DatabaseReference dbr = FirebaseDatabase.getInstance().getReference().child(MetaData.CUSTOMER_STATION_REQUEST);
        CustomerStationRequest csr = new CustomerStationRequest(
                customer.getUserName(),
                customer.getCustomerName(),
                "",
                customer.getCustomerMobileNumber(),
                customer.getLatitude(),
                customer.getLongitude(),
                0, 0,
                dateTime,
                noOfCars,
                cars,
                salesManStation.getUsername(),
                salesManStation.getName(),
                salesManStation.getAddress(),
                salesManStation.getLatitude(),
                salesManStation.getLongitude(),
                salesManStation.getMobileNumber(),
                salesManStation.getEmail(),
                0
        );
        final String childNode = customer.getUserName() + salesManStation.getUsername();
        dbr.child(childNode).setValue(csr).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                successDialog = new SuccessDialog(getContext(), MSG_ORDER_SENT, true);
                successDialog.show();

                waitingForResponse(childNode);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                showToast("Something went wrong");
            }
        });

    }


    private void waitingForResponse(final String childNode) {
        final DatabaseReference dbr = FirebaseDatabase
                .getInstance()
                .getReference()
                .child(MetaData.CUSTOMER_STATION_REQUEST)
                .child(childNode);


        dbr.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Log.i(TAG, "waiting response");
                if (dataSnapshot.exists()) {
                    csr = dataSnapshot.getValue(CustomerStationRequest.class);

                    if (customer.getUserName().equals(csr.getcUserName())) {

                        Log.i("Accepted : ", String.valueOf(csr.getAccepted()));
                        if (csr.getAccepted() == MetaData.ORDER_ACCEPTED) {

                            if (!wasAccepted) {
                                orderAccepted();
                            }

                        } else if (csr.getAccepted() == MetaData.ORDER_REJECTED) {

                            orderRejected(dbr);

                        } else if (csr.getAccepted() == MetaData.ORDER_COMPLETED) {
                            orderCompleted(dbr);
                        }
                    }
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void orderCompleted(DatabaseReference dbr) {
        dialogInfo.setVisibility(View.GONE);
        if (getActivity() != null) {

            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle(MSG_TASK_STATUS);
            builder.setMessage(MSG_ORDER_COMPLETED);
            builder.setPositiveButton(MSG_CLOSE, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                    Intent homeIntent = new Intent(getContext(), CarwashSplash.class);
                    homeIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(homeIntent);
                }
            });

            AlertDialog dialog = builder.create();
            dialog.show();
        }
        dbr.removeValue();
    }

    private void orderRejected(DatabaseReference dbr) {
        if (getActivity() != null) {
            if (successDialog.isShowing()) {
                successDialog.dismiss();
            }
            successDialog = new SuccessDialog(getContext(), MSG_ORDER_REJECTED, false);
            successDialog.show();
        }
        //if the request is rejected we will remove the orders from child node
        dbr.removeValue();

    }

    private void orderAccepted() {
        if (successDialog.isShowing()) {
            successDialog.dismiss();
        }

        //update flag
        wasAccepted = true;

        //update the language
        //updating the text for the station name
//        updateDialogInfo();
        showConfirmedDialog();

        /**
         * Draw the route path between customer and the client
         * if the request is accepted by the client
         * Also remove all the markers accept the selected client
         */
        if (polylineOptions != null) {
            if (polyline != null) {
                polyline.remove();
            }
            polyline = googleMap.addPolyline(polylineOptions);
        }
    }

    private void showConfirmedDialog() {
        if (getActivity() != null) {
            String close = CURRENT_LANGUAGE ? getString(R.string.close_arabic) : getString(R.string.close);
            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            builder.setTitle(APP_NAME);
            builder.setMessage(MSG_APPOINTMENT_CONFIRMED);
            builder.setPositiveButton(close, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });
            builder.create().show();
        }
    }

    private void updateDialogInfo() {
        String smTitle = CURRENT_LANGUAGE ?
                getString(R.string.station_name_arabic) :
                getString(R.string.station_name);
        dialogStationNameTitleView.setText(smTitle);

        //updating the vehicle type
        String vt = CURRENT_LANGUAGE ?
                getString(R.string.vehicle_type_arabic) :
                getString(R.string.vehicle_type);
        dialogVehicleTypeTextView.setText(vt);

        //updating the contact captain button text
        String cc = CURRENT_LANGUAGE ?
                getString(R.string.contact_captain_arabic) :
                getString(R.string.contact_captain);
        dialogContactTv.setText(cc);

        //updating the share contact button text
        String s = CURRENT_LANGUAGE ?
                getString(R.string.share_tracker_arabic) :
                getString(R.string.share_tracker);
        dialogShareTv.setText(s);

        //set information of the station
        dialogInfo.setVisibility(View.VISIBLE);
        dialogDistanceTextView.setText(distance);
        if (getActivity() != null) {
            String msg = CURRENT_LANGUAGE ?
                    getString(R.string.time_away_arabic, duration)
                    : getString(R.string.time_away, duration);

            dialogTimeAwayTextView.setText(msg);
            dialogCarwasherNameTextview.setText(csr.getsName());
            dialogStationNameTextView.setText(csr.getsAddress());
        }
    }


    private void showToast(String s) {
        if (getActivity() != null) {
            Toast.makeText(getContext(), s, Toast.LENGTH_SHORT).show();
        }
    }

    private void removeAllMarkersAndSetOnlyCustomerStation(String mTag) {
        Log.i("userTag : ", mTag);
        for (Marker m : markers) {
            String tag = (String) m.getTag();
            Log.i("tagwas ", tag);
            if (tag.equals(mTag)) {
                Log.i("Tag : ", tag);
                m.setTag(tag + "selected");
            } else if (!TextUtils.isEmpty(tag) && !tag.equals(customer.getUserName())) {
                m.remove();
            }
        }

//        LatLng l = new LatLng(v, v1);
//        MarkerOptions markerOption = new MarkerOptions()
//                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN))
//                .position(l);
//        googleMap.addMarker(markerOption);
    }

    private void resetTheTagFortheMarker(String userName) {
        for (Marker m : markers) {
            String tag = (String) m.getTag();
            if (tag.startsWith(userName)) {
                m.setTag(userName);
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        googleApiClient.connect();
    }

    @Override
    public void onStop() {
        super.onStop();
        googleApiClient.disconnect();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (googleApiClient.isConnected()) {
            requestLocationupdate();
        }

    }

    @Override
    public void onPause() {
        super.onPause();
        if (googleApiClient.isConnected())
            LocationServices.FusedLocationApi.removeLocationUpdates(googleApiClient, this);
    }

    @OnClick(R.id.toggle_map)
    public void onToggleMapClicked() {
        if (isSatelliteView) {
            isSatelliteView = false;
            googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
            toggleMap.setText(R.string.satellite);
        } else {
            isSatelliteView = true;
            googleMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
            toggleMap.setText(R.string.defaultmap);
        }
    }

    @Subscribe
    public void onLanguageChangeEventReceived(Events.LanguageChangeEvent event) {
        CURRENT_LANGUAGE = event.isLang();
        if (getActivity() != null)
            updateUILang();
        if (dialogInfo.getVisibility() == View.VISIBLE) {
            updateDialogInfo();
        }
    }

    private void updateUILang() {
        APP_NAME = CURRENT_LANGUAGE ? getString(R.string.app_name_arabic) : getString(R.string.app_name);
        MSG_DEST_10M = CURRENT_LANGUAGE ? getString(R.string.destination_10m_ahead_arabic) : getString(R.string.destination_10m_ahead);
        MSG_CNT_FIND_ROUTE = CURRENT_LANGUAGE ? getString(R.string.cannot_find_route_arabic) : getString(R.string.cannot_find_route);
        MSG_WATING_RESPONSE = CURRENT_LANGUAGE ? getString(R.string.waiting_for_response_arabic) : getString(R.string.waiting_for_response);
        MSG_INTERNET_CONNECTION = CURRENT_LANGUAGE ? getString(R.string.enable_internet_arabic) : getString(R.string.enable_internet);
        MSG_CANNOT_MAKE_NXT_ORDER = CURRENT_LANGUAGE ? getString(R.string.cannot_make_order_unless_finished_arabic) : getString(R.string.cannot_make_order_unless_finished);
        MSG_LOCATION_NOT_FOUND = CURRENT_LANGUAGE ? getString(R.string.location_not_found_arabic) : getString(R.string.location_not_found);
        MSG_TASK_STATUS = CURRENT_LANGUAGE ? getString(R.string.task_status_arabic) : getString(R.string.task_status);
        MSG_CLOSE = CURRENT_LANGUAGE ? getString(R.string.close_arabic) : getString(R.string.close);
        MSG_ORDER_COMPLETED = CURRENT_LANGUAGE ? getString(R.string.order_complete_arabic) : getString(R.string.order_complete);
        MSG_ORDER_SENT = CURRENT_LANGUAGE ? getString(R.string.order_sent_arabic) : getString(R.string.order_sent);
        MSG_ORDER_REJECTED = CURRENT_LANGUAGE ? getString(R.string.order_rejected_arabic) : getString(R.string.order_rejected);
        MSG_APPOINTMENT_CONFIRMED = CURRENT_LANGUAGE ? getString(R.string.appointment_confirmed_arabic) : getString(R.string.appointment_confirmed);

        String pleasewait = CURRENT_LANGUAGE ? getString(R.string.please_wait_arabic) : getString(R.string.please_wait);
        progressDialog.setMessage(pleasewait);
    }
}
