package com.ronem.carwash.view.appointment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.ronem.carwash.R;
import com.ronem.carwash.adapters.CustomerStationRequestAdapter;
import com.ronem.carwash.interfaces.OrderChangeStatusListener;
import com.ronem.carwash.model.users.CustomerStationRequest;
import com.ronem.carwash.utils.EventBus;
import com.ronem.carwash.utils.Events;
import com.ronem.carwash.utils.MetaData;
import com.ronem.carwash.utils.SessionManager;
import com.ronem.carwash.view.dashboard.DashboardStationSalesman;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by ram on 11/12/17.
 */

public class AppointmentActivity extends AppCompatActivity implements OrderChangeStatusListener {

    private String TAG = getClass().getSimpleName();

    @Bind(R.id.toolbar)
    Toolbar toolbar;


    @Bind(R.id.recycler_title)
    TextView recyclerTitleTv;
    @Bind(R.id.empty_recycler)
    TextView emptyListView;
    @Bind(R.id.order_recycler_view)
    RecyclerView recyclerView;

    private CustomerStationRequest customerStationRequest;
    private ArrayList<String> userStack;
    private List<CustomerStationRequest> appointments;
    private CustomerStationRequestAdapter adapter;

    private boolean CURRENT_LANGUAGE;
    private SessionManager sessionManager;
    private String customer, address, contact, ordermadeby, close;
    private String orejected, oCompleted;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.appointment_order_layout);
        ButterKnife.bind(this);
        EventBus.register(this);
        sessionManager = new SessionManager(this);
        CURRENT_LANGUAGE = sessionManager.getCurrentLanguage();

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.hasFixedSize();

        appointments = new ArrayList<>();
        adapter = new CustomerStationRequestAdapter(appointments, MetaData.ORDER_STATUS_LIVE);
        adapter.setLang(CURRENT_LANGUAGE);
        recyclerView.setAdapter(adapter);
        adapter.setOnOrderChangeStatusListener(this);

        listeningToCustomers();

        updateRecyclerTitle();
    }


    private void updateRecyclerTitle() {
        String rt = CURRENT_LANGUAGE ? getString(R.string.appointments_arabic) : getString(R.string.appointments);
        recyclerTitleTv.setText(rt);

        String wfa = CURRENT_LANGUAGE ? getString(R.string.waiting_for_appintments_arabic) : getString(R.string.waiting_for_appintments);
        emptyListView.setText(wfa);

        customer = CURRENT_LANGUAGE ? getString(R.string.customer_arabic) : getString(R.string.customer);
        address = CURRENT_LANGUAGE ? getString(R.string.d_address_arabic) : getString(R.string.d_address);
        contact = CURRENT_LANGUAGE ? getString(R.string.d_contact_arabic) : getString(R.string.d_contact);
        ordermadeby = CURRENT_LANGUAGE ? getString(R.string.d_order_made_by_arabic) : getString(R.string.d_order_made_by);
        close = CURRENT_LANGUAGE ? getString(R.string.close_arabic) : getString(R.string.close);
        orejected = CURRENT_LANGUAGE ? getString(R.string.order_rejected_arabic) : getString(R.string.order_rejected);
        oCompleted = CURRENT_LANGUAGE ? getString(R.string.order_complete_arabic) : getString(R.string.order_complete);
    }

    private void listeningToCustomers() {
        //storage to list the accepted users
        userStack = new ArrayList<>();

        DatabaseReference dbr;

        if (DashboardStationSalesman.userType.equals(MetaData.USER_TYPE_CLIENT_STATION)) {
            dbr = FirebaseDatabase
                    .getInstance()
                    .getReference()
                    .child(MetaData.CUSTOMER_STATION_REQUEST);
        } else {
            dbr = FirebaseDatabase
                    .getInstance()
                    .getReference()
                    .child(MetaData.CUSTOMER_SALESMAN_REQUEST);
        }

        dbr.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Log.i("Client", "request received");
//                if (!accepted) {
                if (dataSnapshot.exists()) {
                    for (DataSnapshot ds : dataSnapshot.getChildren()) {
                        customerStationRequest = ds.getValue(CustomerStationRequest.class);
                        Log.i("csr", customerStationRequest.toString());
                        if (customerStationRequest.getsUserName().equals(DashboardStationSalesman.salesManStation.getUsername())) {
                            if (customerStationRequest.getAccepted() == MetaData.ORDER_IDLE) {
                                if (!presentInStack(customerStationRequest.getcUserName())) {
                                    appointments.add(customerStationRequest);
                                    adapter.notifyItemInserted(appointments.size() - 1);
                                }
                            }
                        }
                    }

                }
//                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    private boolean presentInStack(String userName) {
        for (String s : userStack) {
            if (s.equals(userName)) {
                return true;
            }
        }
        userStack.add(userName);
        return false;
    }

    @Override
    public void onOrderAccepted(final CustomerStationRequest csr) {
        final String childNode = csr.getcUserName() + csr.getsUserName();
        DatabaseReference dbr;
        if (DashboardStationSalesman.userType.equals(MetaData.USER_TYPE_CLIENT_STATION)) {
            dbr = FirebaseDatabase.getInstance().getReference().child(MetaData.CUSTOMER_STATION_REQUEST);
        } else {
            dbr = FirebaseDatabase.getInstance().getReference().child(MetaData.CUSTOMER_SALESMAN_REQUEST);
        }

        csr.setAccepted(MetaData.ORDER_ACCEPTED);
        dbr.child(childNode).setValue(csr).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                appointments.remove(csr);
                adapter.notifyDataSetChanged();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(AppointmentActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onOrderRejected(final CustomerStationRequest csr) {
        final String childNode = csr.getcUserName() + csr.getsUserName();
        DatabaseReference dbr;
        if (DashboardStationSalesman.userType.equals(MetaData.USER_TYPE_CLIENT_STATION)) {
            dbr = FirebaseDatabase.getInstance().getReference().child(MetaData.CUSTOMER_STATION_REQUEST);
        } else {
            dbr = FirebaseDatabase.getInstance().getReference().child(MetaData.CUSTOMER_SALESMAN_REQUEST);
        }

        /**
         * accepted value
         * request = 0
         * accepted = 1
         * rejected = -1
         */
        csr.setAccepted(MetaData.ORDER_REJECTED);
        dbr.child(childNode).setValue(csr).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                appointments.remove(csr);
                adapter.notifyDataSetChanged();
                userStack.remove(csr.getcUserName());

                addToRejectedList(csr);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(AppointmentActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
            }
        });

        /**
         * remove the user from the stack so that new listen will be activated
         */

    }

    private void addToRejectedList(CustomerStationRequest csr) {
        DatabaseReference dbr;
        if (DashboardStationSalesman.userType.equals(MetaData.USER_TYPE_CLIENT_STATION)) {
            dbr = FirebaseDatabase.getInstance().getReference().child(MetaData.STATION_REJECTED_ORDER);
        } else {
            dbr = FirebaseDatabase.getInstance().getReference().child(MetaData.SALESMAN_REJECTED_ORDER);
        }

        dbr.push().setValue(csr);
    }

    @Override
    public void onOrderComplete(CustomerStationRequest csr) {

    }


    @Subscribe
    public void onLanguageChangeEventReceived(Events.LanguageChangeEvent event) {
        CURRENT_LANGUAGE = event.isLang();
        Log.i(TAG, "Reached");
        updateRecyclerTitle();


        if (appointments != null && appointments.size() > 0) {
            adapter.setLang(CURRENT_LANGUAGE);
            adapter.notifyDataSetChanged();

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) this.finish();
        return super.onOptionsItemSelected(item);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.unregister(this);
    }
}
