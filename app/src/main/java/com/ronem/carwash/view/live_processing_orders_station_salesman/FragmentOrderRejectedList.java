//package com.ronem.carwash.view.live_processing_orders_station_salesman;
//
//import android.os.Bundle;
//import android.support.annotation.Nullable;
//import android.support.v4.app.Fragment;
//import android.support.v7.widget.LinearLayoutManager;
//import android.support.v7.widget.RecyclerView;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.TextView;
//
//import com.google.firebase.database.DataSnapshot;
//import com.google.firebase.database.DatabaseError;
//import com.google.firebase.database.DatabaseReference;
//import com.google.firebase.database.FirebaseDatabase;
//import com.google.firebase.database.ValueEventListener;
//import com.ronem.carwash.R;
//import com.ronem.carwash.adapters.CustomerStationRequestAdapter;
//import com.ronem.carwash.model.users.CustomerStationRequest;
//import com.ronem.carwash.utils.EventBus;
//import com.ronem.carwash.utils.Events;
//import com.ronem.carwash.utils.MetaData;
//import com.ronem.carwash.utils.SessionManager;
//import com.ronem.carwash.view.dashboard.DashboardStationSalesman;
//import com.squareup.otto.Subscribe;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import butterknife.Bind;
//import butterknife.ButterKnife;
//
///**
// * Created by ram on 10/24/17.
// */
//
//public class FragmentOrderRejectedList extends Fragment {
//
//    @Bind(R.id.order
//            _recycler_view)
//    RecyclerView recyclerView;
//    @Bind(R.id.recycler_title)
//
//    private List<CustomerStationRequest> rejectedOrderLists;
//    private CustomerStationRequestAdapter adapter;
//    private boolean CURRENT_LANGUAGE;
//    private SessionManager sessionManager;
//
//
//    @Nullable
//    @Override
//    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        View root = inflater.inflate(R.layout.fragment_live_order, container, false);
//        ButterKnife.bind(this, root);
//        EventBus.register(this);
//        sessionManager = new SessionManager(getContext());
//        CURRENT_LANGUAGE = sessionManager.getCurrentLanguage();
//
//
//        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
//        recyclerView.hasFixedSize();
//        rejectedOrderLists = new ArrayList<>();
//        adapter = new CustomerStationRequestAdapter(rejectedOrderLists, MetaData.ORDER_STATUS_FINISHED);
//
//        recyclerView.setAdapter(adapter);
//        return root;
//    }
//
//    @Override
//    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
//        super.onViewCreated(view, savedInstanceState);
//
//        updateRecyclerTitle();
//
//        DatabaseReference dbr;
//        if (DashboardStationSalesman.userType.equals(MetaData.USER_TYPE_CLIENT_STATION)) {
//            dbr = FirebaseDatabase.getInstance().getReference().child(MetaData.STATION_REJECTED_ORDER);
//        } else {
//            dbr = FirebaseDatabase.getInstance().getReference().child(MetaData.SALESMAN_REJECTED_ORDER);
//        }
//        dbr.addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(DataSnapshot dataSnapshot) {
//                if (dataSnapshot.exists()) {
//                    for (DataSnapshot ds : dataSnapshot.getChildren()) {
//                        CustomerStationRequest csr = ds.getValue(CustomerStationRequest.class);
//                        if ((csr.getsUserName())
//                                .equals(DashboardStationSalesman.salesManStation.getUsername())) {
//                            rejectedOrderLists.add(csr);
//                            adapter.notifyItemInserted(rejectedOrderLists.size() - 1);
//                        }
//                    }
//                }
//            }
//
//            @Override
//            public void onCancelled(DatabaseError databaseError) {
//
//            }
//        });
//
//    }
//
//    @Subscribe
//    public void onLanguageChangeEventReceived(Events.LanguageChangeEvent event) {
//        CURRENT_LANGUAGE = event.isLang();
//        updateRecyclerTitle();
//    }
//
//    @Override
//    public void onDestroyView() {
//        super.onDestroyView();
//        EventBus.unregister(this);
//        ButterKnife.unbind(this);
//    }
//
//    private void updateRecyclerTitle() {
//
//        String rt = CURRENT_LANGUAGE ? getString(R.string.finished_task_arabic) : getString(R.string.finished_task);
//        recyclerTitle.setText("rejected list");
//
//        String wfa = CURRENT_LANGUAGE ? getString(R.string.waiting_for_finished_job_arabic) : getString(R.string.waiting_for_finished_job);
//        emptyTv.setText("waiting for items");
//    }
//}
